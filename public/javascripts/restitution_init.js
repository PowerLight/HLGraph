/**
* Initialisation des varibles utiles à la restitution des données de Handi-chart
*
* @author Gérard Sauer
*
*/

/* ----------------------------------------------------------------------------
Initialisation des variables
----------------------------------------------------------------------------- */

/* Mise en page */
var width = document.getElementById("chart").offsetWidth;
var height = document.getElementById("chart").offsetHeight - 250 ;
var margin = 20;
var padding = 5;
var position = 3;
var legendText = null;

/* Fonctionnalités de zoom sur le graphique */
var zoom1 = d3.behavior.zoom(),
    zoom2 = d3.behavior.zoom(),
    zoom3 = d3.behavior.zoom(),
    zoom4 = d3.behavior.zoom(),
    zoom5 = d3.behavior.zoom(),
    zoom6 = d3.behavior.zoom(),
    zoom7 = d3.behavior.zoom(),
    zoom8 = d3.behavior.zoom(),
    zoom9 = d3.behavior.zoom(),
    zoom10 = d3.behavior.zoom();

/* Evènements personnalisés */
var eventZoomRequestOnWheel = null,
    eventZoomRequestOnMouseUp = null;

/* Compteur d'évènements sur l'évènement mousewheel lord d'un zoom */
var counterWheel = 0;

/* Création d'échelles par défaut */
var x = d3.scale.linear();
var y = d3.scale.linear();
var x2 = d3.time.scale();

var ycpt1 = d3.scale.linear();
    ycpt2 = d3.scale.linear(),
    ycpt3 = d3.scale.linear(),
    ycpt4 = d3.scale.linear(),
    ycpt5 = d3.scale.linear(),
    ycpt6 = d3.scale.linear(),
    ycpt7 = d3.scale.linear(),
    ycpt8 = d3.scale.linear(),
    ycpt9 = d3.scale.linear(),
    ycpt10 = d3.scale.linear();

/* Création d'axes par défaut */
var yaxe1 = d3.svg.axis();
    yaxe2 = d3.svg.axis(),
    yaxe3 = d3.svg.axis(),
    yaxe4 = d3.svg.axis(),
    yaxe5 = d3.svg.axis(),
    yaxe6 = d3.svg.axis(),
    yaxe7 = d3.svg.axis(),
    yaxe8 = d3.svg.axis(),
    yaxe9 = d3.svg.axis(),
    yaxe10 = d3.svg.axis();

/* Génération des lignes par défaut */
var line1 = d3.svg.line(),
    line2 = d3.svg.line(),
    line3 = d3.svg.line(),
    line4 = d3.svg.line(),
    line5 = d3.svg.line(),
    line6 = d3.svg.line(),
    line7 = d3.svg.line(),
    line8 = d3.svg.line(),
    line9 = d3.svg.line(),
    line10 = d3.svg.line();

/* interpolation par defaut entre deux valeurs a et b */
var interpolation = "monotone";

/* activesLines : Contiendra les id_captor des courbes affichées à l'écran */
var activesLines = [];

var horizontalLines = [];
/* updatedMeasures : tableau contenant les données relatives au mesures provenant du dataset */
var updatedMeasures = [];

/* Initialisation de marqueurs qui permettront de créer dynamiquement des cercles
   sur chaque ligne au déplacement de la souris sur le graphique */
var marker1 = null,
    marker2 = null,
    marker3 = null,
    marker4 = null,
    marker5 = null,
    marker6 = null,
    marker7 = null,
    marker8 = null,
    marker9 = null,
    marker10 = null;

/* Variable qui contiendra la valeur de la position en y de chaque cercle */
var markervalue = null;
var allPositions = [];

/* Variable qui servira à comparer une ancienne valeur de x.domain[0] à une nouvelle,
   dans le but de déclencher une requête Ajax pour mettre à jour le graphique */
var previousDomain = 0;

/* Sélection manuelle de timestamp par l'utilisateur */
var minTimesptamp = "";
var maxTimesptamp = "";

 var  stopTimestamp  = "";
 var  startTimestamp = "";
 var  stop_mseconds = 0;
 var start_mseconds = 0;
 var aa="";
 var bb="";

/*definitin des min et max y*/
var minDataReceived/* = 0*/;
var maxDataReceived/* = 0*/;

/* JSON object */
var d = {};
