/**
* Restitution des données présentes dans MongoDB sous la forme d'un Graphique en courbes.
* Ces données concernent un patient pour une séances contenant différents capteurs.
*
* Librairies : JavaScript, jQuery et d3js
* d3.js est une librarie qui permet de manipuler le DOM et qui est efficace dans la manipulation de SVG.
*
* @author Gérard Sauer
*/
/* -----------------------------------------------------------------------------
parsing en JSON d'un String dataset et suppression de l'élement du DOM
----------------------------------------------------------------------------- */
//console.log($('#dataset').val());
var dataset = JSON.parse($('#dataset').val());
var patient_id =  $('#patient').val();
var lesSequences = JSON.parse($('#sequences').val());
var heure_debut_seance = $('#seance_start_time').val();
var date_debut_seance = $('#seance_date_start').val();

var a = heure_debut_seance.split(':'); // split it at the colons

// minutes are worth 60 seconds. Hours are worth 60 minutes.
var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);

var date_debut_seance_timestamp=date_debut_seance.split("-");
var newDate=date_debut_seance_timestamp[1]+"/"+date_debut_seance_timestamp[0]+"/"+date_debut_seance_timestamp[2];
//console.log(new Date(newDate).getTime());

/*console.log(seconds);
console.log(heure_debut_seance);
console.log(date_debut_seance);*/

$("#dataset").remove();
/* -----------------------------------------------------------------------------
Construction d'échelles linéaires (Positionnement, taille et valeurs)
----------------------------------------------------------------------------- */
x.range([margin * 3, width - margin])
 .domain(d3.extent(dataset.captor_data[0].measures, function (d) {
   return d.measureTimestamp;
  }));

x2.range([margin * 3, width - margin])
 .domain(d3.extent(dataset.captor_data[0].measures, function (d) {
   return d.measureTimestamp + (new Date(newDate).getTime()) + seconds*1000;
      }));
/*
function initVerticalRange(loadVal){
    minDataReceived=0;
    maxDataReceived=0;
    console.log(loadVal);
    dataset.captor_data.forEach(function(k, i){
        if(loadVal){
            var ext = d3.extent(shiftCaptorData(k,i,true), function(d){
                return d.dataReceived;
            });
            minDataReceived > ext[0] ? minDataReceived = ext[0] : minDataReceived = minDataReceived;
            maxDataReceived < ext[1] ? maxDataReceived = ext[1] : maxDataReceived = maxDataReceived;
        }
        else shiftCaptorData(k,i,false).forEach(function(l, j){
            minDataReceived > l.dataReceived ? minDataReceived = l.dataReceived : minDataReceived = minDataReceived;
            maxDataReceived < l.dataReceived ? maxDataReceived = l.dataReceived : maxDataReceived = maxDataReceived;
        });
    });
    y.range([height - margin, margin])
     .domain([minDataReceived, maxDataReceived]);
}*/
initVerticalRange(false,true);

dataset.captor_data.forEach(function (k, i) {
  window["ycpt" + k.captor_id] = y
  activesLines.push(k.captor_id);
});
/* -----------------------------------------------------------------------------
Définition des axes
----------------------------------------------------------------------------- */
var xAxis = d3.svg.axis()
  .scale(x)
  .ticks(5);

yaxe1
    .scale(y)
    .orient("left")
    .ticks(5);

var x2Axis = d3.svg.axis()
                .scale(x2)
                .tickFormat(d3.time.format("%H:%M:%S"));

                //.ticks(5);

//dataset.captor_data.forEach(function (k, i) {
//  window["yaxe" + k.captor_id]
//    .scale(window["ycpt" + k.captor_id])
//    .orient("left")
//    .ticks(4);
//});

/* -----------------------------------------------------------------------------
Type du graphique, définit également les colonnes du dataset à utiliser
----------------------------------------------------------------------------- */
dataset.captor_data.forEach(function (k, i) {
  window["line" + k.captor_id] = d3.svg.line() // Courbes
    .interpolate(interpolation)
    .x(function (d) {
        return x(d.measureTimestamp);
    })
    .y(function (d) {
        return y(d.dataReceived);
    });
});

/* -----------------------------------------------------------------------------
Grille appliquée au graphique
----------------------------------------------------------------------------- */
make_x_axis = function () {
return d3.svg.axis()
  .scale(x)
  .orient("bottom")
  .ticks(10);
};

make_y_axis = function () {
  return d3.svg.axis()
    .scale(y)
    .orient("left")
    .ticks(10);
};

make_x2_axis = function () {
return d3.svg.axis()
            .scale(x2)
            .orient("bottom");
};

//dataset.captor_data.forEach(function (k, i) {
//  if (i == 0) {
//    make_y_axis = function () {
//      return d3.svg.axis()
//        .scale(window["ycpt" + k.captor_id])
//        .orient("left")
//        .ticks(10);
//    };
//  }
//});

/* -----------------------------------------------------------------------------
Déclaration du zoom principal et des zooms secondaires
----------------------------------------------------------------------------- */
window["zoom" + dataset.captor_data[0].captor_id].on("zoom", zoomed);

/* -----------------------------------------------------------------------------
Création d'une section svg et des éléments qui lui sont associés
----------------------------------------------------------------------------- */
svg = d3.select('#chart')
  .append("svg")
    .attr('height', height + 50)
    .append("g")
      .attr("transform", "translate(0, 0)")
      .call(window["zoom" + dataset.captor_data[0].captor_id]);

// Ajout d'un rectangle pour pouvoir capturer les évènements souris
svg.append("svg:rect")
  .attr("width", width - margin * 4)
  .attr("height", height - margin * 2 )
  .attr("transform", "translate(" + margin * 3 + "," + margin  + ")")
  .attr("class", "plot");

// Appel de l'axe x
svg.append("svg:g")
  .attr("transform", "translate(0," + (height - margin ) + ")")
  .attr("class", "x axis")
  .style("font-size", "1em")
  .style("font-weight", "bold")
  .call(xAxis);

// Ajout d'un pour nommer l'axe x
svg.append("text")
  .attr("x", width - margin * 3)
  .attr("y", height * 0.95)
  .text("Temps (ms)");

svg.append("svg:g")
  .attr("transform", "translate(0," + (height) +")")
  .attr("class", "x2 axis")
  .style("font-size", "1em")
  //.style("font-weight", "bold")
  .call(x2Axis);
//    .selectAll("text")
//    .style("display", "none");

//d3.select(".x2 path")
//  .style("fill", "none")
//  .style("stroke", lineColor(0))
//  .style("stroke-width", 1.5);



//Essai ajout de balise pour les mesures sur les axes
//svg.append("g")
//    .attr("id", "dynamics-values")
//    .style("height", "auto%");


// Appel des axes y
//dataset.captor_data.forEach(function (k, i) {
  svg.append("svg:g")
    .attr("transform", "translate(" + (margin * position) + ", 0)")
    .attr("class", "y axis y1" )
    .call(window["yaxe1"])
      .selectAll("text")
          .style("display", "none");
//        .style("font-size", "1em")
//        .style("font-weight", "bold")
//        .attr("x", -5)
//        .attr("y", -5)
//        .attr("transform", function(d) {
//            return "rotate(-90)"
//            });

  // Style des ordonnées
  d3.select(".y1 path")
  .style("fill", "none")
  .style("stroke", lineColor(0))
  .style("stroke-width", 1.5);

  position = position - 0.4;
/*
   d3.select(".x2 path")
        //.style("fill","red")
        .style("stroke", lineColor(0));*/
//});

/* -----------------------------------------------------------------------------
Appel et définition de la grille du graphique
----------------------------------------------------------------------------- */
svg.append("svg:g")
  .attr("class", "x grid")
  .attr("transform", "translate(0," + (height) + ")")
  .call(make_x_axis()
    .tickSize(-height + margin * 2, 0, 0)
    .tickFormat("")
  );

svg.append("svg:g")
  .attr("class", "y grid")
  .attr("transform", "translate(" + (margin * 3) + ", 0)")
  .call(make_y_axis()
    .tickSize(-width + margin * 4, 0, 0)
    .tickFormat("")
  );

/* -----------------------------------------------------------------------------
Création du conteneur utilisé par le clip-path
Le clip-path sert à délimiter la zone d’affichage d’un contenu
----------------------------------------------------------------------------- */
var clip = svg.append("defs").append("svg:clipPath")
  .attr("id", "clip")
  .append("svg:rect")
    .attr("id", "clip-rect")
    .attr("x", "0")
    .attr("y", "0")
    .attr("width", width - margin * 4)
    .attr("height", height - margin * 2)
    .attr("transform", "translate(" + margin * 3 + "," + margin + ")");

var chartBody = svg.append("g")
  .attr("clip-path", "url(#clip)");


/* -----------------------------------------------------------------------------
Tracé des courbes
----------------------------------------------------------------------------- */
dataset.captor_data.forEach(function (k, i) {

  updatedMeasures.push(k.measures);
  drawLine(chartBody, k.measures, k.captor_id, i);
  // linesCaptorName(svg, window["ycpt" + k.captor_id], k.measures, k.captor_id, i, width - (margin - padding));


/* -----------------------------------------------------------------------------
Création des légendes du graphique. Définition des comportements onclick sur
ces légendes.
----------------------------------------------------------------------------- */
  d3.select("#chart-legends")
    .attr("class", "row col-md-offset-2")
    .append("div")
      .attr("id", "chart-legend-" + k.captor_id)
      .style("cursor", "pointer")
      .style("display", "inline-block")
      .style("padding-right", "10px")
      .append("div")
        .attr("id", "square" + k.captor_id)
        .attr("class", "fixed-square ")
        .style("vertical-align", "middle")
        .style("background-color",  lineColor(i))
        .style("border", "1px solid black")
        .style("display", "inline-block");

  d3.select("#chart-legend-" + k.captor_id)
    .append("div")
      .attr("id", "legend-name" + k.captor_id)
      //.text(k.captor.captorType)
      .text(captorName(k.captor_id))
      .style("font-weight", "bold")
      .style("display", "inline-block")
      .style("vertical-align", "middle")
      .style("padding-left", "5px");


  /* Si une légende est décochée la courbe associée est supprimée puis recréée
     si re-cochée. Le tableau "activesLines" contient chaque numéro des capteurs
     étant actif visuellement. */

  d3.select("#chart-legend-" + k.captor_id)
    .on("click", function () {
      var isActive = true;
      if (k.measures.active) {
        isActive = false;
        generateLine(dataset, interpolation);
        drawLine(chartBody, updatedMeasures[i], k.captor_id, i);
        activesLines.push(k.captor_id);
        d3.select("#tooltip-y-" + k.captor_id).style("display", "block");
        d3.select("#square" + k.captor_id)
          .style("background-color",  lineColor(i));
        d3.select(".y" + k.captor_id).style("display", "block");
        d3.select("#tools-line-option-" + k.captor_id).style("display", "block");
 //       drawHorizontalLine(k.captor_id);

      }
      else {
        d3.select("#line" + k.captor_id).remove();
        d3.select("#chart-legend-" + k.captor_id)
          .style("color", "black")
        d3.select("#square" + k.captor_id)
          .style("background-color", "white");
        d3.select(".y" + k.captor_id).style("display", "none");
        d3.select("#tools-line-option-" + k.captor_id).style("display", "none");
        d3.select("#tooltip-y-" + k.captor_id).style("display", "none");
        d3.select("#hl-" + k.captor_id).style("display", "none");

        var remove = null;
        horizontalLines.forEach(function(ligne, index){
            if(ligne.attr("id") == "hl-" + k.captor_id){
                remove = index;
            }
        });
        horizontalLines.splice(remove, 1),

        removeArrayElement(activesLines, k.captor_id);
      }
      k.measures.active = isActive;
    });

   // Création des balises "option" pour l'outil de sélection de courbes
  d3.select("#tools-line")
  .append("option")
    .attr("id", "tools-line-option-" + k.captor_id)
    .attr("value", k.captor_id)
    .style("color", lineColor(i))
    .text(captorName(k.captor_id));

  // Styles appliqués aux légendes au survol de la souris
  d3.select("#chart-legend-" + k.captor_id)
    .on("mouseover", function(){
      d3.select("#legend-name" + k.captor_id)
        .style("text-decoration", "underline")
      })
    .on("mouseout", function(){
      d3.select("#legend-name" + k.captor_id)
        .style("text-decoration", "none")
      })
});

// Positionnement de la boite à outil pour les zoom
d3.select("#toolbar-chart")
  .style("margin-left", margin * 3)

/* -----------------------------------------------------------------------------
redimenssionement du svg lorsque la fenêtre du navigateur change de taille
----------------------------------------------------------------------------- */
$(window).resize(function () {
  resizeSvg(dataset);
});

/**
* Fonction éxécutée lors d'un zoom
*/
function zoomed() {
   var captor;
   // Récupération des informations de la boite à outil.
   var toolAxe = document.getElementById("tools-axe");
   var optionAxe = toolAxe.options[toolAxe.selectedIndex].value;
   var toolLine = document.getElementById("tools-line");
   var optionLine = toolLine.options[toolLine.selectedIndex].value;

   dataset.captor_data.forEach(function (k, i) {
       if (i == 0) {
           captor = k.captor_id;
           // Définition des propriétés des zoom secondaires en fonction du zoom primaire
           dataset.captor_data.forEach(function (j, f) {
               if (f > 0) {
                   window["zoom" + j.captor_id].scale(window["zoom" + captor].scale());
                   window["zoom" + j.captor_id].translate(window["zoom" + captor].translate());
               }
           });
       }

       // Définit selon le choix d'axe(s) et de courbe(s) des comportements de zoom
       ZoomToolBox(optionAxe, optionLine, k.captor_id, x);
   });
   // Appel des axes, grille et courbes
   callLine(dataset);
   displayStimuli();
 };

/* -----------------------------------------------------------------------------
Gestion de la boite modal bloquant la vue lors d'une reqûete Ajax
----------------------------------------------------------------------------- */
$(document).on({
   ajaxStart: function () {
       $("body").addClass("loading");
   },
   ajaxStop: function () {
       $("body").removeClass("loading");
   }
});

function jsonData(_xmin, _xmax, numeroUniqueDeSeance, patient_id){

  var obj = {
      "patient_id": patient_id,
      "start": Math.round(_xmin, 0),
      "stop": Math.round(_xmax, 0),
      "numeroUniqueDeSeance" : numeroUniqueDeSeance
  };
  return obj;
}

/**
* Mise à jour du graphique SVG.
*
* Coté métier, un filtre est appliqué au nombre de données que le dataset contiendra pour chaque
* capteurs. Ceci afin d'éviter d'avoir pour chaque intervalle de temps des milliers
* voir des millions de coordonnées (le navigateur ne le supporterait pas).
*
* Lors d'une action zoomer/dézoomer à la molette souris ou lors d'un déplacement du
* graphique sur l'axe du temps, un rafraichissement du dataset doit être exécuté pour
* obtenir de nouvelles données filtrées relative à un nouvel intervalle de temps.
*/
function updateGraph(data) {

  $.ajax({
      type: "POST",
      dataType: 'json',
      data: JSON.stringify(data),
      contentType: "application/json; charset=utf-8",
      url: "/ajax/svg",
      timeout: 0,
      beforeSend: function(){
        updatedMeasures = [];
      },
      success: function (result) {
        var cpt = null;
        var index = null;

        dataset = result;
        //ajustement de l'echelle verticale
        initVerticalRange(true,true);
        dataset.captor_data.forEach(function (k, i) {
          d3.select("#line" + k.captor_id).remove();
          cpt = null;
          // Détection des légendes cochées et comparaison avec les capteurs
          for (var j = 0; j < activesLines.length; j++) {
              if (activesLines[j] == k.captor_id) {
                  cpt = k.captor_id;
                  index = i;
              }
          }
          if (cpt != null) {
              drawLine(chartBody, shiftCaptorData(k,i), cpt, index);
          }
          /* Récupération dans un tableau des nouvelles mesures pour chaque capteur. */
          updatedMeasures.push(k.measures);
        });
      },
      error: function (result, statut, error) {
          alert("Erreur d'exécution de la requête. Veuillez rafraichir la page de votre navigateur");
      },
      complete: function(){
        d3.select("#linenull").remove();
      }
  });
};

$("#shift").change(function(){
    var cpt,index;

    initVerticalRange(false,false);
});

/* -----------------------------------------------------------------------------
Cercles et tooltips
----------------------------------------------------------------------------- */
dataset.captor_data.forEach(function (k, i) {
  /* Cercle qui se déplaceront sur chaque courbe */
  window["marker" + k.captor_id] = svg.append('circle')
    .attr('r', 2)
    .attr("id", "circle" + k.captor_id)
    .style('display', 'none')
    .style('fill', lineColor(i))
    .style('pointer-events', 'none')
    .style('stroke', lineColor(i))
    .style('stroke-width', '3px');
})

/* Les données étant dans un tableau d'objets avec des attributs, D3 doit connaître
   l'emplacement des measureTimestamp qu'il faudra couper en deux.

   Lorsque la fonction sera appelée "bisect(measuresObject, timestamp)", elle va
   inspecter cet objet et comparer chaque measureTimestamp au timestamp passé en
   paramètre qui est celui de la position x de la souris sur le svg.

   Le paramètre .right signifie que la fonction va sélectionner le measureTimestamp
   le plus proche et à la droite du timestamp passé en paramètre.

   Une fois le point trouvé, l'index du tableau pour cette position est retournée.*/
var bisect = d3.bisector(function (d) {
  return d.measureTimestamp;
}).right;

/* Ligne pointillée horizontale qui sera positionnée sur le curseur de la souris */

var verticalLine = svg.append("line")
  .attr("class", "dasharray-line")
  .attr("y1", margin)
  .attr("y2", height - margin);
//Lignes pour visualiser les intervals de sélection
// Pour le moment ça ne fonctionne PAS !!!
var verticalLine1 = svg.append("line")
                    .attr("y1", margin)
                    .attr("y2", height - margin)
                    .style('display', 'inherit')/*
                    .attr("transform", function () {
                        return "translate(" + mouse[0] + ",0)";
                    })*/;
var verticalLine2 = svg.append("line")
                    .attr("y1", margin)
                    .attr("y2", height - margin)
                    .style('display', 'inherit')/*
                    .attr("transform", function () {
                        return "translate(" + mouse[0] + ",0)";
                    })*/;

//dataset.captor_data.forEach(function(k,i){
//    drawHorizontalLine(k.captor_id);
//});
function drawHorizontalLine(captor_id){
    horizontalLines.push(svg.append("line")
        .attr("id", "hl-" + captor_id)
        .attr("class", "dasharray-line")
         .attr("x1", width)
         .attr("x2", 8)
    );

};


// Création des tooltip et positionnement.
position = 3;
dataset.captor_data.forEach(function(k, i){
  svg.append("text")
  .attr("id", "tooltip-y-" + k.captor_id)
  .attr("transform", "translate(" + position + ", " + (margin
  ) + ")")
  .style("font-weight", "bold")
  .style('fill', lineColor(i));
  position = position - 0.4;
})

svg.append("text")
  .attr("id", "tooltip-x")
  .attr("class", "tooltip-x")
  .attr('y', margin - padding);

/* Affichage des cercles, tooltip, lignes suivant différents évènements et légendes
   cochées/décochées*/
svg.on('mouseout', function () {
  d3.selectAll("circle").style("display", "none");
  d3.select("#tooltip-x").style("display", "none");
  verticalLine.style('display', 'none');
  // horizontalLine.style('display', 'none');
  dataset.captor_data.forEach(function (k, i) {
    for (var j = 0; j < activesLines.length; j++) {
      if (activesLines[j] == k.captor_id) {
        legendText = k.captor.unite;
        d3.select("#tooltip-y-" + k.captor_id)
          .style("display", "block")
          .text(legendText);
      }
    }
  })

  horizontalLines.forEach(function(k,i){
          k.style('display', 'none');

  });

}).on('mousemove', function () {
    allPositions = [];
  d3.select("#tooltip-x").style("display", "inherit");
  verticalLine.style('display', 'inherit');
  // horizontalLine.style('display', 'inherit');
  var mouse = d3.mouse(this);
  var mouseTimestamp = Math.round(x.invert(mouse[0]), 0) / 1000;



  verticalLine.attr("transform", function () {
    return "translate(" + mouse[0] + ",0)";
  });

  horizontalLines.forEach(function(k,i){
        k.style('display', 'inherit');
   });

  dataset.captor_data.forEach(function (k, i) {
    markervalue = markerVal(k.measures, x, mouse, bisect,i);
    //legendText = Math.round(markervalue, 0) + " " + k.captor.unite;
    //suppression de l'effet du décalage dans la lecture des amplitudes
    legendText = Math.round(markervalue, 0)-$('#shiftMemo').val()*i + " " + k.captor.unite;

    if (markervalue != false) {

    allPositions.push({"captor_id" : k.captor_id, "markervalue" : markervalue});
      // Positionnement d'un cercle pour chaque capteur.
      window["marker" + k.captor_id]
        .attr('cx', mouse[0])
        .attr('cy', window["ycpt" + k.captor_id](markervalue));

        for (var j = 0; j < activesLines.length; j++) {
          if (activesLines[j] == k.captor_id) {
            var countMeasures = k.measures.length;
            window["marker" + k.captor_id].style('display', 'inherit');
            d3.select("#tooltip-y-" + k.captor_id)
              .style("display", "block")
              .text(legendText);
            // Positionner un élément sur le graphique à la fin de chaque courbes.
            // .attr("transform", "translate(" + (width - margin + padding) + "," + window["ycpt" + k.captor_id](k.measures[countMeasures - 1].dataReceived) + ") rotate(300)")

            //d3.select("hl-" + k.captor_id).style('display', 'inherit');
          }
      }
    }

dataset.captor_data.forEach(function (k, i) {

    tabbedPositions = tabPositions(allPositions)

    tabbedPositions.forEach(function(pos,ind){
        if(pos.captor_id == k.captor_id){
            d3.select("#tooltip-y-" + k.captor_id)
                .attr("transform", "translate(3, " + (y(pos.markervalue)) + ")");
        }
    });
});
    });

  d3.select("#tooltip-x")
    .attr('x', mouse[0] - 25)
    .style("display", "inherit")
    .style("font-weight", "bold")
    .text("T : " + mouseTimestamp + " sec.")
})

/* -----------------------------------------------------------------------------
Déclenchements personnalisés des requêtes Ajax sur les évènnements souris.
----------------------------------------------------------------------------- */
$("#chart")
.on("mouseup", function(e){
  if (x.domain()[0] != previousDomain) {
    eventZoomRequestOnMouseUp = new Event("zoomRequestOnMouseUp");
    this.dispatchEvent(eventZoomRequestOnMouseUp);
    previousDomain = x.domain()[0]
  }
}).on("mousewheel wheel", function(e){ // Chrome + Firefox
  if (x.domain()[0] != previousDomain) {
    if (e.originalEvent.deltaY < 0) {
      if (counterWheel < 0) {
        counterWheel = 0;
      }
      counterWheel++;
    }else{
      if (counterWheel > 0) {
        counterWheel = 0;
      }
      counterWheel--;
    }
    if (counterWheel == 5 || counterWheel == -4) {
      counterWheel = 0;
      eventZoomRequestOnWheel = new Event('zoomRequestOnWheel');
      this.dispatchEvent(eventZoomRequestOnWheel);
    }
    previousDomain = x.domain()[0];
  }

}).on("zoomRequestOnWheel", function(){
  updateGraph(d = jsonData( x.domain()[0], x.domain()[1],dataset.numeroUniqueDeSeance,patient_id));

}).on("zoomRequestOnMouseUp", function(){
  updateGraph(d = jsonData(  x.domain()[0], x.domain()[1],dataset.numeroUniqueDeSeance,patient_id));
})

/* -----------------------------------------------------------------------------
Déclenchements personnalisés des requêtes Ajax sur des timestamp choisis.
----------------------------------------------------------------------------- */
function getCustomTimestamps(){

}


$("#max-timestamp, #min-timestamp").on("input", function(){
  maxTimestamp = $("#max-timestamp").val();
  minTimestamp = $("#min-timestamp").val();
  if (minTimestamp.length != 0 && maxTimestamp.length != 0 && maxTimestamp != minTimestamp) {
    $("#btn-ajax").attr("disabled", null);
  }else{
    $("#btn-ajax").attr("disabled", "disabled");
  }
})

$("#btn-ajax").on("click", function(){
  if(minTimestamp>maxTimestamp){
    var tmp=minTimestamp;
    minTimestamp=maxTimestamp;
    maxTimestamp=tmp;
  }
  window.location.href = "/reloadRestitution/"+ minTimestamp + "/" + maxTimestamp+"/"+
    dataset.numeroUniqueDeSeance + "/" + patient_id+'/'+$('#shift').val();
})

function extractStartStop(){
      var CheminComplet = document.location.href;
      var CheminRepertoire  = CheminComplet.substring( 0 ,CheminComplet.lastIndexOf( "/" ) );
      CheminRepertoire  = CheminRepertoire.substring( 0 ,CheminRepertoire.lastIndexOf( "/" ) );
      CheminRepertoire  = CheminRepertoire.substring( 0 ,CheminRepertoire.lastIndexOf( "/" ) );
      var idStop        = CheminRepertoire.substring(CheminRepertoire.lastIndexOf( "/" )+1 );
      CheminRepertoire  = CheminRepertoire.substring( 0 ,CheminRepertoire.lastIndexOf( "/" ) );
      var idStart       = CheminRepertoire.substring(CheminRepertoire.lastIndexOf( "/" )+1 );
      $("#min-timestamp").val(idStart);
      $("#max-timestamp").val(idStop);
      CheminRepertoire  = CheminRepertoire.substring( 0 ,CheminRepertoire.lastIndexOf( "/" ) );
      return CheminRepertoire.substring(CheminRepertoire.lastIndexOf( "/" )+1 );
}

function tronqueUnite(x){
    x+='';
    var t=x.lastIndexOf( "." );
    if(t<0)return x;
    return x.substring( 0 ,t);
}
$("#btn-ajax2").on("click", function(){
    //initialisation de minTimestamp et maxTimestamp à partir de l'url
    {
      var tst=extractStartStop();
      var idStop  = $("#max-timestamp").val();
      var idStart = $("#min-timestamp").val();
      var delta =(idStop-idStart)/8;
      minTimestamp=tronqueUnite(idStop-delta);
      maxTimestamp=tronqueUnite(2*idStop-idStart-delta);
    }
    window.location.href = "/"+tst+"/"+ minTimestamp + "/" + maxTimestamp+"/"+
        dataset.numeroUniqueDeSeance + "/" + patient_id+'/'+$('#shift').val();
});

$("#btn-ajax3").on("click", function(){
    //initialisation de minTimestamp et maxTimestamp à partir de l'url
    {
      var tst=extractStartStop();
      var idStop  = $("#max-timestamp").val();
      var idStart = $("#min-timestamp").val();
      var delta=(idStop-idStart)/8;
      maxTimestamp=tronqueUnite(idStart*1+delta);
      minTimestamp=tronqueUnite(2*idStart-idStop+delta);
    }
    window.location.href = "/"+tst+"/"+ minTimestamp + "/" + maxTimestamp+"/"+
        dataset.numeroUniqueDeSeance + "/" + patient_id+'/'+$('#shift').val();
});


displayStimuli();
function displayStimuli(){
    var lesTicks = d3.select(".x2").selectAll(".tick");
    var tcm = 0;
    lesSequences.forEach(function (sequence, index){
         tcmStart = tcm;
         tcmStop = tcmStart + (sequence.duration * 60000);

         lesTicks[0].forEach(function(unTick, sonIndex){
            d3Tick = d3.select(unTick);
            var xpos = Math.round(x.invert(d3.transform(d3Tick.attr("transform"))
                                                .translate[0]), 0);
            //d3Tick.append("text").style("display", "block"); //.attr("transform", "translate("+xpos+", "+ (width - margin) + ")");
			if (xpos >= tcmStart && xpos <= tcmStop){

                if((sequence.stimuliType.localeCompare("NO_STIMUL") == 0)){
                    d3Tick.style("fill", "black");
                    d3Tick.selectAll("text").text("NO_STIMUL");
                 } else {
                        colorFlag=sequence.stimuli.ledColor.substr(0,3);
                        switch(colorFlag){
                            case "GRE":
                                d3Tick.attr("stroke", "green");
                                break;
                            case "RED":
                                d3Tick.attr("stroke", "red");
                                break;
                            case "WHI":
                                d3Tick.attr("stroke", "lightGrey")
                        }
                        d3Tick.selectAll("text").text(sequence.stimuli.frequency + "Hz - " + sequence.stimuli.intensity +"%");
                 }
            }
         });
        tcm = tcmStop;
    });

}
/* -----------------------------------------------------------------------------
Calculer la distance entre deux points du graphique.
----------------------------------------------------------------------------- */
///////Affichage des positions sélectionnées

///////
                d3.select("#interval").on("click",function(){
                    var x1=0;
                    var x2=0;
                    $("#x_fin").val(0);

                    $("#interval-text").val("Cliquez sur x1");
                    d3.select("#chart").on("click", function(e){

                        x1 = x.invert(d3.mouse(this)[0]);
                        $("#interval-text").val("Cliquez sur x2");
                        verticalLine1.attr("transform", function () {
                            return "translate(" + x.invert(d3.mouse(this)[0]) + ",0)";
                        }).style('display', 'inherit');
                        $("#x_debut").val(Math.round(x1,1));

                        d3.select("#chart").on("click", function(e){
                            if($("#x_fin").val()==0){
                                 x2 = x.invert(d3.mouse(this)[0]);
                                verticalLine2.attr("transform", function () {
                                    return "translate(" + d3.mouse(this)[0] + ",0)";
                                }).style('display', 'inline-block');
                                console.log(verticalLine1);

                                 if(x2>x1) $("#x_fin").val(Math.round(x2,1));
                                 else{
                                    $("#x_debut").val(Math.round(x2,1));
                                    $("#x_fin").val(Math.round(x1,1));
                                 }

                                 var interval = Math.round((x2-x1),49);
                                 $("#interval-text").val("("+$("#x_debut").val()/1000+" ; "+$("#x_fin").val()/1000+") "+interval/1000 + " s");

                                 $("#spindle-complexeK").show();
                                 $("#spindle-text").val(Math.round(x1,49) + " - " + Math.round(x2,49));
                                 $("#complexeK-text").val(Math.round(x1,49) + " - " + Math.round(x2,49));
                            }
                            else {console.log('Un peu trop de clicks')}
                        });
                    });
                });


                $("#spindle").on("click",function(){
                    $("#spindle-text").show();
                });
                $("#complexeK").on("click",function(){
                    $("#complexeK-text").show()
                });








//Créer des intervalles


// Remove zoom listener
// svg.on("mousedown.zoom", null);
// svg.on("mousemove.zoom", null);
// svg.on("dblclick.zoom", null);
// svg.on("touchstart.zoom", null);
// svg.on("wheel.zoom", null);
// svg.on("mousewheel.zoom", null);
// svg.on("MozMousePixelScroll.zoom", null);

// refresh button
// var cpt = null;
// var index = null;
// dataset = result;
//
// Mise à jour des axes y en fonction du nouveau dataset
//            dataset.captor_data.forEach(function(k, i) {
//              window["ycpt" + k.captor_id] = d3.scale.linear()
//                .range([height - margin, margin])
//                .domain(d3.extent(k.measures, function(d){
//                  return d.dataReceived;
//                }));
//              // activesLines.push(k.captor_id);
//              window["yaxe" + k.captor_id] = d3.svg.axis()
//                .scale(window["ycpt" + k.captor_id])
//                .orient("left")
//                .ticks(6);
//              });
//
// generateLine(dataset, interpolation);
// callLine(dataset);
//
// dataset.captor_data.forEach(function (k, i) {
//     d3.select("#line" + k.captor_id).remove();
//     cpt = null;
//     for (var j = 0; j < activesLines.length; j++) {
//         if (activesLines[j] == k.captor_id) {
//             cpt = k.captor_id;
//             index = i;
//         }
//     }
//     if (cpt != null) {
//         drawLine(chartBody, k.measures, cpt, index);
//     }
//     updatedMeasures.push(k.measures);
