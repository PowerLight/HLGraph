/**
 * restitution_functions contient toutes les fonctions relatives au fonctionnement
 * de la restitution des graphiques handi'Light via le fichier restitution.js
 *
 * @author Gérard Sauer
 */


/**
 * Supprime un élément dans un tableau en le comparant à un autre
 *
 * @param _inactiveLine : Contient les id_captor des courbes masquées
 * @param _captorId     : Elément à comparer
 */
function removeArrayElement(_inactiveLine, _captorId) {
     for(var i = _inactiveLine.length; i--;) {
         if(_inactiveLine[i] === _captorId) {
             _inactiveLine.splice(i, 1);
         }
     }
 }


/**
Décale les mesures verticalement.
*/
function shiftCaptorData(k/*captor*/,i/*indice*/){
    var res=k.measures;
    var shift;
    var memo=$('#shiftMemo').val();

    shift = $('#shift').val()*i;
    k.measures.forEach(function(l,j){
        res[j].dataReceived=l.dataReceived+shift;
    });
    k.measures=res;
    return res;
}

function initVerticalRange(relaodBoolean,sameShift){
    var ext;
    var shift;
    if($('#shift').val()<0){
      var CheminComplet = document.location.href;
      $('#shift').val(CheminComplet.substring(CheminComplet.lastIndexOf( "/" )+1 ));
    }
    minDataReceived =0;
    maxDataReceived =$('#shift').val()*(dataset.captor_data.length-1);
    if(sameShift){
        if(!relaodBoolean){
            dataset.captor_data.forEach(function(k, i){
                    var res=k.measures;
                    shift=$('#shift').val()*i;
                    res.forEach(function(l,j){ l.dataReceived=l.dataReceived+shift; });
                    ext = d3.extent(res, function(d){ return d.dataReceived; });//application du décalage
                    if(minDataReceived > ext[0]) minDataReceived = ext[0];
                    if(maxDataReceived < ext[1]) maxDataReceived = ext[1];
            });
        }
        else{
            //Calcule du "vertical range" rapide à partir du premier et du dernier signal
            maxDataReceived=0;
            ext=dataset.captor_data.length-1;

            dataset.captor_data[0].measures.forEach(function(k, i){
                if(minDataReceived>k.dataReceived)minDataReceived=k.dataReceived;
            });

            dataset.captor_data[ext].measures.forEach(function(k, i){
                if(maxDataReceived<k.dataReceived)maxDataReceived=k.dataReceived;
            });
            maxDataReceived+=($('#shift').val())*(ext);
        }
    }
    else{
        dataset.captor_data.forEach(function(k, i){
                var res=k.measures;
                shift=($('#shift').val()-$('#shiftMemo').val())*i
                k.measures.forEach(function(l,j){ res[j].dataReceived=l.dataReceived+shift; });//décalage des mesures
                ext = d3.extent(res, function(d){ return d.dataReceived; });//envoit vers l'affichage est extraction des extrema
                if(minDataReceived > ext[0]) minDataReceived = ext[0];
                if(maxDataReceived < ext[1]) maxDataReceived = ext[1];
        });
    }
    $('#shiftMemo').val($('#shift').val());//mémorisation du décalage

    y.range([height - margin, margin]).domain([minDataReceived-5, maxDataReceived+5]);//modification du 'range'
}

/**
 * Définit selon le choix d'axe(s) et de courbe(s) des comportements de zoom
 *
 * @param _optionAxe  : Axe sélectionnée
 * @param _optionLine : Courbe sélectionnée
 * @param _captorId   : Id du capteur
 * @param _x          : Echelle linéaire x définit
 */
function ZoomToolBox(_optionAxe, _optionLine, _captorId, _x){
  var xnull= d3.scale.linear();
  var ynull= d3.scale.linear();
  if (_optionAxe == 3) {
    if (_optionLine == 0) {
     window["zoom" + _captorId].x(_x).y(window["ycpt" + _captorId]);
    }
    else if (_optionLine == _captorId) {
     window["zoom" + _captorId].x(_x).y(window["ycpt" + _captorId]);
    }
    else {
     window["zoom" + _captorId].x(xnull).y(ynull);
    }
  }
  else if (_optionAxe == 2) {
    if (_optionLine == 0) {
     window["zoom" + _captorId].x(xnull).y(window["ycpt" + _captorId]);
    }
    else if (_optionLine == _captorId) {
     window["zoom" + _captorId].x(xnull).y(window["ycpt" + _captorId]);
    }
    else {
     window["zoom" + _captorId].x(xnull).y(ynull);
    }
  }
  else {
    window["zoom" + _captorId].x(_x).y(ynull);
  }
}

/**
 * Appel d'un nom de classe Bootstrap
 *
 * @param i : Index en cours dans une boucle
 * @return un String
 */
function bootstrapColor(i){
  switch (i) {
    case 0: return "btn-danger"; break;
    case 1: return "btn-primary"; break;
    case 2: return "btn-success"; break;
    case 3: return "btn-warning"; break;
    case 4: return "btn-info"; break;
    case 5: return "btn-BlueViolet"; break;
    case 6: return "btn-GreenYellow"; break;
    case 7: return "btn-HotPink"; break;
    case 8: return "btn-OrangeRed"; break;
    case 9: return "btn-LightSalmon"; break;
    case 10: return "btn-LightSeaGreen"; break;
    default: return "btn-success";
  }
}

/**
 * Utilisation de coloris pour les courbes, axes et boutons
 *
 * @param i : Index en cours dans une boucle
 * @return un String
 */
function lineColor(i){
  switch (i) {
    case 0: return "#d9534f"; break;
    case 1: return "#337ab7"; break;
    case 2: return "#5cb85c"; break;
    case 3: return "#f0ad4e"; break;
    case 4: return "#5bc0de"; break;
    case 5: return "#FF69B4"; break;
    case 6: return "#8A2BE2"; break;
    case 7: return "#7ccc00"; break;
    case 8: return "#FF4500"; break;
    case 9: return "#ff9266"; break;
    case 10: return "#1fada5"; break;
    default: return "#5cb85c";
  }
}

/**
 * Récupération des nom de capteurs et retranscription en langage commun
 *
 * @param i : ID du capteur
 * @return un String
 */
function captorName(_captorId){
    switch (_captorId) {
       case 1: return "EEG"
         break;
       case 2: return "Fréquence cardiaque"
         break;
       case 3: return "Clignements Yeux"
         break;
       case 4: return "Gyro axe x"
         break;
       case 5: return "Gyro axe y"
         break;
       case 6: return "Gyro axe z"
         break;
       case 7: return "Accélération axe x"
         break;
       case 8: return "Accélération axe y"
         break;
       case 9: return "Accélération axe z"
         break;
       case 10: return "Conductance Peau"
         break;
       case 11: return "old_captor";
         break;
      default: return "Capteur inconnu";
    }
  }

/**
 * Modification de l'interpolation des courbes
 */
function getInterpolation(){
  var toolInterpolation = document.getElementById("tools-interpolate");
  var optionInterpolation = toolInterpolation.options[toolInterpolation.selectedIndex].value;

  switch (optionInterpolation) {
    case "2": interpolation = "basis";
      break;
    case "3": interpolation = "linear";
      break;
    case "4": interpolation = "step-before";
      break;
    // case "3": interpolation = "basis-open";
    //   break;
    // case "4": interpolation = "basis-closed";
    //   break;
    // case "5": interpolation = "linear";
    //   break;
    // case "6": interpolation = "linear-closed";
    //   break;
    // case "7": interpolation = "step-before";
    //   break;
    // case "8": interpolation = "step-after";
    //   break;
    // case "9": interpolation = "bundle";
    //   break;
    // case "10": interpolation = "cardinal";
    //   break;
    // case "11": interpolation = "cardinal-open";
    //   break;
    // case "12": interpolation = "cardinal-closed";
    // break;
    default: interpolation = "monotone";
  }

  generateLine(dataset, interpolation);
  callLine(dataset);
}

/**
 * Tracer les courbes sur le graphique svg grâce à l'attribut obligatoire "d" (data)
 * qui reverse les données à la balise "path".
 * "path" est une balise générique qui permet de tracer toutes les formes possibles
 *
 * @param _chartBody : Conteneur délimitant la zone d'affichage des courbes
 * @param _measures  : Tableau contenant les coordonnées x et y à utiliser
 * @param _captorId  : capteur sur lequel le travail en cours s'effectue.
 * @param _i         : Index d'itération en cours dans une boucle
 */
function drawLine(_chartBody, _measures, _captorId, _i){

  _chartBody.append("path")
    .data([_measures])
    .attr("d", window["line" + _captorId])
    .attr('id', 'line' + _captorId)
    .attr('class', 'line' + _captorId)
    .style("fill", "none")
    .style("stroke", lineColor(_i))
    .style("stroke-width", 1.5);
  }

/**
 * Construit un nouveau générateur de ligne ayant des accesseurs X et Y et l'interpolation.
 * Path Data Generator Function for lines
 *
 * @param _dataset      : Objet Json contenant les données à exploiter
 * @param _interpolate  : Interpolation définit
 * @return Génère des données de chemin pour une courbe linéaire
 */
function generateLine(_dataset, _interpolate){
  _dataset.captor_data.forEach(function(k, i) {
    window["line" + k.captor_id] = d3.svg.line() // Courbes
      .interpolate(_interpolate)
      .x(function(d){
        return x(d.measureTimestamp);
      })
      .y(function(d){
        return window["ycpt" + k.captor_id](d.dataReceived);
      });
    });
  }

/**
 * Invoque plusieurs fonctions spécifiques : axe x et y, grille et mise à jour
 * des data pour le path de chaque courbe. Utile aussi au redimenssionement du svg
 *
 * @param _dataset : Objet Json contenant les données à exploiter
 */
function callLine(_dataset){
  _dataset.captor_data.forEach(function(k, i) {
    svg.select(".x.axis").call(xAxis);
    svg.select(".y.axis.y" + k.captor_id)
      .call(window["yaxe" + k.captor_id])
        .selectAll("text")
          .style("font-size", "1em")
          .style("font-weight", "bold")
          //.style("display", "none")
          .attr("x", -5)
          .attr("y", -5)
          .attr("transform", function(d) {
              return "rotate(-90)"
              });
    svg.select(".x.grid")
      .call(make_x_axis()
      .tickSize((-height + margin * 2), 0, 0)
      .tickFormat(""));
    svg.select(".y.grid")
      .call(make_y_axis()
      .tickSize((-width + margin * 4), 0, 0)
      .tickFormat(""));
    // inutile de transmettre les données dans l'attribut "d" => window["line" + k.captor_id](k.measures)
    // Elles sont récupérées lors de la sélection d'un path grâce à son id
    svg.select("#line" + k.captor_id)
      .attr("class", "line" + k.captor_id)
      .attr("d", window["line" + k.captor_id]);
    });
}

/**
 * Création dynamique d'un texte représentant le nom d'une courbe. Ce texte est
 * positionné à la fin de la courbe sur le graphique SVG.
 *
 * @param _svg        : Conteneur SVG
 * @param _ycpt       : Echelle axe y ciblée
 * @param _measures   : Contient les mesures d'un capteur
 * @param _cptId      : ID du capteur ciblé
 * @param _index      : Index de boucle forEach
 * @param _xTranslate : Représent la translation en x à effectuer
 */
function linesCaptorName(_svg, _ycpt, _measures, _cptId, _index, _xTranslate){
  var countMeasures = _measures.length;
  _svg.append("text")
    .attr("class", "linesCaptorName")
    .attr("transform", "translate(" + _xTranslate + "," + _ycpt(_measures[countMeasures - 1].dataReceived) + ")")
    .attr("dy", ".35em")
    .attr("text-anchor", "start")
    .style("fill", lineColor(_index))
    .text(captorName(_cptId));
}

/**
 * redimenssionement du graphique svg lorsque la fenêtre du navigateur change de taille
 *
 * @param _dataset  : Objet Json contenant les données à exploiter
 */
function resizeSvg(_dataset){
  width = document.getElementById("chart").offsetWidth;
  height = document.getElementById("chart").offsetHeight;

  x.range([margin * 3, width - margin])

  y.range([height - margin, margin]);

  callLine(_dataset);

  d3.select("svg")
    .attr('height', height)

  d3.select("rect")
    .attr("width", width - margin * 3)
    .attr("height", height - margin * 2)

  d3.select("text")  // Nommage de l'axe x
    .attr("x", width - margin * 2)
    .attr("y", height * 0.98)
    .text("Temps (sec)");
}

/**
 * Masque la sélection des courbes lors d'un zoom sur l'axe des x
 */
function toolsVisibility(){
  var toolAxe = document.getElementById("tools-axe");
  var optionAxe = toolAxe.options[toolAxe.selectedIndex].value;
  var toolLine = document.getElementById("tools-line");
  var optionLine = toolLine.options[toolLine.selectedIndex].value;
  if (optionAxe == 1) {
    document.getElementById("tools-line").setAttribute('disabled','disabled');
  }
  else {
    document.getElementById("tools-line").removeAttribute('disabled', null);
  }
}

/**
* Retourn une valeur interpolée
* @param _measures  : object contenant les mesures relatives à un capteur.
* @param _x  : échelle linéaire en x
* @param _mouse  : coordonnées x et y de la souris.
* @param _bisect  : fonction bisector. bisect(data, value), retourne un index.
* @return valueY or false
*/
function markerVal(_measures, _x, _mouse, _bisect,i) {
  var timestamp = _x.invert(_mouse[0]);// Convertir de pixel vers timestamp.
  var index = _bisect(_measures, timestamp),
  startDatum = _measures[index - 1],
  endDatum = _measures[index];
  range = endDatum.measureTimestamp - startDatum.measureTimestamp;
  if (index - 1 >= 0) {
    /* Retourne une interpolation entre deux nombres. C'est une valeur intermédiaire. */
    interpolate = d3.interpolateNumber(startDatum.dataReceived, endDatum.dataReceived);
    valueY = interpolate((timestamp % range) / range);
    return valueY;
  }else{
    return false;
  }
}
function getMaxTableau(tableauNumérique) {
    return Math.max.apply(null, tableauNumérique);
}
function getMinTableau(tableauNumérique) {
    return Math.min.apply(null, tableauNumérique);
}

function tabPositions(allPositions){
    allPositions.sort(function(a,b){
        return a.markervalue - b.markervalue
    });
    tabbedPositions = $.extend(true, [], allPositions);

    var last = tabbedPositions[0].markervalue - 50;
    tabbedPositions.forEach(function(unePosition, sonIndex){
        if(unePosition.markervalue < last + 30){
            unePosition.markervalue = last + 30;
            tabbedPositions[sonIndex] = unePosition;
        }
        last = unePosition.markervalue;
    });

    return tabbedPositions;
}

