$(document).ready( function () {
    $('#dataTable').DataTable( {
      "language": {
      	"sProcessing":     "Traitement en cours...",
      	"sSearch":         "Rechercher&nbsp;:",
        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
      	"sInfo":           "Affichage des &eacute;lements _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
      	"sInfoEmpty":      "Affichage des &eacute;lements 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
      	"sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
      	"sInfoPostFix":    "",
      	"sLoadingRecords": "Chargement en cours...",
        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
      	"sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
      	"oPaginate": {
      	"sFirst":      "Premier",
      	"sPrevious":   "Pr&eacute;c&eacute;dent",
      	"sNext":       "Suivant",
      	"sLast":       "Dernier"
      	},
      	"oAria": {
      		"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
      		"sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
      	}
      },
      // Définition des collones ou le tri jj/mm/aaaa sera appliqué (voir fichier date-uk.js)
      "aoColumns": [
			null,
            null,
            { "sType": "uk_date" },
            null,
            null
          ],
    	columnDefs: [{ type: 'natural', targets: 0 }]
    } );
} );

$(document).ready( function () {
    $('#dataTableSeance').DataTable( {
      "language": {
      	"sProcessing":     "Traitement en cours...",
      	"sSearch":         "Rechercher&nbsp;:",
        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
      	"sInfo":           "Affichage des &eacute;lements _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
      	"sInfoEmpty":      "Affichage des &eacute;lements 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
      	"sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
      	"sInfoPostFix":    "",
      	"sLoadingRecords": "Chargement en cours...",
        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
      	"sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
      	"oPaginate": {
      	"sFirst":      "Premier",
      	"sPrevious":   "Pr&eacute;c&eacute;dent",
      	"sNext":       "Suivant",
      	"sLast":       "Dernier"
      	},
      	"oAria": {
      		"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
      		"sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
      	}
      },
      // Définition des collones ou le tri jj/mm/aaaa sera appliqué (voir fichier date-uk.js)
      "aoColumns": [
			null,
			null,
            { "sType": "uk_date" },
            null,
            null,
            null,
            null,
            null
          ],
    	columnDefs: [{ type: 'natural', targets: 0 }]
    } );
} );

$(document).ready( function () {
    $('#dataTableScenario').DataTable( {
      "language": {
      	"sProcessing":     "Traitement en cours...",
      	"sSearch":         "Rechercher&nbsp;:",
        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
      	"sInfo":           "Affichage des &eacute;lements _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
      	"sInfoEmpty":      "Affichage des &eacute;lements 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
      	"sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
      	"sInfoPostFix":    "",
      	"sLoadingRecords": "Chargement en cours...",
        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
      	"sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
      	"oPaginate": {
      	"sFirst":      "Premier",
      	"sPrevious":   "Pr&eacute;c&eacute;dent",
      	"sNext":       "Suivant",
      	"sLast":       "Dernier"
      	},
      	"oAria": {
      		"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
      		"sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
      	}
      },
      // Définition des collones ou le tri jj/mm/aaaa sera appliqué (voir fichier date-uk.js)
      "aoColumns": [
			null,
			null,
            null,
            null,
            null,
            null,
            null
          ],
    	columnDefs: [{ type: 'natural', targets: 0 }]
    } );
} );
$(document).ready( function () {
    $('#dataTableSequences').DataTable( {
      "language": {
      	"sProcessing":     "Traitement en cours...",
      	"sSearch":         "Rechercher&nbsp;:",
        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
      	"sInfo":           "Affichage des &eacute;lements _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
      	"sInfoEmpty":      "Affichage des &eacute;lements 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
      	"sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
      	"sInfoPostFix":    "",
      	"sLoadingRecords": "Chargement en cours...",
        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
      	"sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
      	"oPaginate": {
      	"sFirst":      "Premier",
      	"sPrevious":   "Pr&eacute;c&eacute;dent",
      	"sNext":       "Suivant",
      	"sLast":       "Dernier"
      	},
      	"oAria": {
      		"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
      		"sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
      	}
      },
      // Définition des collones ou le tri jj/mm/aaaa sera appliqué (voir fichier date-uk.js)
      "aoColumns": [
            null,
            null,
            null,
            null
          ],
    	columnDefs: [{ type: 'natural', targets: 0 }]
    } );
} );
$(document).ready( function () {
    $('#dataTableStimulis').DataTable( {
      "language": {
      	"sProcessing":     "Traitement en cours...",
      	"sSearch":         "Rechercher&nbsp;:",
        "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
      	"sInfo":           "Affichage des &eacute;lements _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
      	"sInfoEmpty":      "Affichage des &eacute;lements 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
      	"sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
      	"sInfoPostFix":    "",
      	"sLoadingRecords": "Chargement en cours...",
        "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
      	"sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
      	"oPaginate": {
      	"sFirst":      "Premier",
      	"sPrevious":   "Pr&eacute;c&eacute;dent",
      	"sNext":       "Suivant",
      	"sLast":       "Dernier"
      	},
      	"oAria": {
      		"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
      		"sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
      	}
      },
      // Définition des collones ou le tri jj/mm/aaaa sera appliqué (voir fichier date-uk.js)
      "aoColumns": [
            null,
            null,
            null,
            null
          ],
    	columnDefs: [{ type: 'natural', targets: 0 }]
    } );
} );
