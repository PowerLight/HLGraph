/**
* Gestion du menu de séléction des motifs sur les graphs pour l'enregistrement en base
*
* Librairies : JavaScript, jQuery et d3js
* d3.js est une librarie qui permet de manipuler le DOM et qui est efficace dans la manipulation de SVG.
*
* @author Romain Léguillon
*/
function degreFahrenheit2Celsius(faren){
    return (faren-32)*5/9;//Formule de conversion
}

function conversionMeteoAnglaise(resume){
    if (resume=="Clear") return "Ensoleillé";
    else if (resume=="Mostly Cloudy") return "Très nuageux";
    else if (resume=="Partly Cloudy") return "Eclaircies";
    else if (resume=="Overcast") return "Couvert";
    return resume;
}


$(document).ready(function() {

/*
Chargement de la liste des spectres pour la sélection lors de la saisie des motifs
*/
function captorData(){
    var classMotif=$('.askModifMotif');
    var i=0;
    while (classMotif[i]!=null){
        $(".modifMotifForm[motifId=\""+ classMotif[i].attributes.attr.nodeValue +"\"]").css('display','none');
        i+=1;
    }
    //Conversion des températures
    classMotif=$('.temperature');
    var tmp;
    i=0;
    while (classMotif[i]!=null){
        tmp=classMotif[i].children;
        tmp[1].innerText = (degreFahrenheit2Celsius( tmp[0].innerText )+"").substring(0, 5);
        i+=1;
    }
    //Traduction des conditions météo
    classMotif=$('.resume');
    i=0;
    while (classMotif[i]!=null){
        tmp=classMotif[i].children;
        tmp[1].innerText = conversionMeteoAnglaise( tmp[0].innerText );
        i+=1;
    }
}

    dataset.captor_data.forEach(function (k, i) {
      var legendChoixSignal = "<span style=\"color:red !important;\">----- </span> ";
      d3.select("#id_choix_signaux")
        .append("option")
            .attr("id", "id_choix_signaux-" + k.captor_id)
            .attr("value", k.captor_id)
            .style("background-color", lineColor(i))
            .text(captorName(k.captor_id));
    var classMotif=$('.askModifMotif');
        var j=0;
        while (classMotif[j]!=null){
            d3.select(".modifMotifForm[motifId=\""+ classMotif[j].attributes.attr.nodeValue +"\"] > .id_choix_signaux")
            .append("option")
               .attr("id", "id_choix_signaux-" + k.captor_id)
               .attr("value", k.captor_id)
               .style("background-color", lineColor(i))
               .text(captorName(k.captor_id));
            j+=1;
        }
    });


    function convIdCaptor2CaptorName(index,k){
        var id_sig=$('.IdCaptor .hide').get(index).innerText;
        for (var i = 0; dataset.captor_data[i]!= null; i++){
            if(dataset.captor_data[i].captor.captorId == id_sig){
                k.children[1].innerText = dataset.captor_data[i].captor.description;
                i=100;
            }
        }
        if(i<100){
            var id_ligne='#'+k.children[1].innerText;
            $(id_ligne)[0].hidden=true;
        }
    }

    function initListeSelect(){
        captorData();
        var captorN = $('.IdCaptor').get();
        captorN.forEach(function(k,i){
            convIdCaptor2CaptorName(i,k);
        });
    }
    initListeSelect();


    function initSelection(){
        $('#x_debut').val('');//mise à null de l'interval de temps
        $('#x_fin').val('');
        $('#spindle-complexeK').css("display","none")//cacher la sélection du type de motif, du signal, des bouton d'envoi du formulaire
    }
    //Le bouton "Désectionner" permet de sélectionner à nouveau l'interval de temps => saisie d'un motif sur les courbes
    $('#unselect').on('click',function(){
        initSelection();
        $('#interval-text').val('Sélection éffacée');
    })

/*
Transmission ajaxisée des informations concernant les motifs
*/
    $('#monForm').on('submit', function(e) {
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire

        var $this = $(this);
        var interval_text    = $('#interval-text').val();
        var x_debut          = $('#x_debut').val();
        var x_fin            = $('#x_fin').val();
        var id_choix_signaux = $('#id_choix_signaux').val();
        var id_choix_motif   = $('#id_choix_motif').val();

        if(!(id_choix_motif === '' || id_choix_signaux === '' || interval_text === '' || x_debut === '' || x_fin === '')) {
            $.ajax({
                type: $this.attr('method'),
                data: $this.serialize(),
                url: $this.attr('action'),
                error : function(resultat, statut, erreur){
                    $('#interval-text').val('Erreur, le motif n\'a pas été enregistré !');
                },
                success: function(){
                    location.reload();
                }
            });
            addNewMotif();
            initSelection();
            $('#interval-text').val('Envoi réussi');
        }
    });

    //Ajout du nouveau motif dans le tableau récapitulatif sous le graph
    function addNewMotif(){
        var td_mo=document.createElement("TD");
        td_mo.innerText=$('#id_choix_motif').val();
        var td_si=document.createElement("TD");
        td_si.innerText=convIdCaptor2CaptorName($('#id_choix_signaux').val());
        var td_xd=document.createElement("TD");
        td_xd.innerText=$('#x_debut').val();
        var td_xf=document.createElement("TD");
        td_xf.innerText=$('#x_fin').val();
        var tr=document.createElement("TR");
        var tableauRecap=$('#visuListMotif');
        tr.appendChild(td_mo);
        tr.appendChild(td_si);
        tr.appendChild(td_xd);
        tr.appendChild(td_xf);
        tr.appendChild(document.createElement("TD"));
        tr.appendChild(document.createElement("TD"));
        tr.appendChild(document.createElement("TD"));
        tableauRecap.append(tr);
    }


//Suppression de l'enregistrement
    $('.deleteMotifForm').on('click', function(e) {
        if (confirm('êtes-vous sûr de vouloir supprimer ce motif ?')) {
        } else {
            return;
        }
        e.preventDefault();
        var thisM=$(this);
        $.ajax({
            type: $(this).attr('method'),
            data: $(this).serialize(),
            url: $(this).attr('action'),
            error : function(resultat, statut, erreur){
                console.log(erreur);
            },
            success : function(){//effacement de la ligne  concernée
                    var thisP=thisM.parent();
                    var tmp;
                    do{
                        thisM=thisP;
                        thisP=thisM.parent();
                        tmp=thisM["0"].outerHTML.toString();
                    }while(!tmp.startsWith('<td'));
                    thisP.css('display','none');
            }
        });
    });

//Affichage du formulaire de modification
    $('.askModifMotif').on('click', function(e) {
        e.preventDefault();
        captorData();
        $(".modifMotifForm[motifId=\""+$(this).attr('attr')+"\"]").css('display','inline');
    });
    //
//Mofification de l'enregistrement
    $('.modifMotifForm').on('submit', function(e) {
        e.preventDefault();
        var thisM=$(this);
        var selectedLine = thisM.parent().parent();
        var classMotif=$('.askModifMotif');
        var i=-1;
        $.ajax({
            type: $(this).attr('method'),
            data: $(this).serialize(),
            url: $(this).attr('action'),
            error : function(resultat, statut, erreur){
                console.log(erreur);
                alert(erreur);
            },
            success : function(){//Modification de l'affichage de la ligne modifiée
                 selectedLine[0].children[0].innerText=thisM[0].children[5].value;
                 selectedLine[0].children[1].children[0].innerText=thisM[0].children[6].value;
                 initListeSelect();
                 while (classMotif[i]!=null){
                     $(".modifMotifForm[motifId=\""+ classMotif[i].attributes.attr.nodeValue +"\"]").css('display','none');
                     i+=1;
                 }
            }
        });
    });
});
