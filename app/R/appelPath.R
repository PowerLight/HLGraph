if (!"signal" %in% rownames(installed.packages())) {
  install.packages("signal", repos = 'http://cran.univ-paris1.fr/')
}

library(signal)
if (!"stats" %in% rownames(installed.packages())) install.packages("stats", repos = 'http://cran.univ-paris1.fr/')
library(stats)
if (!"GeneCycle" %in% rownames(installed.packages())) install.packages("GeneCycle", repos = 'http://cran.univ-paris1.fr/')
library(GeneCycle)
if (!"pracma" %in% rownames(installed.packages())) install.packages("pracma", repos = 'http://cran.univ-paris1.fr/')
library(pracma)
if (!"mongolite" %in% rownames(installed.packages())) install.packages("mongolite", repos = 'http://cran.univ-paris1.fr/')
library(mongolite)
if (!"matrixcalc" %in% rownames(installed.packages())) install.packages("matrixcalc", repos = 'http://cran.univ-paris1.fr/')
library(matrixcalc)
if (!"jsonlite" %in% rownames(installed.packages())) install.packages("jsonlite", repos = 'http://cran.univ-paris1.fr/')
library(jsonlite)

cheminConnect = ("app/R/decoupage30sEtFFT.R")
cheminFFT = paste(cheminCourant, cheminConnect, sep="/")
#cheminMongo = paste("D:/HLGraph", cheminConnect, sep="/")
print(cheminFFT)
source(cheminFFT)

fichierReconstitution = ("app/R/reconstitution.R")
cheminReconstitution = paste(cheminCourant, fichierReconstitution, sep="/")
#cheminMongo = paste("D:/HLGraph", cheminConnect, sep="/")
print(cheminReconstitution)
source(cheminReconstitution)

args = commandArgs(TRUE)

if(length(args)==0){
  print("No arguments supplied.")
  
}else{
  for(i in 1:length(args)){

      tab<-strsplit(args[[i]], "=", fixed=TRUE)

      var<-tab[[1]][1]
      val=tab[[1]][2]
     assign(var, val)

  }
}




print(numUniqueSeance)
print(patient_id)
print(captor_id)

#mongoconn<-connect()



leSignalDecoupeEtTransformeMatrice<-decoupage(strtoi(captor_id), numUniqueSeance)

resultat<-data.frame()
resultat[1, "numeroUniqueDeSeance"]<-numUniqueSeance
resultat[1, "patient_id"]<-patient_id
resultat[1, "lesSequences"]<-"[]"

 #server
 #mongoconn<-mongo(url="mongodb://handimong:t3F5n7WS@localhost:55077/handiLight", collection="analyzedData", db="handiLight")
 #local
 mongoconn<-mongo(url="mongodb://localhost/handiLight", collection="analyzedData", db="handiLight")
 
 
mongoconn$insert(resultat)

quer<-paste0('{"numeroUniqueDeSeance" : "',numUniqueSeance,'"}')
upd<-paste0('{"$set": {"lesSequences" : ',toJSON(leSignalDecoupeEtTransformeMatrice, auto_unbox=TRUE), '}}')
print(quer)
print(upd)
mongoconn$update(query=quer, update=upd)


#Reconstitution du signal

#test<-reconstitution(mongoconn, "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")


