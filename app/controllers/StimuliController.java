package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import model.new_database.Stimulus;
import model.StimuliType;
import org.jongo.MongoCursor;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.creationStimuli;
import views.html.listeStimulis;
import views.html.modifierStimuli;

import java.util.ArrayList;

public class StimuliController extends Controller {

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result listeStimulis() {
        MongoCursor<Stimulus> stimulis = new Stimulus().find("");
        ArrayList<Stimulus> stimuliList = new ArrayList<>();
        for (Stimulus unStimuli: stimulis){
            stimuliList.add(unStimuli);
        }
//        Logger.debug(stimulis.count()+"");
        return ok(listeStimulis.render(stimuliList));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result creationStimuli() {
        MongoCursor<StimuliType> lesStimuliTypes= new StimuliType().find("");
        ArrayList<StimuliType> list = new ArrayList<>();
        for (StimuliType unStimuliType : lesStimuliTypes){
            list.add(unStimuliType);
        }
        return ok(creationStimuli.render(list));
    }
    //----------
    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result addStimuli(){
        Form<Stimulus> stimuliForm = Form.form(Stimulus.class).bindFromRequest();
        Stimulus unStimuli = stimuliForm.get();
        unStimuli.setColor(((StimuliType) new StimuliType().findOneById(stimuliForm.data().get("stimuliType_id"))).getName());
        //TODO same as roles with permissions
        unStimuli.save();
        return redirect(routes.StimuliController.listeStimulis());
    }
//----------

    //----------
    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result modifyStimuli() {
        Form<Stimulus> modifyStimuliForm = Form.form(Stimulus.class).bindFromRequest();
        Stimulus leStimuli = new Stimulus();
        ArrayList<StimuliType> leStimuliType = new ArrayList<>();

        MongoCursor<StimuliType> cursor = new StimuliType().find("");
        for (StimuliType unStimuliType : cursor){
            leStimuliType.add(unStimuliType);
        }
        leStimuli = (Stimulus) leStimuli.findOneById(modifyStimuliForm.data().get("_id"));

        if (modifyStimuliForm.data().get("stimuliType").compareTo("") == 0) {
            flash("error", "La couleur ne peut être vide.");
            return forbidden(modifierStimuli.render(
                    leStimuli, leStimuliType));
        } else {
            leStimuli.setColor(modifyStimuliForm.data().get("stimuliType"));
        }

        leStimuli.setIntensity(Integer.parseInt(modifyStimuliForm.data().get("intensity")));
        leStimuli.setFrequency(Integer.parseInt(modifyStimuliForm.data().get("frequency")));

        //TODO same as roles with permissions
        leStimuli.save();
        return redirect(routes.StimuliController.listeStimulis());
    }
    //----------
    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result modifierStimuli(String _id) {
        Stimulus leStimuli = new Stimulus();
        leStimuli = (Stimulus)leStimuli.findOneById(_id);

        MongoCursor<StimuliType> lesStimuliTypes= new StimuliType().find("");
        ArrayList<StimuliType> list = new ArrayList<>();
        for (StimuliType unStimuliType : lesStimuliTypes){
            list.add(unStimuliType);
        }
        return ok(modifierStimuli.render(leStimuli, list));
    }
    //----------
    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result deleteStimuli(String _id) {
        Stimulus leStimuli = new Stimulus();
        leStimuli=(Stimulus) leStimuli.findOneById(_id);
        leStimuli.remove();
        return redirect(routes.StimuliController.listeStimulis());
    }

}
