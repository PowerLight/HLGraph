package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.fasterxml.jackson.databind.JsonNode;
import model.*;
import model.new_database.Patient;
import org.jongo.MongoCursor;
import play.Play;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.*;

import java.io.File;
import java.util.List;

public class Application extends Controller {

    public Result analyseDesDonnees(String patient_id, String numeroUniqueDeSeance, int captor_id){

        try{
            MongoCursor lesData =  new AnalyzedData().findWithLimit("", 1);
            if(!lesData.hasNext()){
                String CHEMIN;
                String OS = System.getProperty("os.name").toLowerCase();
                if(OS.indexOf("win") >= 0){
                    CHEMIN = Play.application().path().getAbsolutePath()+File.separator+"app"+File.separator+"R"+File.separator;
                }else{
                    CHEMIN = "/var/www/activator/HLGraph/app/R/";
                }
                String[] commande = { "R", "CMD", "BATCH", "--no-save", "--no-restore", "--args patient_id="+patient_id+" captor_id="+captor_id+" numUniqueSeance="+numeroUniqueDeSeance, CHEMIN+"appelPath.R"};
                Process p = Runtime.getRuntime().exec(commande);
                p.waitFor();
                p.destroy();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return redirect(routes.RestitutionController.restitution(0, 30000, numeroUniqueDeSeance,  patient_id,30));
    }

    public Result index(){
        session().remove("login");
        session().remove("userFirstName");
        session().remove("userName");
        return ok(index.render(Form.form(User.class)));
    }

    public Result authenticate(){
        Form<User> loginForm = Form.form(User.class).bindFromRequest();
        String login = loginForm.get().login;
        String password = loginForm.data().get("userPassword");
        User unUser = new User();
        if (!unUser.authenticate(login, password)){
            flash("error", "Nom d'utilisateur ou mot de passe incorrect.");
            return forbidden(index.render(Form.form(User.class)));
        }
        session().clear();
        unUser=unUser.findByLogin(login);
        session("login", login);
        session("userFirstName", unUser.userFirstName);
        session("userName", unUser.userName);
        flash("success", "Utilisateur "+unUser.userFirstName+" "+unUser.userName+" connecté.");
        return redirect(routes.Application.accueil());
    }

    @Security.Authenticated(Secured.class)
    public Result accueil() {
        return ok(accueil.render());
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("admin"))
    public Result updateMotifs() {
        List<Patient> patients = Patient.findAll(null);
        MotifTypeLearn monType;
        List<MotifTypeLearn> motifTypeLearns1= MotifTypeLearn.findAll(null);
        for(MotifTypeLearn monTypetmp: motifTypeLearns1){
            monTypetmp.supprimerMotif();
            monTypetmp.save();
        }
        for(Patient unPatient : patients)if(unPatient.getLesSeances().size()>0){
            for(SeanceMongo uneSeance: unPatient.getLesSeances())if(uneSeance.lesMotifs.size()>0){
                uneSeance.ordonnerMotif();
                for(MotifMongo unMotif: uneSeance.lesMotifs){
                    monType= new MotifTypeLearn().find1Type(unMotif.motifName);
                    if(null==monType)monType=new MotifTypeLearn(unMotif);
                    monType.fill(unPatient,uneSeance,unMotif);
                    monType.save();
                }
            }
        }
        return ok(accueil.render());
    }

    public Result updateSvg() {
        JsonNode json = request().body().asJson();
        long start = json.findPath("start").asLong();
        long stop = json.findPath("stop").asLong();
        String numeroUniqueDeSeance = json.findPath("numeroUniqueDeSeance").asText();
        String patient_id = json.findPath("patient_id").asText();
        Patient patient = new Patient().findOne(patient_id);
        DataSet leDataset = new DataSet().findOneBySeanceIdBetweenTimestampFiltered(start, stop,patient, numeroUniqueDeSeance);
        if (json == null) {
            return badRequest("Expecting Json data");
        } else {
            return ok(JsonTools.toJson(leDataset)).as("application/json");
        }
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result etablissementsequence()   {
        return ok(etablissementsequence.render("etablissementsequence"));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result etablissementLumineuse()  {
        return ok(etablissementLumineuse.render("etablissementLumineuse"));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result etablissementParadigme()  {
        return ok(etablissementParadigme.render("etablissementParadigme"));
    }
}