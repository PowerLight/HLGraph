package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import model.HashUtils;
import model.SecurityRole;
import model.User;
import org.jongo.MongoCursor;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.creationCompte;
import views.html.gestionCompte;
import views.html.modifierCompte;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rcollas on 10/08/2016.
 */
public class UserController extends Controller{

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("admin"))
    public Result creationCompte() {
        return ok(creationCompte.render());
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("admin"))
    public Result modifierCompte(String _id) {
        User leUser = new User();
        leUser = (User)leUser.findOneById(_id);
        return ok(modifierCompte.render(Form.form(User.class), leUser));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("admin"))
    public Result gestionCompte(){
        User u = new User();
        final MongoCursor<User> users = u.find(null);
        List<User> list = new ArrayList<User>();
        for (User user: users)
            list.add(user);
        return ok(gestionCompte.render(list));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("admin"))
    public Result deleteUser(String _id) {
        User leUser = new User();
        leUser=(User) leUser.findOneById(_id);
        leUser.remove();
        return redirect(routes.UserController.gestionCompte());
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result modifyUser() {
        Form<User> modifyUserForm = Form.form(User.class).bindFromRequest();
        String login = modifyUserForm.data().get("login");
        String password = modifyUserForm.data().get("userOldPassword");
        User leUser = new User();
        leUser = (User) leUser.findOneById(modifyUserForm.data().get("_id"));
        //check login not null
        if (login.compareTo("") == 0) {
            flash("error", "Le login ne peut être vide.");
            return forbidden(modifierCompte.render(Form.form(User.class), leUser));
        } else {
            leUser.login = modifyUserForm.data().get("login");
        }
//        //check login and password to be change
//        if (!leUser.authenticate(login, password)){
//            flash("error", "Ancien mot de passe incorrect pour ce login d'utilisateur.");
//            return forbidden(modifierCompte.render(Form.form(User.class), leUser));
//        }
        leUser.userName = modifyUserForm.data().get("userLastName");
        leUser.userFirstName = modifyUserForm.data().get("userFirstname");
        leUser.roles.clear();
        if(modifyUserForm.get().rolesList.size()>0) {
            for (String role : modifyUserForm.get().rolesList) {
                leUser.roles.add(new SecurityRole(role));
            }
        }
        HashUtils myHashUtils = new HashUtils();
        leUser.userPassword = myHashUtils.createPassword(modifyUserForm.data().get("userPassword"));
        //TODO same as roles with permissions
        leUser.save();
        return redirect(routes.UserController.gestionCompte());
    }

    @Security.Authenticated(Secured.class)
    public Result addUser(){
        Form<User> addUserForm = Form.form(User.class).bindFromRequest();
        User unUser = new User();
        //check login not null
        if(addUserForm.data().get("login").compareTo("")==0){
            flash("error", "Le login ne peut être vide.");
            return forbidden(creationCompte.render());
        }else{

            unUser.login = addUserForm.data().get("login");

        }
        unUser.userFirstName = addUserForm.data().get("userFirstname");
        unUser.userName = addUserForm.data().get("userLastName");
        if(addUserForm.get().rolesList.size()>0) {
            for (String role : addUserForm.get().rolesList) {
                unUser.roles.add(new SecurityRole(role));
            }
        }
        HashUtils myHashUtils = new HashUtils();
        unUser.userPassword = myHashUtils.createPassword(addUserForm.data().get("userPassword"));
        //check login unique
        MongoCursor<User> cursor = unUser.find("{login : '"+addUserForm.data().get("login")+"'}");
        if(cursor.count()==1){
            flash("error", "Ce login est déjà utilisé. Veuillez en choisir un différent.");
            return forbidden(creationCompte.render());
        }
        //TODO same as roles with permissions
        unUser.save();
        return redirect(routes.UserController.gestionCompte());
    }
}
