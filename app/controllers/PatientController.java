package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import model.new_database.Patient;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.creationPatient;
import views.html.listePatients;
import views.html.modifierPatient;

import java.util.List;

/**
 * Created by rcollas on 10/08/2016.
 */
public class PatientController extends Controller{

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result listePatients() {

        List<Patient> patients = Patient.findAll(null);
        return ok(listePatients.render(patients));
    }


    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result creationPatient() {
        return ok(creationPatient.render());
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result modifierPatient(String _id) {
        Patient lePatient = new Patient();
        lePatient = (Patient)lePatient.findOneById(_id);
        return ok(modifierPatient.render(Form.form(Patient.class), lePatient));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result deletePatient(String _id) {
        Patient lePatient = new Patient();
        lePatient=(Patient) lePatient.findOneById(_id);
        lePatient.remove();
        return redirect(routes.PatientController.listePatients());
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result modifyPatient() {
        Form<Patient> modifyPatientForm = Form.form(Patient.class).bindFromRequest();
        Patient lePatient = new Patient();
        lePatient = (Patient) lePatient.findOneById(modifyPatientForm.data().get("_id"));
        if (modifyPatientForm.data().get("lastname").compareTo("") == 0) {
            flash("error", "Le nom ne peut être vide.");
            return forbidden(modifierPatient.render(Form.form(Patient.class), lePatient));
        } else {
            lePatient.setLastname(modifyPatientForm.data().get("lastname"));
        }
        lePatient.setLastname(modifyPatientForm.data().get("lastname"));
        lePatient.setFirstname(modifyPatientForm.data().get("firstname"));


        //TODO same as roles with permissions
        lePatient.save();
        return redirect(routes.PatientController.listePatients());
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result addPatient(){
        Form<Patient> patientForm = Form.form(Patient.class).bindFromRequest();
        Patient unPatient = patientForm.get();
        //TODO same as roles with permissions
        unPatient.save();
        return redirect(routes.PatientController.listePatients());
    }

}
