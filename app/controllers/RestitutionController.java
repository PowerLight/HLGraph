package controllers;


import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.*;
import model.new_database.Patient;
import org.jongo.MongoCursor;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.*;

import play.data.Form;

import java.util.ArrayList;
import java.util.List;

public class RestitutionController extends Controller{

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result restitution(long start, long stop, String numeroUniqueDeSeance, String patientMongoId, int shift) {
        Patient lePatient = (Patient) new Patient().findOneById(patientMongoId);
        int monIndex = lePatient.findSeanceIndex(numeroUniqueDeSeance);
        ScenarioMongo leScenario = lePatient.getLesSeances().get(monIndex).scenario;
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(MapperFeature.USE_ANNOTATIONS);
        Json.setObjectMapper(mapper);

        String dataset = Json.toJson(lePatient.getLesSeances().get(monIndex).dataset.findOneBySeanceIdBetweenTimestampFiltered(start, stop, lePatient, numeroUniqueDeSeance)).toString();
        for(CaptorData unCaptorData : lePatient.getLesSeances().get(monIndex).dataset.captor_data){
            unCaptorData.measures.clear();
        }
        MongoCursor<AnalyzedData> cursor =  new AnalyzedData().find("{numeroUniqueDeSeance : '"+lePatient.getLesSeances().get(monIndex).numeroUniqueDeSeance+"'}");
        AnalyzedData analyzedData = new AnalyzedData();
        if (cursor.hasNext()){
            analyzedData = cursor.next();
        }else{
            analyzedData = null;
        }
        lePatient.getLesSeances().get(monIndex).setMeteoData();
        List<SequenceMongo> lesSequenceMongos = leScenario.sequences;
        lePatient.update("lesSeances", lePatient.getLesSeances());
        return ok(restitution.render(dataset, analyzedData, lePatient, monIndex, leScenario, Json.toJson(lesSequenceMongos).toString()));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result reloadRestitution(long start, long stop, String numeroUniqueDeSeance,String patientMongoId, int shift) {
        return redirect(routes.RestitutionController.restitution( start, stop, numeroUniqueDeSeance,patientMongoId,shift));
    }


    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result deleteMotifRestitution() {
        Form<MotifDelete> motifDeleteForm = Form.form(MotifDelete.class).bindFromRequest();
        Patient lePatient = new Patient();
        lePatient = (Patient)lePatient.findOneById(motifDeleteForm.get().patientId);
        SeanceMongo laSeance;
        laSeance = (SeanceMongo)lePatient.findSeance(motifDeleteForm.get().sceanceId);
        if( laSeance.supprimerMotif( motifDeleteForm.get().motifId ) ){
            lePatient.save();
            return ok();
        }
        return badRequest("data:'Motif introuvable'");
    }


    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result modifMotifRestitution() {
        //Mémorisation des infos de la sélections
        Form<MotifPattern> motifPatternForm = Form.form(MotifPattern.class).bindFromRequest();
        motifPatternForm.get().user_login=session().get("login");
        //Mémorisation des ids pour retrouver l'enregistrement précédent
        Form<MotifDelete> motifDeleteForm = Form.form(MotifDelete.class).bindFromRequest();

        Patient lePatient = new Patient();
        lePatient = (Patient)lePatient.findOneById(motifDeleteForm.get().patientId);//le patient concerné
        SeanceMongo laSeance;// = new SeanceMongo();
        laSeance = (SeanceMongo)lePatient.findSeance(motifDeleteForm.get().sceanceId);//la séance du patient où est l'enregistrement
        if(laSeance.recrireMotif( motifDeleteForm.get().motifId, motifPatternForm.get() )){
            lePatient.save();
            return ok();
        }
        return badRequest("data:'Motif introuvable'");
    }



    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result enregistrementBase() {
        //Memorisation des données de sélection dans le model MotifPattern
        Form<MotifPattern> motifPatternForm = Form.form(MotifPattern.class).bindFromRequest();
        motifPatternForm.get().user_login=session().get("login");//rajout dans le model du nom de l'utilisateur
        MotifMongo monMotifMongo = new MotifMongo();
        monMotifMongo.fillByMotifPattern(motifPatternForm.get());
        monMotifMongo.lastUpdate = System.currentTimeMillis();
        Patient lePatient = new Patient();
        lePatient = (Patient)lePatient.findOneById(motifPatternForm.get().patientId);
        SeanceMongo laSeance = new SeanceMongo();
        laSeance = lePatient.findSeance(motifPatternForm.get().sceanceId);
        laSeance.lesMotifs.add(monMotifMongo);
        laSeance.createUUIDs();

        lePatient.save(); //enregistrement des modifications de la table
        return ok();//temporel
    }


    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result restitutionSingle(long start, long stop, String numeroUniqueDeSeance, String patientMongoId, int captor) {

        Patient lePatient = (Patient) new Patient().findOneById(patientMongoId);
        int monIndex = lePatient.findSeanceIndex(numeroUniqueDeSeance);
        ScenarioMongo leScenario = lePatient.getLesSeances().get(monIndex).scenario;
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(MapperFeature.USE_ANNOTATIONS);
        Json.setObjectMapper(mapper);
        if(captor<0)captor=0;
        else if(captor >= lePatient.getLesSeances().get(monIndex).dataset.captor_data.size()){
            captor = lePatient.getLesSeances().get(monIndex).dataset.captor_data.size()-1;
        }
        String dataset = Json.toJson(lePatient.getLesSeances().get(monIndex).dataset.findOneBySeanceIdBetweenTimestampSingle(start, stop, lePatient, numeroUniqueDeSeance,captor)).toString();
        for(CaptorData unCaptorData : lePatient.getLesSeances().get(monIndex).dataset.captor_data){
            unCaptorData.measures.clear();
        }
        List<SequenceMongo> lesSequenceMongos = leScenario.sequences;
        return ok(restitutionSingle.render(dataset, lePatient, monIndex, Json.toJson(lesSequenceMongos).toString()));
    }


}

