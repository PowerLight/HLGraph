package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import model.new_database.Doctor;
import org.jongo.MongoCursor;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.creationMedecin;
import views.html.listeMedecins;
import views.html.modifierMedecin;


import java.util.ArrayList;
import java.util.List;

public class MedecinController extends Controller {

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result listeMedecins(){

        final MongoCursor<Doctor> lesMedecins = new Doctor().find("");
        List<Doctor> list = new ArrayList<Doctor>();
        for (Doctor doctor : lesMedecins)
            list.add(doctor);
        return ok(listeMedecins.render(list));

    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result creationMedecin() {
        return ok(creationMedecin.render());
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result addMedecin(){
        Form<Doctor> medecinForm = Form.form(Doctor.class).bindFromRequest();
        Doctor unDoctor = medecinForm.get();
        //TODO same as roles with permissions
        unDoctor.save();
        return redirect(routes.MedecinController.listeMedecins());
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result modifierMedecin(String _id){
        Doctor leDoctor = new Doctor();
        leDoctor = (Doctor) leDoctor.findOneById(_id);
        return ok(modifierMedecin.render(Form.form(Doctor.class), leDoctor));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result medecinSave(){
        //Logger.debug("je suis avant la condition");
        Form<Doctor> medecinForm = Form.form(Doctor.class).bindFromRequest();
        Doctor leDoctor = medecinForm.get();

        if (leDoctor._id != null){
            leDoctor = (Doctor) leDoctor.findOneById(medecinForm.data().get("_id"));
            leDoctor = medecinForm.get();
        }
        leDoctor.save();
        return redirect("/listeMedecins");
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result medecinRemove(String _id) {
        Doctor leDoctor = (Doctor) new Doctor().findOneById(_id);
        leDoctor.remove();
        return redirect("/listeMedecins");
    }

}
