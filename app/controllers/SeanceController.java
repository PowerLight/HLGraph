package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import model.*;
import model.new_database.Patient;
import model.new_database.Captor;
import model.new_database.Captor;
import org.jongo.MongoCursor;
import play.Logger;
import play.Play;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.creationSeance;
import views.html.listeSeancesPatient;
import views.html.modifierSeance;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class SeanceController extends Controller {


    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result listeSeancesPatient(String _id) {

        Patient p = new Patient();
        final Patient monPatient =  p.findOne(_id);

        //Logger.debug("lesSeances = " + monPatient.lesSeances.get(0));
        return ok(listeSeancesPatient.render(_id, monPatient.getFirstname(), monPatient.getLastname(), monPatient.getLesSeances()));
    }
    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result creationSeance(String _id, String firstName, String lastName, Integer seanceNumber) {
        MongoCursor<ScenarioMongo> scenarios = new ScenarioMongo().find("");
        ArrayList<ScenarioMongo> listScenarios = new ArrayList<ScenarioMongo>();
        for (ScenarioMongo scenario : scenarios) {
            listScenarios.add(scenario);
        }

        return ok(creationSeance.render(_id, firstName, lastName, seanceNumber, listScenarios));
    }
    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result addSeance(){
        Form<SeanceMongo> seanceMongoForm = Form.form(SeanceMongo.class).bindFromRequest();
        SeanceMongo uneSeanceMongo = seanceMongoForm.get();

        ScenarioMongo leScenarioMongo = (ScenarioMongo) new ScenarioMongo().findOneById(uneSeanceMongo.scenarioMongoId);
        uneSeanceMongo.scenario_id = leScenarioMongo.scenarioId;

        uneSeanceMongo.status = "NOT_STARTED";

        Patient lePatient = new Patient();
        lePatient = (Patient)lePatient.findOneById(uneSeanceMongo.patientMongoId);
        lePatient.getLesSeances().add(uneSeanceMongo);
        lePatient.createUUIDs();
        lePatient.save();
        return redirect(routes.SeanceController.listeSeancesPatient(uneSeanceMongo.patientMongoId));
    }


    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result modifierSeance(String _id, int seanceNumber) {
        Patient lePatient = new Patient();
        lePatient = (Patient)lePatient.findOneById(_id);

        int index = seanceNumber-1;

        MongoCursor<ScenarioMongo> scenarios = new ScenarioMongo().find("");
        ArrayList<ScenarioMongo> listScenarios = new ArrayList<ScenarioMongo>();
        for (ScenarioMongo scenario : scenarios) {
            listScenarios.add(scenario);
        }
        return ok(modifierSeance.render(lePatient, listScenarios, seanceNumber, lePatient.getLesSeances().get(index).date_start, lePatient.getLesSeances().get(index).time_start, lePatient.getLesSeances().get(index).scenarioMongoId));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result saveSeance() {
        Form<SeanceMongo> seanceMongoForm = Form.form(SeanceMongo.class).bindFromRequest();
        SeanceMongo uneSeanceMongo = seanceMongoForm.get();

        Patient lePatient = new Patient();
        lePatient = (Patient)lePatient.findOneById(seanceMongoForm.data().get("_id"));

        ScenarioMongo leScenarioMongo = (ScenarioMongo) new ScenarioMongo().findOneById(uneSeanceMongo.scenarioMongoId);
        uneSeanceMongo.scenario_id = leScenarioMongo.scenarioId;

        int index = uneSeanceMongo.seanceNumber-1;
        lePatient.getLesSeances().get(index).date_start = uneSeanceMongo.date_start;
        lePatient.getLesSeances().get(index).scenario_id = uneSeanceMongo.scenario_id;
        lePatient.getLesSeances().get(index).scenarioMongoId = uneSeanceMongo.scenarioMongoId;
        lePatient.getLesSeances().get(index).time_start = uneSeanceMongo.time_start;

        lePatient.save();

        return redirect(routes.SeanceController.listeSeancesPatient(lePatient._id));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result deleteSeance(String _id, int seanceNumber) {
        Patient lePatient = new Patient();
        lePatient=(Patient) lePatient.findOneById(_id);
        int index = seanceNumber - 1;
        lePatient.getLesSeances().remove(index);
        lePatient.save();
        return redirect(routes.SeanceController.listeSeancesPatient(_id));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result exportJSON(String patient_id, String numeroUniqueDeSeance) {
        Patient patient = new Patient().findOne(patient_id);
        SeanceMongo laSeance = patient.getLesSeances().get(patient.findSeanceIndex(numeroUniqueDeSeance));
        laSeance.dataset.setCompleteCaptorData(laSeance);


        Path jsonDir =  Paths.get(Play.application().path()+"/public/data/");




        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.disable(MapperFeature.USE_ANNOTATIONS);
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            DefaultPrettyPrinter.Indenter indenter = new DefaultIndenter("    ", DefaultIndenter.SYS_LF);
            DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
            printer.indentObjectsWith(indenter);
            printer.indentArraysWith(indenter);
            Json.setObjectMapper(objectMapper);
            Files.createDirectories(jsonDir);
            ArrayList<Path> pathsList = new ArrayList<>();
            for(CaptorData unCaptorData : laSeance.dataset.captor_data){
                Path jsonFile = Paths.get(Play.application().path()+"/public/data/patient-"+ patient._id + "--seance-" + laSeance.numeroUniqueDeSeance + "--capteur-"+unCaptorData.captor.captorType+".json");
                if(!Files.exists(jsonFile)){
                    Files.createFile(jsonFile);
                    Files.write(jsonFile, objectMapper.writer(printer).writeValueAsString(unCaptorData).getBytes("UTF-8"));
                }
                pathsList.add(jsonFile);
            }
            laSeance.dataset.captor_data.clear();
            Path jsonSeanceFile = Paths.get(Play.application().path()+"/public/data/patient-"+ patient._id + "--seance-" + laSeance.numeroUniqueDeSeance + ".json");
            if (!Files.exists(jsonSeanceFile)) {
                Files.createFile(jsonSeanceFile);
                Files.write(jsonSeanceFile, objectMapper.writer(printer).writeValueAsString(laSeance).getBytes("UTF-8"));
            }
            pathsList.add(jsonSeanceFile);
            if(!Files.exists(Paths.get(Play.application().path()+"/public/data/"+patient._id+"--seance-"+laSeance.numeroUniqueDeSeance+"--json.zip"))){
                FileOutputStream fout = new FileOutputStream(new File(Play.application().path()+"/public/data/patient-"+patient._id+"--seance-"+laSeance.numeroUniqueDeSeance+"--json.zip"));
                ZipOutputStream zip = new ZipOutputStream(fout);
                for (Path unPath : pathsList){
                    ZipEntry ze = new ZipEntry(unPath.getFileName().toString());
                    zip.putNextEntry(ze);
                    zip.write(Files.readAllBytes(unPath));
                    zip.closeEntry();
                }
                zip.close();
            }

        } catch (JsonProcessingException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ok(new File(Play.application().path()+"/public/data/patient-"+patient._id+"--seance-"+laSeance.seanceNumber+"--json.zip"));
    }

    public Result exportCSV(String patient_id, String numeroUniqueDeSeance) throws IOException{
        Patient patient = new Patient().findOne(patient_id);
        SeanceMongo laSeance = patient.getLesSeances().get(patient.findSeanceIndex(numeroUniqueDeSeance));
        laSeance.dataset.setCompleteCaptorData(laSeance);

        Path csvDir =  Paths.get(Play.application().path()+"/public/data/");
        Path zipPath = Paths.get(Play.application().path()+"/public/data/patient-"+patient._id+"-seance-"+laSeance.numeroUniqueDeSeance+"-csv.zip");
        CsvMapper mapper = new CsvMapper();
        mapper.disable(MapperFeature.USE_ANNOTATIONS);
        mapper.enable(JsonGenerator.Feature.IGNORE_UNKNOWN);

        ArrayList<Path> pathsList = new ArrayList<>();

//        try {
        Files.createDirectories(csvDir);
        CsvSchema schema = mapper.schemaFor(MeasureMongo.class).withHeader();
        for (CaptorData unCaptorData : laSeance.dataset.captor_data) {

            Path measureFilePath = Paths.get(Play.application().path() + "/public/data/patient-" + patient._id + "--seance-" + laSeance.numeroUniqueDeSeance + "--capteur-" + unCaptorData.captor.captorType + "--measures.csv");
            if (!Files.exists(measureFilePath)) {
                Files.createFile(measureFilePath);
                Files.write(measureFilePath, mapper.writer(schema).writeValueAsBytes(unCaptorData.measures));
            }
            pathsList.add(measureFilePath);
            unCaptorData.measures.clear();

        }
//            schema = mapper.schemaFor(CaptorData.class).withHeader();
//            Path capDataPath = Paths.get(Play.application().path() + "/public/data/patient-" + patient._id + "--seance-" + laSeance.seanceNumber + "--captorData.csv");
//            if (!Files.exists(capDataPath)){
//                Files.createFile(capDataPath);
//                Files.write(capDataPath, mapper.writer(schema).writeValueAsBytes(laSeance.dataset.captor_data));
//            }
//            pathsList.add(capDataPath);
        laSeance.dataset.captor_data.clear();
//            laSeance.dataset = null;
//            laSeance.capteurs = null;

        schema = mapper.schemaFor(DailyMeteo.class).withHeader();
        Path dailyMeteoPath =  Paths.get(Play.application().path() + "/public/data/patient-" + patient._id + "--seance-" + laSeance.numeroUniqueDeSeance + "--daily-meteo.csv");
        if (!Files.exists(dailyMeteoPath)){
            Files.createFile(dailyMeteoPath);
            Files.write(dailyMeteoPath, mapper.writer(schema).writeValueAsBytes(laSeance.meteoData.data));
        }
        pathsList.add(dailyMeteoPath);


        laSeance.meteoData.data.clear();
        schema = mapper.schemaFor(MeteoData.class).withHeader();
        Path meteoDataPath = Paths.get(Play.application().path() + "/public/data/patient-" + patient._id + "--seance-" + laSeance.numeroUniqueDeSeance + "--meteo-data.csv");
        if (!Files.exists(meteoDataPath)){
            Files.createFile(meteoDataPath);
            Files.write(meteoDataPath, mapper.writer(schema).writeValueAsBytes(laSeance.meteoData));
        }
        pathsList.add(meteoDataPath);
        laSeance.meteoData = null;
        schema = CsvSchema.builder()
                .addColumn("seanceNumber")
                .addColumn("date_start")
                .addColumn("patientMongoId")
                .addColumn("scenario_id")
                .addColumn("scenarioMongoId")
                .addColumn("time_start")
                .addColumn("status")
                .build()
                .withHeader();
        Path seancePath = Paths.get(Play.application().path() + "/public/data/patient-" + patient._id + "--seance-" + laSeance.numeroUniqueDeSeance + ".csv");
        if (!Files.exists(seancePath)){
            Files.createFile(seancePath);
            Files.write(seancePath, mapper.writer(schema).writeValueAsBytes(laSeance));
        }
        pathsList.add(seancePath);

        if(!Files.exists(zipPath)){
            FileOutputStream fout = new FileOutputStream(new File(zipPath.toString()));
            ZipOutputStream zip = new ZipOutputStream(fout);
            for (Path unPath : pathsList){
                Logger.debug(unPath.toString());
                ZipEntry ze = new ZipEntry(unPath.getFileName().toString());
                zip.putNextEntry(ze);
                zip.write(Files.readAllBytes(unPath));
                zip.closeEntry();
            }
            zip.close();
        }
//        } catch (IOException e) {
//                e.printStackTrace();
//            }
        return ok(new File(zipPath.toString()));
    }


}
