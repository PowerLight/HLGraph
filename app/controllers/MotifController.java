package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import model.MotifIndividuelLearn;
import model.MotifTypeLearn;
import model.Patient;
import model.TypeMotif;
import org.jongo.MongoCursor;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.creationMotif;
import views.html.listeMotifs;
import views.html.modifierMotif;

import java.util.ArrayList;
import java.util.List;

//import com.sun.java.swing.plaf.motif.resources.motif;

/**
 * Created by dmatjaou on 17/11/2016.
 */
public class MotifController extends Controller{

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result listeMotifs(){

        MongoCursor<TypeMotif> lesMotifs= new TypeMotif().find("");
        ArrayList<TypeMotif> list = new ArrayList<TypeMotif>();

        //Logger.debug("Avant boucle = " + list.toString());
        for(TypeMotif motif : lesMotifs){
            list.add(motif);
            //Logger.debug("Dans boucle = " + motif.getMotifTyp());
        }

        //Logger.debug("List motif = " + list);
        return ok(listeMotifs.render(list));
        //return  ok(JsonTools.toJson(motifTypeLearns1));
        //return  ok(toJson(motifTypeLearns1));
        //return  ok("En cours de developpement");
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result modifierMotif(String mtf){
        //Declarations
        Form<TypeMotif> motifForm = Form.form(TypeMotif.class).bindFromRequest();
        TypeMotif leMotif = motifForm.get();

        leMotif = (TypeMotif) leMotif.findOneById(mtf);

        List<MotifTypeLearn> motifTL = MotifTypeLearn.findAll(null);
        ArrayList<TypeMotif> list = new ArrayList<>();

        // Récupére les motifs individuels (sous liste) dans list
        for (MotifTypeLearn  motif : motifTL) {
            for (MotifIndividuelLearn motifI : motif.lesMotifsIndividuels) {
                TypeMotif tmp = new TypeMotif();
                //tmp.fill(motif,motifI);
                list.add(tmp);
                tmp = null;
            }
        }
        //Logger.debug("Modifier motif  = " + mtf);
        return ok(modifierMotif.render(Form.form(TypeMotif.class),leMotif, mtf));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result deleteMotif(String _id){
        //TypeMotif leMotif = (TypeMotif) new TypeMotif().findOneById(_id);
        //leMotif.remove();
        //return redirect("/listeMotifs");

        //Logger.debug("DeleteMotif = " + _id);
        TypeMotif leMotif = new TypeMotif();
        leMotif = (TypeMotif) leMotif.findOneById(_id);

        MongoCursor<TypeMotif> lesMotifs= new TypeMotif().find("");
        Patient p = new Patient();
        List<Patient> lesPatients = Patient.findAll(null);
        Boolean Flag = false;
        for(Patient unPatient : lesPatients){
            Flag  = Flag || unPatient.testMotifNameExist(leMotif.getMotifTyp());
        }

        if(Flag){
            flash("error", "Le motif : " + leMotif.getMotifTyp() + " : existe encore dans la base impossible de supprimer");

            //return ok(listeMotifs.render(Form.form(TypeMotif.class),lesMotifs));

            return redirect(routes.MotifController.listeMotifs());
        }

        leMotif.remove();
        return redirect("/listeMotifs");
    }


    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result saveMotif(String _id){
        Form<TypeMotif> motifForm = Form.form(TypeMotif.class).bindFromRequest();
        TypeMotif leMotif = motifForm.get();
        String tmf2 = null;


        if (leMotif._id != null){

            leMotif = (TypeMotif) leMotif.findOneById(motifForm.data().get("_id"));
            String tmf = leMotif.getMotifTyp();
            leMotif = motifForm.get();
            leMotif._id = _id;
            tmf2 = tmf;
        }

       // Logger.debug("Method saveMotif = " + leMotif.getMotifTyp() + "  " + tmf2);

        MongoCursor<TypeMotif> lesMotifs= new TypeMotif().find("");
        Patient p = new Patient();
        List<Patient> lesPatients = Patient.findAll(null);
        Boolean Flag = false;
        for(Patient unPatient : lesPatients){
            Flag  = Flag || unPatient.testMotifNameExist(tmf2);
        }

        if(Flag){
            flash("error", "Le motif : " + tmf2 + " : existe encore dans la base impossible de le modifier");

            //return ok(listeMotifs.render(Form.form(TypeMotif.class),lesMotifs));

            //return redirect(routes.MotifController.listeMotifs());
            return ok(modifierMotif.render(Form.form(TypeMotif.class),leMotif,_id));
        }
        leMotif.save();
        return redirect("listeMotifs");
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result creationMotif() {
        return ok(creationMotif.render(Form.form(TypeMotif.class)));
    }

    public Result addMotif(){
        Form<TypeMotif> motifForm = Form.form(TypeMotif.class).bindFromRequest();
        TypeMotif unMotif = motifForm.get();
        String motiFsansEspace = unMotif.getMotifTyp().trim();
        Boolean flag = false;

        MongoCursor<TypeMotif> lesMotifs= new TypeMotif().find("");
        ArrayList<TypeMotif> list = new ArrayList<TypeMotif>();
        //Logger.debug("Method addMotif = " + unMotif.getMotifTyp());
        for (TypeMotif motif : lesMotifs){
            if((motif.getMotifTyp().toUpperCase()).equals(motiFsansEspace.toUpperCase())){
                flag = true;
                break;
            }
        }
        if(flag){
            flash("error", unMotif.getMotifTyp() + " : existe déja veuillez saisir un autre motif");

            //Logger.debug("Method addMotif = " + unMotif.toString().toUpperCase());
            return ok(creationMotif.render(Form.form(TypeMotif.class)));
            //return redirect(routes.MotifController.listeMotifs());
        }

        //Logger.debug("Flag = " + flag  + " == " + unMotif.getMotifTyp());
        //Logger.debug("Compteur = " + lesMotifs.count());
        unMotif.save();
        return redirect("/listeMotifs");
    }


}
