package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import model.*;
import org.jongo.MongoCursor;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;

/**
 * Created by bderouin on 30/05/2016.
 */
public class WsApp extends Controller {

    public Result hello(String name) {
        JsonNode response = Json.toJson("Hello " + name);
        return ok(response);

    }

    public Result getAllPatients() {
        ArrayList<Patient> lesPatients = new ArrayList<>();
        MongoCursor<Patient> cursor = new Patient().find("");
        for (Patient p : cursor) {
            lesPatients.add(p);
        }
        JsonNode result = Json.toJson(lesPatients);
        return ok(result);
    }

    public Result getStimulis() {
        ArrayList<Stimuli> stimulisList = new ArrayList<>();
        MongoCursor<Stimuli> cursor = new Stimuli().find("");
        for (Stimuli s : cursor) {
            stimulisList.add(s);
        }
        JsonNode result = Json.toJson(stimulisList);
        return ok(result);
    }

    public Result getSequences() {
        ArrayList<Sequence> sequenceList = new ArrayList<>();
        MongoCursor<Sequence> cursor = new Sequence().find("");
        for (Sequence sq : cursor) {
            sequenceList.add(sq);
        }
        JsonNode result = Json.toJson(sequenceList);
        return ok(result);
    }

    public Result getScenarios() {
        ArrayList<Scenario> scenarioList = new ArrayList<>();
        MongoCursor<Scenario> cursor = new Scenario().find("");
        for (Scenario sc : cursor) {
            scenarioList.add(sc);
        }
        JsonNode result = Json.toJson(scenarioList);
        return ok(result);
    }

    public Result getSeances() {
        ArrayList<Seance> seanceList = new ArrayList<>();
        MongoCursor<Seance> cursor = new Seance().find("");
        for (Seance sn : cursor) {
            seanceList.add(sn);
        }
        JsonNode result = Json.toJson(seanceList);
        return ok(result);
    }

    public Result getCaptors() {
        ArrayList<Captor> captorsList = new ArrayList<>();
        MongoCursor<Captor> cursor = new Captor().find("");
        for (Captor s : cursor) {
            captorsList.add(s);
        }
        JsonNode result = Json.toJson(captorsList);
        return ok(result);
    }

    public Result postCaptors() {
        String req = "";
        req = request().getHeader("synchroCaptors");
        Logger.debug("req - " + req);

        JsonNode capSqlite = Json.parse(req);
        ArrayList<Captor> captorsList = new ArrayList<>();
        MongoCursor<Captor> cursor = new Captor().find("");
        for (Captor s : cursor) {
            captorsList.add(s);
        }
        JsonNode capMongo = Json.toJson(captorsList);

        for (JsonNode unCapSqlite : capSqlite) {
            boolean update = true;
            Captor neuCaptor;
            for (JsonNode unCapMongo : capMongo) {
                if (unCapMongo.get("captorType").asText().equals(unCapSqlite.get("captorType").asText())) {
                    update = false;
                }
            }
            if (update) {
                neuCaptor = Json.fromJson(unCapSqlite, Captor.class);
                Logger.debug(unCapSqlite.toString());
                neuCaptor.save();
            }
        }
        ArrayList<Captor> listCapt = new ArrayList<>();
        for (JsonNode unCapMongo : capMongo) {
            boolean update = true;
            Captor neuCaptor;
            for (JsonNode unCapSqlite : capSqlite) {
                if (unCapMongo.get("captorType").asText() == unCapSqlite.get("captorType").asText()) {
                    update = false;
                }
            }
            if (update) {
                listCapt.add(Json.fromJson(unCapMongo, Captor.class));

            }
        }
        return ok(Json.toJson(listCapt.toString()));
    }

    public Result postStimulis() {

        String req = new String();
        req = request().getHeader("synchroStimulis");

        //SQlite stimulis list
        JsonNode stimuliSqlite = Json.parse(req);

        //Mongodb stimulis list
        ArrayList<Stimuli> stimuliList = new ArrayList<Stimuli>();
        MongoCursor<Stimuli> cursor = new Stimuli().find("");
        for (Stimuli s : cursor) {
            stimuliList.add(s);
        }
        JsonNode stimuliMongo = Json.toJson(stimuliList);
        Logger.debug("req - " + stimuliList.toString());
        for (JsonNode unStimuliSqlite : stimuliSqlite) {
            boolean update = true;

            for (JsonNode unStimuliMongo : stimuliMongo) {
                //Logger.debug("Tests : " + unStimuliMongo.get("ledColor").asText());
                if (unStimuliMongo.get("ledColor").asText().equals(unStimuliSqlite.get("ledColor").asText())
                        && unStimuliMongo.get("frequency").asInt() == unStimuliSqlite.get("frequency").asInt()
                        && unStimuliMongo.get("intensity").asInt() == unStimuliSqlite.get("intensity").asInt()) {
                    update = false;
                }
            }
            if (update) {
                Stimuli nouveauStimuli;
                nouveauStimuli = Json.fromJson(unStimuliSqlite, Stimuli.class);
                Logger.debug(unStimuliSqlite.toString());
                nouveauStimuli.save();
            }
        }

        ArrayList<Stimuli> listStimulis = new ArrayList<>();

        for (JsonNode unStimuliMongo : stimuliMongo) {
            boolean update = true;
            Stimuli neuStimuli;
            for (JsonNode unStimuliSqlite : stimuliSqlite) {
                if (unStimuliMongo.get("ledColor").asText() == unStimuliSqlite.get("ledColor").asText()) {
                    update = false;
                }
            }
            if (update) {
                listStimulis.add(Json.fromJson(unStimuliMongo, Stimuli.class));
            }
        }
        return ok(Json.toJson(listStimulis));
    }

    public Result postScenarios() {
        //Logger.debug("Scenario DEBUT........","Début .......");
        String req = "";
        String sqDesc = null, sqTitle;
        int sqltID, mongoID;

        //SQlite : liste des scenarios à récupérer dans la requête Http
        // et transformer en format json
        req = request().getHeader("synchroScenarios");
        JsonNode scenarioSqlite = Json.parse(req);

        //Mongodb : liste des scénarios
        ArrayList<Scenario> scenariosList = new ArrayList<>();
        MongoCursor<Scenario> cursor = new Scenario().find("");
        for (Scenario s : cursor) {
            scenariosList.add(s);
        }

        JsonNode scenarioMongo = Json.toJson(scenariosList);

        //1) Prendre un senaio Sqlite
        for (JsonNode unScenarioSqlite : scenarioSqlite) {
            boolean update = true; //On suppose que ce scenario n'existe pas sur mongo
                                   //si supposition confirmée: on l'ajoute à Mongo
            sqDesc = unScenarioSqlite.get("description").asText();
            sqTitle = unScenarioSqlite.get("title").asText();
            sqltID = unScenarioSqlite.get("scenarioId").asInt();
            Scenario majScenario,nouvScenario, mngScnCourant = null;
            int scenarioId = 0;
            SqltSenario sqltScn;
            String _id = null;

            //2) Le comparer avec chaque scénario Mongo
            for (JsonNode unScenarioMongo : scenarioMongo) {
                String mgDesc = unScenarioMongo.get("description").asText();
                String mgTypSc = unScenarioMongo.get("typeScenario").asText();
                String mgTitle = unScenarioMongo.get("title").asText();
                mongoID = unScenarioMongo.get("scenarioId").asInt();

                //Comparaison des : ID, decriptions et Title
                if ( mongoID == sqltID && mgDesc.equals(sqDesc) /*&& mgTitle.equals(sqTitle)*/){
                    //Si scenario Sqlite a un equivalent sur Mongo, pas de sauvegarde : update = false
                    update = false;
                    scenarioId = unScenarioMongo.get("scenarioId").asInt();
                    _id = unScenarioMongo.get("_id").asText();
                    mngScnCourant = Json.fromJson(unScenarioMongo, Scenario.class);
                    break;
                }
            }

            if (update) {
                //Si scénario SQLite n'existe pas dans Mongo alors : le creer copie exacte sur Mongo
                sqltScn = Json.fromJson(unScenarioSqlite, SqltSenario.class);

                nouvScenario = new Scenario(sqltScn.scenarioId, sqltScn.description, sqltScn.typeScenario,
                                            sqltScn.title, sqltScn.lastUpdate,
                                            unScenarioSqlite.get("switch_caption_eeg").asBoolean(),
                                            unScenarioSqlite.get("switch_caption_eyelid").asBoolean(),
                                            unScenarioSqlite.get("switch_caption_head_movement").asBoolean(),
                                            unScenarioSqlite.get("switch_caption_pulse").asBoolean(),
                                            unScenarioSqlite.get("switch_caption_skin_conductance").asBoolean());
                nouvScenario.save();
            }
            else {
                    String mongoUpDat, sqltUpDat;
                    mongoUpDat = mngScnCourant.lastUpdate;
                    sqltUpDat = unScenarioSqlite.get("lastUpdate").asText();
//                    if (Long.valueOf(mngScnCourant.lastUpdate) < unScenarioSqlite.get("lastUpdate").asLong()) {
                    if (Long.valueOf(mongoUpDat) < Long.valueOf(sqltUpDat)) {
                        sqltScn = Json.fromJson(unScenarioSqlite, SqltSenario.class);
                        majScenario = Json.fromJson(unScenarioSqlite, Scenario.class);

                        majScenario = new Scenario(sqltScn.scenarioId, sqltScn.description, sqltScn.typeScenario,
                                sqltScn.title, sqltScn.lastUpdate,
                                unScenarioSqlite.get("switch_caption_eeg").asBoolean(),
                                unScenarioSqlite.get("switch_caption_eyelid").asBoolean(),
                                unScenarioSqlite.get("switch_caption_head_movement").asBoolean(),
                                unScenarioSqlite.get("switch_caption_pulse").asBoolean(),
                                unScenarioSqlite.get("switch_caption_skin_conductance").asBoolean());

                       majScenario._id = _id;
                       majScenario.save();
                    }
            }

            }
        //Mongodb : liste finale des scénarios après les mises à jour
        ArrayList<Scenario> scenariosListFinal = new ArrayList<>();
        MongoCursor<Scenario> cursor2 = new Scenario().find("");
        for (Scenario s : cursor2) {
            scenariosListFinal.add(s);
        }
//        Logger.debug("Scenario FIN .......","FIN .......");
        return ok(Json.toJson(scenariosListFinal));
    }

    public Result postSeances(){
        String req = request().getHeader("synchroSeances");
        Seance[] lesSeancesSqlite = Json.fromJson(Json.parse(req),Seance[].class);
        MongoCursor<Patient> cursor = new Patient().find("");
            for (Seance uneSeance: lesSeancesSqlite){
                boolean create = true;
                while (cursor.hasNext()){
                    Patient unPatient = cursor.next();
                    boolean update = false;
                    SeanceMongo laNouvelleSeanceMongo = new SeanceMongo();
                    for (SeanceMongo uneSeanceMongo : unPatient.lesSeances){
                        if (uneSeanceMongo.numeroUniqueDeSeance.equals(uneSeance.numeroUniqueDeSeance)
                                && unPatient.lastUpdate < uneSeance.lastUpdate){
                            laNouvelleSeanceMongo = uneSeanceMongo;
                            update = true;
                        }
                        if (uneSeanceMongo.numeroUniqueDeSeance.equals(uneSeance.numeroUniqueDeSeance)){
                            create = false;
                        }
                        if(update){
                            uneSeanceMongo.scenario_id = uneSeance.scenario_id;
                            uneSeanceMongo.time_start = uneSeance.time_start;
                            uneSeanceMongo.date_start = uneSeance.date_start;
                            uneSeanceMongo.seanceId = uneSeance.seanceId;
                            uneSeanceMongo.lastUpdate = uneSeance.lastUpdate;
                            uneSeanceMongo.status = uneSeance.status;
                            uneSeanceMongo.patientMongoId = unPatient._id;
                        }
                        //A spprimer apres utilisation
                        if(uneSeance.seanceId == uneSeanceMongo.seanceId && uneSeanceMongo.numeroUniqueDeSeance == null){
                            uneSeanceMongo.numeroUniqueDeSeance = uneSeance.numeroUniqueDeSeance;
                        }
                    unPatient.save();
                    }
                    unPatient.update("lesSeances", unPatient.lesSeances);
                }
                if (create){
                    Logger.debug("{patientId:"+uneSeance.patient_id+"}");
                    Patient lePatient = (Patient) new Patient().find("{patientId:"+uneSeance.patient_id+"}").next();
                    SeanceMongo nouvelleSeanceMongo = new SeanceMongo();
                    nouvelleSeanceMongo.scenario_id = uneSeance.scenario_id;
                    nouvelleSeanceMongo.time_start = uneSeance.time_start;
                    nouvelleSeanceMongo.date_start = uneSeance.date_start;
                    nouvelleSeanceMongo.seanceId = uneSeance.seanceId;
                    nouvelleSeanceMongo.lastUpdate = uneSeance.lastUpdate;
                    nouvelleSeanceMongo.status = uneSeance.status;
                    nouvelleSeanceMongo.patientMongoId = lePatient._id;
                    ScenarioMongo leScenario = (ScenarioMongo) new ScenarioMongo().findorCreate(uneSeance.scenario_id);
                    nouvelleSeanceMongo.scenarioMongoId = leScenario._id;
                    nouvelleSeanceMongo.scenario = (ScenarioMongo) new ScenarioMongo().findorCreate(uneSeance.scenario_id);
                    lePatient.lesSeances.add(nouvelleSeanceMongo);
                    lePatient.lastUpdate = uneSeance.lastUpdate;
                    lePatient.synchroSave();
                }
        }
        return ok(Json.toJson(lesSeancesSqlite.toString()));
    }

    public Result postPatients() {
        Logger.debug("Patient DEBUT..............");
        Patient patMongo = null;

        JsonNode patientsSqlite = request().body().asJson();
        ArrayList<Patient> patientList = new ArrayList<>();
        MongoCursor<Patient> cursor = new Patient().find("");
        for (Patient s : cursor) {
            patientList.add(s);
        }
        JsonNode patientMongo = Json.toJson(patientList);
        /*************************************************
         SYNCHRONISATION  SENS : SQLITE --> MONGO
         **************************************************/

        for (JsonNode unpatientSqlite : patientsSqlite) {
            boolean update = true;
            for (JsonNode unpatientMongo : patientMongo) {
                if (unpatientSqlite.get("firstname").asText().equals(unpatientMongo.get("firstname").asText())
                        && unpatientSqlite.get("lastname").asText().equals(unpatientMongo.get("lastname").asText())
                        && unpatientSqlite.get("birthday").asText().equals(unpatientMongo.get("birthday").asText())) {

                    update = false;
                    patMongo = Json.fromJson(unpatientMongo, Patient.class);
                    unpatientMongo.get("lastUpdate").asLong();
                }
            }
            if (update) {
                Patient p = new Patient();

                Patient pSqlite = Json.fromJson(unpatientSqlite, Patient.class);
                p.firstname = pSqlite.firstname;
                p.lastname = pSqlite.lastname;
                p.birthday = pSqlite.birthday;
                p.permanentId = pSqlite.patientId;   //xxx
                p.lastUpdate = pSqlite.lastUpdate;
                p.patientId = pSqlite.patientId;

                p.save();
            }
            else {
                if (patMongo.lastUpdate < unpatientSqlite.get("lastUpdate").asLong()) {

                    Patient p = new Patient();
                    Logger.debug("test - " + unpatientSqlite.toString());
                    //                MongoCursor cur = p.find("{'lastname' : '" + unpatientSqlite.get("lastname").asText()
                    //                        + "'," + " 'firstname' : '" + unpatientSqlite.get("firstname").asText() +
                    //                        "' , 'birthday' : '" + unpatientSqlite.get("birthday").asText() + "'}");
                    //                if (cur.count() == 1) {
                    //                    p = (Patient) cur.next();
                    //                } else if (cur.count() > 1) {
                    //                    return null;
                    //                }
                    Patient pSqlite = Json.fromJson(unpatientSqlite, Patient.class);
                p.patientId = pSqlite.patientId;
                    p.firstname = pSqlite.firstname;
                    p.lastname = pSqlite.lastname;
                    p.birthday = pSqlite.birthday;
                    p.permanentId = pSqlite.permanentId;
                    p.lastUpdate = pSqlite.lastUpdate;
                    p.patientId = pSqlite.patientId;
                    p._id = patMongo._id;

                    p.save();
                    //                ArrayList<SeanceMongo> sessionAttended = new ArrayList<>();
                    //                if (unpatientSqlite.get("sessionsAttended") != null) {
                    //                    sessionAttended = Json.fromJson(unpatientSqlite.get("sessionsAttended"), ArrayList.class);
                    //                }
                    //                Logger.debug("Seances - " + sessionAttended.toString());
                    //p.save();
                }
            }
        }
//        //Mongodb : liste finale des patients après les mises à jour
//        ArrayList<Patient> pListFinal = new ArrayList<>();
//        MongoCursor<Patient> cursor2 = new Patient().find("");
//        for (Patient p : cursor2) {
//            pListFinal.add(p);
//        }
//        Logger.debug("Patient FIN .......","FIN .......");
//        return ok(Json.toJson(pListFinal));

        /*************************************************
          SYNCHRONISATION  SENS : MONGO --> SQLITE
        **************************************************/
        cursor = new Patient().find("");
        ArrayList<Patient> listePatients = new ArrayList<>();
        while (cursor.hasNext()){
            Patient pMongo = cursor.next();
            boolean update = true;
            Patient pSqlit = new Patient();
            for (JsonNode unpatientSqlite : patientsSqlite) {
                if (unpatientSqlite.get("firstname").asText().equals(pMongo.firstname)
                        && unpatientSqlite.get("lastname").asText().equals(pMongo.lastname)
                        && unpatientSqlite.get("birthday").asText().equals(pMongo.birthday)) {

                    update = false;
                    pSqlit = Json.fromJson(unpatientSqlite, Patient.class);
                }
            }
            if(update) {
                pSqlit.permanentId = pMongo.permanentId;
                pSqlit.firstname = pMongo.firstname;
                pSqlit.lastname = pMongo.lastname;
                pSqlit.birthday = pMongo.birthday;
                pSqlit.lastUpdate = pMongo.lastUpdate;
                pSqlit.patientId = pMongo.patientId;
                listePatients.add(pSqlit);

            }else {
                if (pMongo.lastUpdate > pSqlit.lastUpdate) {
                    pSqlit.permanentId = pMongo.permanentId;
                    pSqlit.firstname = pMongo.firstname;
                    pSqlit.lastname = pMongo.lastname;
                    pSqlit.birthday = pMongo.birthday;
                    pSqlit.lastUpdate = pMongo.lastUpdate;
                    pSqlit.patientId = pMongo.patientId;
                    listePatients.add(pSqlit);
//                ArrayList<SeanceMongo> sessions = new ArrayList<>();
//                if(unpatientMongo.get("sessionsAttended") != null){
//                    sessions = Json.fromJson(unpatientMongo.get("sessionsAttended"), ArrayList.class);
//                }
//                Logger.debug("Seances - " + sessions.toString());
                }
            }
        }
        Logger.debug("Patient FIN .......","FIN .......");
        return ok(Json.toJson(listePatients));
    }
}


