package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.*;
import model.new_database.*;
import model.SequenceMongo;
import model.new_database.Scenario;
import org.jongo.MongoCursor;
import play.Logger;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.*;

import java.util.ArrayList;
import java.util.Date;


public class ScenarioController extends Controller {

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result scenario()                {return ok(scenario.render("scenario"));}
    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result scenarioCapteur()         {return ok(scenarioCapteur.render("stimuli"));}
    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result scenarioSequence()        {return ok(scenarioSequence.render("stimuli"));}

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result listeScenarios() {
        MongoCursor<Scenario> scenarios = new Scenario().find("");
        ArrayList<Scenario> list = new ArrayList<Scenario>();

        MongoCursor<Patient> patients = new Patient().find("");
        ArrayList verif = new ArrayList();

        for (Scenario scenario : scenarios) {
            list.add(scenario);
            //Logger.debug("scenario._id = " + scenario._id + " /// titre = " + scenario.title);
            //controle si le scenario existe chez un patient
            int test = 0;
            for(Patient patient : patients) {
                for(int j=0; j< patient.getLesSeances().size(); j++) {
                    if(scenario._id.equals(patient.getLesSeances().get(j).scenario_id)) {
                        test = 1;
                    }
                    //Logger.debug("id = " + patient.lesSeances.get(j).ScenarioId + " ===== test = " + test);
                }
            }
            verif.add(test);
        }
        return ok(listeScenarios.render(list, verif));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result creationScenario() {
        MongoCursor<Stimulus> cursor = new Stimulus().find("");
        ArrayList<Stimulus> list = new ArrayList<>();
        for (Stimulus unStimulus : cursor){
            list.add(unStimulus);
        }
        MongoCursor<Capteur> cursor2 = new Capteur().find("");
        ArrayList<Capteur> list2 = new ArrayList<>();
        for (Capteur unCapteur : cursor2){
            list2.add(unCapteur);
        }
        return ok(creationScenario.render(list,list2));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result addScenario(){
        Form<Scenario> ScenarioForm = Form.form(Scenario.class).bindFromRequest();
        Scenario leScenario = ScenarioForm.get();


        JsonNode nodes = Json.parse(ScenarioForm.data().get("jsonListOfSequences"));
        for (JsonNode unNode : nodes){
            ScenarioSequence laSequence = new ScenarioSequence();
            if((unNode.get("stimuliType").asText().equals("LIGHT")) && (unNode.get("stimuli").asText()=="")) {
                Stimulus leStimulus = (Stimulus) new Stimulus().findOneById(unNode.get("stimuli").get("_id").asText());
                laSequence.setStimulus(leStimulus);
            }

            else if((unNode.get("stimuliType").asText().equals("LIGHT")) && (unNode.get("stimuli").asText()!="null")) {
                Stimulus leStimulus = (Stimulus) new Stimulus().findOneById(unNode.get("stimuli").asText());
                laSequence.setStimulus(leStimulus);
            }
            else {
                laSequence.setStimulus(null);
            }
            laSequence.setDescription(unNode.get("description").asText());
            laSequence.setDuration(unNode.get("duration").asInt());
            laSequence.setOrderIndex(unNode.get("orderIndex").asInt());
            leScenario.getSequences().add(laSequence);
        }
        for(int i=0;i<request().body().asFormUrlEncoded().get("capteur").length;i++) {
            System.out.println(request().body().asFormUrlEncoded().get("capteur")[i]);
            Capteur leCapteur = (Capteur) new Capteur().findOneById(request().body().asFormUrlEncoded().get("capteur")[i]);
            leScenario.getCaptors().add(leCapteur);
        }
        Logger.debug("le scenario = " + leScenario);
        //leScenario.setType(request().body().asFormUrlEncoded(""));
        leScenario.setLastUpdate(new Date().getTime());
        leScenario.save();
        return redirect(controllers.routes.ScenarioController.listeScenarios());
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result modifierScenario(String _id) throws JsonProcessingException {
        MongoCursor<Stimulus> cursor = new Stimulus().find("");
        ArrayList<Stimulus> stimulis = new ArrayList<>();
        for (Stimulus unStimulus : cursor){
            stimulis.add(unStimulus);
        }

        MongoCursor<Capteur> cursor2 = new Capteur().find("");
        ArrayList<Capteur> capteurs = new ArrayList<>();
        for (Capteur unCapteur : cursor2){
            capteurs.add(unCapteur);
        }

        Scenario leScenario = new Scenario();
        leScenario = (Scenario)leScenario.findOneById(_id);

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonSequences = objectMapper.writer().writeValueAsString(leScenario.getSequences());
        String jsonCapteurs = objectMapper.writer().writeValueAsString(leScenario.getCaptors());
        /*
        JsonNode jsonSequences = new JsonNode() {
        };
        for(SequenceMongo sequence : leScenario.sequences) {
            String description = sequence.description;
            int duration = sequence.duration;
            String stimuli = sequence.stimuli._id;
            String stimuliType = sequence.stimuliType;
            int orderIndex = sequence.orderIndex;
            String uneSequence = "{'description:'"+description+"',duration:'" + duration + "}";

            jsonSequences.add(uneSequence);
        }*/

        //Logger.debug("sequences = " + jsonSequences);

        return ok(modifierScenario.render(Form.form(Scenario.class), leScenario, stimulis, capteurs, jsonSequences, jsonCapteurs));
        // TODO: 07/03/2017 modifier scénario à faire

    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result dupliquerScenario(String _id) throws JsonProcessingException{
        MongoCursor<Stimulus> cursor = new Stimulus().find("");
        ArrayList<Stimulus> stimulis = new ArrayList<>();
        for (Stimulus unStimulus : cursor){
            stimulis.add(unStimulus);
        }

        Scenario leScenario = new Scenario();
        leScenario = (Scenario)leScenario.findOneById(_id);

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonSequences = objectMapper.writer().writeValueAsString(leScenario.getSequences());

        //return ok(dupliquerScenario.render(Form.form(Scenario.class), leScenario, stimulis, jsonSequences));
        // TODO: 07/03/2017 dupliquer scénario à faire
        Result temp = new Result() {
            @Override
            public play.api.mvc.Result toScala() {
                return null;
            }
        };
        return temp;
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result saveScenario(){
        Form<Scenario> ScenarioForm = Form.form(Scenario.class).bindFromRequest();
        Scenario leScenario = ScenarioForm.get();

        String id = ScenarioForm.data().get("_id");

        leScenario = (Scenario) leScenario.findOneById(id);
        leScenario = ScenarioForm.get();

        System.out.println(request().body().asFormUrlEncoded());

        //liste des séquences
        //JsonNode nodes = Json.parse(ScenarioForm.data().get("jsonListOfSequences"));

        /*for (JsonNode unNode : nodes){
            SequenceMongo laSequence = new SequenceMongo();

                System.out.println(unNode);

                Stimulus leStimulus = (Stimulus) new Stimulus().findOneById(unNode.get("stimulus").get("_id").asText());
                laSequence.stimuli = leStimulus;
                laSequence.stimuli_id = leStimulus._id;


            laSequence.description = unNode.get("description").asText();
            laSequence.duration = unNode.get("duration").asInt();
            laSequence.orderIndex = unNode.get("orderIndex").asInt();
            // TODO: 03/03/2017 adapter la séquence
        }*/

        leScenario.save();
        return redirect(controllers.routes.ScenarioController.listeScenarios());
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result descriptionScenario(String _id, int test){

        Scenario leScenario = new Scenario();
        leScenario = (Scenario)leScenario.findOneById(_id);
        //Logger.debug("scenario = " + leScenario);
        return ok(descriptionScenario.render(leScenario, test));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result scenarioRemove(String _id) {
        Scenario leScenario = (Scenario) new Scenario().findOneById(_id);
        leScenario.remove();
        return redirect("/listeScenarios");
    }

}
