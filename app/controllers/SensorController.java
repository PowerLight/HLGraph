package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import model.new_database.Capteur;
import org.jongo.MongoCursor;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.createSensor;
import views.html.listSensor;
import views.html.modifySensor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by froncin on 07/02/2017.
 */
public class SensorController extends Controller{

    public Result listSensor(){

        final MongoCursor<Capteur> lesCapteurs = new Capteur().find("");
        List<Capteur> list = new ArrayList<Capteur>();
        for (Capteur capteur : lesCapteurs)
            list.add(capteur);
        return ok(listSensor.render(list));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result createSensor() {
        return ok(createSensor.render());
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result addSensor() {
        Form<Capteur> capteurForm = Form.form(Capteur.class).bindFromRequest();
        Capteur unCapteur = capteurForm.get();
        //TODO same as roles with permissions
        unCapteur.save();
        return redirect(routes.SensorController.listSensor());
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result modifySensor(String _id){
        Capteur leCapteur = new Capteur();
        leCapteur = (Capteur)leCapteur.findOneById(_id);
        return ok(modifySensor.render(Form.form(Capteur.class), leCapteur));
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result saveSensor(){
        //Logger.debug("je suis avant la condition");
        Form<Capteur> capteurForm = Form.form(Capteur.class).bindFromRequest();
        Capteur leCapteur = capteurForm.get();

        if (leCapteur._id != null){
            leCapteur = (Capteur) leCapteur.findOneById(capteurForm.data().get("_id"));
            leCapteur = capteurForm.get();
        }
        leCapteur.save();
        return redirect("/listSensor");
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result removeSensor(String _id) {
        Capteur leCapteur = (Capteur) new Capteur().findOneById(_id);
        leCapteur.remove();
        return redirect("/listSensor");
    }
}
