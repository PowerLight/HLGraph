package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import model.new_database.ScenarioSequence;
import model.new_database.Stimulus;
import org.jongo.MongoCursor;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.creationSequence;
import views.html.listeSequencesMongo;

import java.util.ArrayList;
import java.util.List;

public class SequenceController extends Controller {

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result listeSequences(){
        //List<ScenarioSequence> list = (ArrayList<ScenarioSequence>) sequenceMongo.createAndFind();
        final MongoCursor<ScenarioSequence> lesSequences = new ScenarioSequence().find("");
        List<ScenarioSequence> list = new ArrayList<ScenarioSequence>();
        for (ScenarioSequence scenarioSequence : lesSequences)
            list.add(scenarioSequence);
        return ok(listeSequencesMongo.render(list));

    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result creationSequence() {
        MongoCursor<Stimulus> stimulis = new Stimulus().find("");
        ArrayList<Stimulus> stimuliList = new ArrayList<>();
        for (Stimulus unStimulus: stimulis){
            stimuliList.add(unStimulus);
        }
        return ok(creationSequence.render(stimuliList));
    }


    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result addSequence(){
        final MongoCursor<ScenarioSequence> lesSequences = new ScenarioSequence().find("");
        Form<ScenarioSequence> sequenceForm = Form.form(ScenarioSequence.class).bindFromRequest();
        ScenarioSequence uneScenarioSequence = sequenceForm.get();
        uneScenarioSequence.setOrderIndex(lesSequences.count());
        uneScenarioSequence.setDuration(sequenceForm.get().getDuration());
        //uneScenarioSequence.setStimulus(sequenceForm.get().getStimulus());
        Stimulus unStimulus = new Stimulus();
        unStimulus = (Stimulus)unStimulus.findOneById(request().body().asFormUrlEncoded().get("stimuliId")[0]);
        uneScenarioSequence.setStimulus(unStimulus);

        //controle si la séquence existe déjà
        /*final MongoCursor<ScenarioSequence> lesSequences = new ScenarioSequence().find("");
        for (ScenarioSequence sequence : lesSequences) {
            if(sequenceForm.get().stimuliId == sequence.stimuliId && sequenceForm.get().duration == sequence.duration
                && sequenceForm.get().stimuliType == sequence.stimuliType) {
                flash("Cette séquence exite déjà !!!");
                return forbidden(creationSequence.render(Form.form(ScenarioSequence.class)));
            }
        }*/

        //TODO same as roles with permissions
        uneScenarioSequence.save();
        return redirect(routes.SequenceController.listeSequences());
    }

    @Security.Authenticated(Secured.class)
    @Restrict(@Group("user"))
    public Result sequenceMongoRemove(String _id) {
        ScenarioSequence laScenarioSequence = (ScenarioSequence) new ScenarioSequence().findOneById(_id);
        laScenarioSequence.remove();
        return redirect("/listeSequencesMongo");
    }

}
