package model;

import be.objectify.deadbolt.core.models.Permission;
import be.objectify.deadbolt.core.models.Role;
import be.objectify.deadbolt.core.models.Subject;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import play.Logger;
import play.data.validation.Constraints;

import java.util.ArrayList;
import java.util.List;



public class User extends ModeleMongo implements Subject {
    @Constraints.Required

    public String login;
    public String userName;
    public String userFirstName;
    public String userPassword;

    public List<SecurityRole> roles = new ArrayList<SecurityRole>();;
    public List<UserPermission> permissions= new ArrayList<UserPermission>();
    @JsonIgnore
    public List<String> rolesList= new ArrayList<String>();

    @JsonIgnore
    public boolean sessionActive;

    @Override
    public List<? extends Permission> getPermissions()
    {
        return permissions;
    }

    @Override
    public List<? extends Role> getRoles()
    {
        return roles;
    }


    @Override
    public String getIdentifier()
    {
        return userName;
    }


    @JsonCreator

    public User(@JsonProperty("login") String login,@JsonProperty("userName") String userName, @JsonProperty("userFirstName") String userFirstName,@JsonProperty("userPassword") String userPassword) {

        super("users");

        this.login = login;
        this.userName = userName;
        this.userFirstName = userFirstName;
        this.userPassword = userPassword;

    }


    public User() {
        super("users");
    }



    public  User findByLogin(String login) {
        return db().findOne("{login: #}", login).as(User.class);

    }


    public  boolean authenticate(String login, String passwd){
        User leUser = findByLogin(login);
        if(leUser==null){
            return false;
        }
        HashUtils myHashUtils = new HashUtils();
        Logger.debug(login);
        return myHashUtils.verifyPassword(passwd, leUser.userPassword);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", userName='" + userName + '\'' +
                ", userFirstName='" + userFirstName + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", roles=" + roles +
                ", permissions=" + permissions +
                '}';
    }

    public String getLogin(){
        return this.login;
    }

    public void setLogin(String login){
        this.login = login;
    }
}
