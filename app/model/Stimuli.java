package model;

/**
 * Created by tnguyend on 22/01/2016.
 */
public class Stimuli extends ModeleMongo {
    public int stimuliId;
    public String ledColor;
    public int intensity;
    public int frequency;

    public Stimuli(){
        super("lightStimulation");
    }

    @Override
    public String toString() {
        return "Stimuli{" +
                "stimuliId=" + stimuliId +
                ", ledColor='" + ledColor + '\'' +
                ", intensity=" + intensity +
                ", frequency=" + frequency +
                '}';
    }
}
