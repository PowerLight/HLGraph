
package model;


/**
 * Created by rbougrin on 05/11/2015.
 */

import be.objectify.deadbolt.core.models.Role;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;
import play.data.validation.Constraints;
import uk.co.panaxiom.playjongo.PlayJongo;


/**
 * @author Steve Chaloner (steve@objectify.be)

 */


public class SecurityRole extends ModeleMongo implements Role
{
    @Constraints.Required
    public String name;

    public String getName()
    {
        return name;
    }

    public SecurityRole() {
        super("securityRoles");
    }

    @JsonCreator
    public SecurityRole(@JsonProperty("name") String name) {
        super("securityRoles");
        this.name = name;
    }

    public SecurityRole findByName(String roleName) {
        return db().findOne("{name: #}", roleName).as(SecurityRole.class);
    }

    @Override
    public String toString(){
        return this.getName();
    }
}

