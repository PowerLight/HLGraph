
package model;


/**
 * Created by rbougrin on 05/11/2015.
 *
 */
import be.objectify.deadbolt.core.models.Permission;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;
import play.data.validation.Constraints;
import uk.co.panaxiom.playjongo.PlayJongo;



/**
 * @author Steve Chaloner (steve@objectify.be)

 */

public class UserPermission extends ModeleMongo implements Permission
{
    @MongoObjectId
    public String _id;

    //@Column(name = "permission_value")
    @Constraints.Required
    public String value;

    public String getValue()
    {
        return value;
    }

    @JsonCreator
    public UserPermission(@JsonProperty("_id") String _id, @JsonProperty("value") String value) {
        super("userPermissions");
        this._id = _id;
        this.value = value;
    }

    public UserPermission() {
        super("userPermissions");
    }

    public static MongoCollection userPermissions() {
        return PlayJongo.getCollection("userPermissions");

    }



    public  UserPermission findByPermissionValue(String value) {
        return db().findOne("{value: #}", value).as(UserPermission.class);
    }
}

