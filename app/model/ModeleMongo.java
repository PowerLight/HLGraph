package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;
import play.data.validation.Constraints;
import uk.co.panaxiom.playjongo.PlayJongo;
import play.Logger;

import java.util.HashMap;


/**
 * Classe métier ModeleMongo
 *
 * Jongo est un driver permettant d'interagir avec une bdd MongoDB en utilisant la même syntaxe que la console shell de Mongo.
 *
 * Références :
 * Query and Projection Operators : https://docs.mongodb.org/manual/reference/operator/query/
 *
 * @author Created by Gérard Sauer on 12/11/2015.
 *
 */

public class ModeleMongo {
    @MongoObjectId
    public String _id;
    @JsonIgnore
    @Constraints.Required
    protected String collectionName;

    public void set_id(String _id) {
        this._id = _id;
    }

    /**
     *
     * @param collectionName
     */
    public ModeleMongo(String collectionName){
        this.collectionName = collectionName;
    }

    /**
     *
     * @return
     */
    public MongoCollection db(){
        return PlayJongo.getCollection(this.collectionName);
    }

    /**
     *
     * @return
     */
    public String save(){
        db().save(this);
        return this._id;
    }

    /**
     *
     */
    public void remove() {
        db().remove("{ _id: # }", new ObjectId(this._id));
    }

    /**
     *
     * @param conditions
     * @return
     */
    public MongoCursor find(String conditions) {
        return db().find(conditions).as(this.getClass());
    }

    public MongoCursor findWithLimit(String conditions, int limit) {
        return db().find(conditions).limit(limit).as(this.getClass());
    }



    public ModeleMongo findOne(String conditions) {
        return  db().findOne(conditions).as(this.getClass());
    }

    public ModeleMongo findOneById(String _id) {
        ObjectId mongoId = new ObjectId(_id);
        return  db().findOne("{_id : #}", mongoId).as(this.getClass());
    }

    public void update(String field, Object value){
        db().update("{_id : #}",new ObjectId(this._id)).with("{$set : {"+field+" : #}}", value);
    }


    /**
     *
     * @return
     */
    public  long count(String query){
        return db().count(query);
    }


    @Override
    public String toString() {
        return "ModeleMongo{" +
                "id='" + _id + '\'' +
                ", collectionName='" + collectionName + '\'' +
                '}';
    }



}
