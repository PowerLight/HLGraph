package model;



import java.util.ArrayList;

/**
 * Created by jblairon on 15/03/2016.
 */
public class MeteoData {
    public float longitude;
    public float latitude;
    public String timezone;
    public ArrayList<DailyMeteo> data;

    public MeteoData() {
        data = new ArrayList<DailyMeteo>();
    }

    @Override
    public String toString() {
        return "MeteoData{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                ", timezone='" + timezone + '\'' +
                ", data=" + data +
                '}';
    }
}
