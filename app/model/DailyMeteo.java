package model;

/**
 * Created by jblairon on 15/03/2016.
 */
public class DailyMeteo {
    public long time;
    //type de temps
    public String summary;
    //précipitations
    public float precipIntensity;
    public float precipProbability;

    public float temperature;
    public float apparentTemperature;
    public float dewPoint;
    public float humidity;
    public float windSpeed;
    public float windBearing;
    public float visibility;

    //eccombrement du ciel
    public float cloudCover;
    //pression athmosphérique
    public float pressure;

    public float ozone;

    @Override
    public String toString() {
        return "DailyMeteo{" +
                "time=" + time +
                ", summary='" + summary + '\'' +
                ", precipIntensity=" + precipIntensity +
                ", precipProbability=" + precipProbability +
                ", temperature=" + temperature +
                ", apparentTemperature=" + apparentTemperature +
                ", dewPoint=" + dewPoint +
                ", humidity=" + humidity +
                ", windSpeed=" + windSpeed +
                ", windBearing=" + windBearing +
                ", visibility=" + visibility +
                ", cloudCover=" + cloudCover +
                ", pressure=" + pressure +
                ", ozone=" + ozone +
                '}';
    }
}
