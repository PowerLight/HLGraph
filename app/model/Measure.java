package model;

import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.jongo.marshall.jackson.oid.MongoObjectId;
import uk.co.panaxiom.playjongo.PlayJongo;

import java.util.ArrayList;

/**
 * Created by bderouin on 05/11/2015.
 */
public class Measure extends ModeleMongo implements java.lang.Comparable{


    public int measureId;
    public int captor_id;
    public int seance_id;
    public float dataReceived;
    public long measureTimestamp;

    public Measure() {
        super("measures");

    }
    public Measure(int captor_id, int seance_id) {
        super("measures");
        this.captor_id = captor_id;
        this.seance_id = seance_id;
    }

    public Measure(String _id,  int measureId, int captor_id, int seance_id, float dataReceived, long measureTimestamp) {
        super("measures");
        this._id = _id;
        this.measureId = measureId;
        this.captor_id = captor_id;
        this.seance_id = seance_id;
        this.dataReceived = dataReceived;
        this.measureTimestamp = measureTimestamp;
    }

    @Override
    public String save(){
        return "Illegal operation";
    }

/*    public ArrayList<Measure> findAllBySeanceId(int seance_id){
        ArrayList<Measure> retour = new ArrayList<Measure>();
        MongoCursor<Measure> cursor = find("{seance_id : "+seance_id+"}");
        for( Measure uneMeasure: cursor){
            retour.add(uneMeasure);
        }
        return retour;
    }

    public ArrayList<Measure> findByCaptorAndSeanceIds(){
        ArrayList<Measure> retour = new ArrayList<Measure>();
        MongoCursor<Measure> cursor = find("{seance_id : "+this.seance_id+", captor_id : "+this.captor_id+"}");
        for( Measure uneMeasure: cursor){
            retour.add(uneMeasure);
        }
        return retour;
    }

    public static ArrayList<Measure> find300BeetweenTimestamp(long start, long stop, int seance_id, int captor_id){
        ArrayList<Measure> resultats = new ArrayList<Measure>();
       MongoCursor<Measure> total = findMeasuresBetweenTimestamps(start, stop, seance_id, captor_id);

        long count = total.count();
        if (count > 300){
            long offset = count % 300;
            for (long cpt = 0 ; cpt <= count ; cpt += offset){
                for(long i = cpt ; i <= cpt + offset +1  ; i ++){
                    if(total.hasNext()){
                        total.next();
                    }

                }
                if (total.hasNext()) {
                    resultats.add(total.next());
                }
            }
        }else{
            for(Measure uneMeasure : total){
                resultats.add(uneMeasure);
            }
        }


        return resultats;
    }

    public static Measure findFirstAfterTimestamp(long timestamp){
        Measure maMeasure = measures().find("{measureTimestamp: {$gte :"+timestamp+"}}").sort("{measureTimestamp: 1}").limit(1).as(Measure.class).next();



        return maMeasure;
    }*/

    @Override
    public int compareTo(Object o) {
        long nombre1 = ((Measure) o).measureTimestamp;
        long nombre2 = this.measureTimestamp;
        if (nombre1 > nombre2)  return -1;
        else if(nombre1 == nombre2) return 0;
        else return 1;
    }

}
