package model;

import be.objectify.deadbolt.core.models.Permission;
import be.objectify.deadbolt.core.models.Role;
import be.objectify.deadbolt.core.models.Subject;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import play.Logger;
import play.data.validation.Constraints;


import javax.validation.Constraint;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by rleguill on 24/10/2016.
 * Contenu de la sous classe "patients/lesSeance/"
 */
public class MotifMongo implements Comparable {//il n'est aps nécessaire d'hériter de ModeleMongo
    //@Constraints.Required
    public String xDebut;//ms
    public String xFin;//ms
    public String motifName;
    public String captorId;//id_choix_signaux;
    public String userLogin;//login de l'utilisateur entrant la sélection
    public long lastUpdate;
    public int motifNumber;

    public String numeroUniqueMotif;

    @Override
    public int compareTo(Object o) {
        MotifMongo quote=(MotifMongo)o;
        int result = this.motifName.compareTo(quote.motifName);//tri par type de motif
        if (result == 0) {
            result = this.captorId.compareTo(quote.captorId);//tri par capteur
            if (result == 0) {
                result = Integer.parseInt(this.xDebut)-Integer.parseInt(quote.xDebut);//tri au fil du temps
                if (result == 0) {
                    result = Integer.parseInt(this.xFin)-Integer.parseInt(quote.xFin);
                }
            }
        }
        return result;
    }


    public void fillByMotifPattern (MotifPattern motif){
        xDebut=motif.x_debut;
        xFin=motif.x_fin;
        motifName=motif.id_choix_motif;
        captorId=motif.id_choix_signaux;
        userLogin=motif.user_login;
    }

    @JsonCreator
    public MotifMongo(@JsonProperty("xDebut") String xDebut,@JsonProperty("xFin") String xFin,
                        @JsonProperty("motifName") String motifName,@JsonProperty("captorId") String captorId,
                        @JsonProperty("userLogin") String userLogin) {
        this.xDebut = xDebut;
        this.xFin = xFin;
        this.motifName = motifName;
        this.captorId = captorId;
        this.userLogin = userLogin;
    }

    private String sCV(String clef,String val){
        return ", " + clef + "='" + val + '\'' ;
    }
    @Override
    public String toString() {
        return "MotifMongo{" + "xDebut='" + xDebut+  '\'' +
                sCV("xFin",xFin) + sCV("motifName",motifName) +
                sCV("captorId",captorId) + sCV("userLogin",userLogin) +
                sCV("numeroUniqueMotif",numeroUniqueMotif) + '}';
    }

    public MotifMongo() { }

}
