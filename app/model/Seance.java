package model;


import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import play.Logger;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.ArrayList;

/**
 * Created by bderouin on 13/11/2015.
 */
public class Seance extends ModeleMongo {

    public int seanceId;
    public String date_start;
    public int patient_id;
    public int scenario_id;
    private HashMap capteurs = new HashMap();
    public String numeroUniqueDeSeance;
    public long lastUpdate;
    public String time_start;
    public String status;


    public Seance() {
        super("seances");
    }
    public Seance findOneBySeanceId(int seanceId){
        return db().findOne("{seanceId : "+seanceId+"}").as(Seance.class);
    }


    public HashMap getCapteurs(){
        return capteurs;
    }


}
