package model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;

/**
 * Created by bderouin on 23/11/2015.
 */
public class MeasureMongo extends ModeleMongo implements java.lang.Comparable {
    public long measureTimestamp;
    public float dataReceived;
    public int captor_id;
/*
    @JsonIgnore
    public int seanceNumber;
    @JsonIgnore
    public String numeroUniqueDeSeance;
    @JsonIgnore
    public String patient_id;
    @JsonIgnore
    public int measureId;
*/
    public MeasureMongo() {
        super("measureMongo");
    }

    public MeasureMongo( int captor_id, int seanceNumber, String patient_id) {
        super("measureMongo");
        this.captor_id = captor_id;
        //this.seanceNumber = seanceNumber;
        //this.patient_id = patient_id;
    }



    public MeasureMongo( int measureId, int captor_id, int seanceNumber, String numUniqueSeance, float dataReceived, long measureTimestamp) {
        super("measureMongo");
        //this.measureId = measureId;
        //this.numeroUniqueDeSeance = numUniqueSeance;
        this.captor_id = captor_id;
        //this.seanceNumber = seanceNumber;
        this.dataReceived = dataReceived;
        this.measureTimestamp = measureTimestamp;
    }

    public HashMap toMap(){
        HashMap monMap = new HashMap();
        //monMap.put("measureId", this.measureId);
        //monMap.put("numeroUniqueDeSeance", this.numeroUniqueDeSeance);
        //monMap.put("patient_id", this.patient_id);
        monMap.put("captor_id", this.captor_id);
        //monMap.put("seanceNumber", this.seanceNumber);
        monMap.put("dataReceived", this.dataReceived);
        monMap.put("measureTimestamp", this.measureTimestamp);

        return monMap;
    }

    @Override
    public int compareTo(Object o) {
        long nombre1 = ((MeasureMongo) o).measureTimestamp;
        long nombre2 = this.measureTimestamp;
        if (nombre1 > nombre2)  return -1;
        else if(nombre1 == nombre2) return 0;
        else return 1;
    }

    @Override
    public String toString() {
        return "MeasureMongo{" +
                "captor_id=" + captor_id +/*
                ", measureId=" + measureId +
                ", seanceNumber=" + seanceNumber +*/
                ", dataReceived=" + dataReceived +
                ", measureTimestamp=" + measureTimestamp +
                '}';
    }
}
