package model;

import org.jongo.MongoCursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbougrin on 18/11/2015.
 */
public class Sequence extends ModeleMongo {
    public int sequenceId;
    public String description;
    public int duration;
    public String stimuliType;
    public int stimuliId;

    public int orderIndex;

    public Sequence() {
        super("sequences");

    }

    public static List<Sequence> findAll(String term) {
        Sequence s = new Sequence();
        final MongoCursor<Sequence> sequences = s.find(term);
        List<Sequence> list = new ArrayList<Sequence>();
        for (Sequence sequence : sequences) {

            list.add(sequence);
        }

        return list;
    }

    @Override
    public String save(){
        return null;
    }

    @Override
    public String toString() {
        return "ScenarioSequence{" +
                "sequenceId=" + sequenceId +
                ", description='" + description + '\'' +
                ", duration=" + duration +
                ", stimuliType='" + stimuliType + '\'' +
                ", stimuliId=" + stimuliId +
                ", orderIndex=" + orderIndex +
                '}';
    }
}
