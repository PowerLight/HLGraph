package model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import play.data.validation.Constraints;

public class MotifDelete {

    @Constraints.Required

    public String sceanceId;
    public String patientId;
    public String motifId;
    public String user_login;
    public long lastUpdate;


    @JsonCreator
    public MotifDelete( @JsonProperty("patientId") String patientId,@JsonProperty("sceanceId") String sceanceId,
                        @JsonProperty("motifId") String motifId) {
        this.sceanceId = sceanceId;
        this.patientId = patientId;
        this.motifId = motifId;
        this.lastUpdate = System.currentTimeMillis();
    }


    public MotifDelete() { }

    public String getSceanceId() {
        return sceanceId;
    }

    public void setSceanceId(String sceanceId) {
        this.sceanceId = sceanceId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getMotifId() {
        return motifId;
    }

    public void setMotifId(String motifId) {
        this.motifId = motifId;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
