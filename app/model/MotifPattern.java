package model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import play.data.validation.Constraints;

public class MotifPattern {

    @Constraints.Required

    public String x_debut;
    public String x_fin;
    public String id_choix_motif;
    public String id_choix_signaux;
    public String sceanceId;
    public String patientId;
    public String motifId;
    public long lastUpdate;

    public String user_login;


    @JsonCreator
    public MotifPattern(
                        @JsonProperty("sceanceId") String sceanceId,@JsonProperty("patientId") String patientId,
                        @JsonProperty("x_debut") String x_debut,@JsonProperty("x_fin") String x_fin,
                        @JsonProperty("id_choix_motif") String id_choix_motif,
                        @JsonProperty("id_choix_signaux") String id_choix_signaux) {
        this.sceanceId = sceanceId;
        this.patientId = patientId;
        this.x_debut = x_debut;
        this.x_fin = x_fin;
        this.id_choix_motif = id_choix_motif;
        this.id_choix_signaux = id_choix_signaux;
        this.motifId = null;
        this.lastUpdate = System.currentTimeMillis();
    }

    @JsonCreator
    public MotifPattern(
            @JsonProperty("sceanceId") String sceanceId,@JsonProperty("patientId") String patientId,
            @JsonProperty("x_debut") String x_debut,@JsonProperty("x_fin") String x_fin,
            @JsonProperty("id_choix_motif") String id_choix_motif,
            @JsonProperty("id_choix_signaux") String id_choix_signaux,
            @JsonProperty("id_choix_signaux") String motifId) {
        this.sceanceId = sceanceId;
        this.patientId = patientId;
        this.x_debut = x_debut;
        this.x_fin = x_fin;
        this.id_choix_motif = id_choix_motif;
        this.id_choix_signaux = id_choix_signaux;
        this.motifId = motifId;
        this.lastUpdate = System.currentTimeMillis();
    }

    public MotifPattern() { }

    public String getX_debut() {
        return x_debut;
    }

    public void setX_debut(String x_debut) {
        this.x_debut = x_debut;
    }

    public String getX_fin() {
        return x_fin;
    }

    public void setX_fin(String x_fin) {
        this.x_fin = x_fin;
    }

    public String getId_choix_motif() {
        return id_choix_motif;
    }

    public void setId_choix_motif(String id_choix_motif) {
        this.id_choix_motif = id_choix_motif;
    }

    public String getId_choix_signaux() {
        return id_choix_signaux;
    }

    public void setId_choix_signaux(String id_choix_signaux) {
        this.id_choix_signaux = id_choix_signaux;
    }

    public String getSceanceId() {
        return sceanceId;
    }

    public void setSceanceId(String sceanceId) {
        this.sceanceId = sceanceId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public String getMotifId() {
        return motifId;
    }

    public void setMotifId(String motifId) {
        this.motifId = motifId;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
