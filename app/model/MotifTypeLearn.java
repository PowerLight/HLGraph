package model;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.bson.types.ObjectId;
import org.jongo.MongoCursor;
import play.data.validation.Constraints;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import model.new_database.Patient;

/**
 * Created by rleguill on 04/11/2016.
 */
public class MotifTypeLearn extends ModeleMongo {
    @Constraints.Required
    public String _id;//"Spindle","ComplexeK",...
    @Constraints.Required
    public String motifTypeId;
    public long lastUpdate;//util pour obtimiser la mise à jour
    public ArrayList<MotifIndividuelLearn> lesMotifsIndividuels;



    public MotifTypeLearn() {
        super("motifTypeLearn");
        this.lesMotifsIndividuels = new ArrayList<MotifIndividuelLearn>();
    }

    public MotifTypeLearn(MotifMongo motif) {
        super("motifTypeLearn");
        this.motifTypeId=motif.motifName;
        this._id= new ObjectIdGenerators.UUIDGenerator().generateId(this).toString();
        this.lesMotifsIndividuels = new ArrayList<MotifIndividuelLearn>();
    }

    public void supprimerMotif(){
        while(this.lesMotifsIndividuels.size()>0)this.lesMotifsIndividuels.remove(0);
    }
    public void fill(Patient patient,SeanceMongo seance, MotifMongo motif){
        MotifIndividuelLearn motifIndiv= new MotifIndividuelLearn();
        motifIndiv.patientId            = patient._id;
        motifIndiv.numeroUniqueDeSeance = seance.numeroUniqueDeSeance;
        motifIndiv.numeroUniqueDeMotif  = motif.numeroUniqueMotif;
        motifIndiv.captorId             = Integer.parseInt(motif.captorId);
        motifIndiv.debutMotif           = Integer.parseInt(motif.xDebut);
        motifIndiv.finMotif             = Integer.parseInt(motif.xFin);

        Captor captor= seance.dataset.findCaptor(motifIndiv.captorId);

        motifIndiv.captorName   = captor.description;
        motifIndiv.captorType   = captor.captorType;
        motifIndiv.unite        = captor.unite;
        motifIndiv.fillMeasuresBetweenTimestamps(seance,Long.parseLong(motif.xDebut)-1000,Long.parseLong(motif.xFin)+1000);
        //System.out.println(captor);
        this.lesMotifsIndividuels.add(motifIndiv);
    }

    @Override
    public String save(){
        //this.createUUIDs();
        this.lastUpdate = new Date().getTime();
        db().save(this);
        return this._id;

    }public String synchroSave(){
        //this.createUUIDs();
        db().save(this);
        return this._id;
    }

    @Override
    public void update(String field, Object value){

        db().update("{_id : #}",new ObjectId(this._id)).with("{$set : {"+field+" : #, lastUpdate : #}}", value, new Date().getTime());

    }

    public static List<MotifTypeLearn> findAll(String term) {
        MotifTypeLearn p = new MotifTypeLearn();
        final MongoCursor<MotifTypeLearn> motifs = p.find(term);
        List<MotifTypeLearn> list = new ArrayList<MotifTypeLearn>();
        for (MotifTypeLearn motifTypeLearn : motifs) {
            list.add(motifTypeLearn);
        }
        return list;
    }


    @Override
    public MotifTypeLearn findOne(String _id) {
        MotifTypeLearn motifTypeLearn = (MotifTypeLearn) new MotifTypeLearn().findOneById(_id);
        return motifTypeLearn;
    }
    public MotifTypeLearn find1Type(String type) {
        List<MotifTypeLearn> mesTypes= this.findAll(null);
        Iterator<MotifTypeLearn> mTypeIter=mesTypes.iterator();
        MotifTypeLearn tmp;
        if(mTypeIter.hasNext()){
            while(mTypeIter.hasNext()){
                tmp = mTypeIter.next();
                if(type.compareTo(tmp.motifTypeId)==0) {
                    return tmp;
                }
            }
        }
        return null;
    }

    public List<MotifIndividuelLearn> createUUIDs( ){
        for (MotifIndividuelLearn unMotif : this.lesMotifsIndividuels){
            if (unMotif.nbUniqueMotif == null){
                unMotif.nbUniqueMotif = new ObjectIdGenerators.UUIDGenerator().generateId(unMotif).toString();
            }
        }
        return this.lesMotifsIndividuels;
    }


    public int findMotifIndex(String numeroUniqueDeMotif){
        int i = 0;
        int monIndex = -1;
        for(MotifIndividuelLearn unMotif : this.lesMotifsIndividuels){
            if(unMotif.numeroUniqueDeMotif.equals(numeroUniqueDeMotif)){
                monIndex = i;
            }
            i++;
        }
        return monIndex;
    }

   public MotifIndividuelLearn findSeance(String numeroUniqueDeMotif){
       for(MotifIndividuelLearn unMotif : this.lesMotifsIndividuels){
           if(unMotif.numeroUniqueDeMotif.equals(numeroUniqueDeMotif)){
               return unMotif;
           }
       }
       return null;
   }

    @Override
    public String toString() {
        return "MotifTypeLearn{" +
                "_id='" + _id + '\'' +
                "motifTypeId='" + motifTypeId + '\'' +
                "lastUpdate='" + lastUpdate + '\'' +
                ", lesMotifsIndividuels=" + lesMotifsIndividuels +
                '}';
    }

}
