package model;

import com.google.gson.Gson;

/**
 * Created by bderouin on 06/11/2015.
 */
public class JsonTools {

    public static String toJson(Object obj) {

        Gson gson = new Gson();
        return gson.toJson(obj);
    }
}
