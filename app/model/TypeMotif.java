package model;

/* Created by dmatjaou on 18/11/2016. */
public class TypeMotif extends ModeleMongo{
    //public  String _id;
    private String motifTyp; // Spindel ou Complex K

    public TypeMotif() {
        super("typeMotif");
    }

    public void setMotifTyp(String mtp) {
        this.motifTyp = mtp;
    }

    public String getMotifTyp() {
        return motifTyp;
    }

    @Override
    public String toString() {
        return "TypeMotif{" +
                "motif=" + motifTyp +
                '}';
    }
}
