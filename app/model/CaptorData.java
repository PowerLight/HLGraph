package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.DBObject;
import org.jongo.MongoCursor;
import play.Logger;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by bderouin on 06/11/2015.
 */
public class CaptorData{
    public int captor_id;
    public Captor captor;
    @JsonIgnore
    public ArrayList<MeasureMongo> measures = new ArrayList<MeasureMongo>();

    public CaptorData() {
    }

    public CaptorData(int captor_id) {
        this.captor_id = captor_id;
    }


    public void generateMeasuresMongo(SeanceMongo seance){
        Logger.debug("Generating MeasuresMongo");
        this.findMeasuresBySeanceId(seance);
        BulkWriteOperation bulk = new MeasureMongo().db().getDBCollection().initializeUnorderedBulkOperation();
        int spliter = 1;
        for(MeasureMongo uneMM : this.measures){

            //uneMM.patient_id = seance.patientMongoId;
            DBObject unDBO = new BasicDBObject();
            unDBO.putAll(uneMM.toMap());
            bulk.insert(unDBO);
            if (spliter == 100000){
                bulk.execute();
                bulk = new MeasureMongo().db().getDBCollection().initializeUnorderedBulkOperation();
                spliter =0;
            }
            spliter ++;
        }
        if (spliter != 1){
            bulk.execute();
        }

    }



    public void findMeasuresBySeanceId(SeanceMongo seance){
        Logger.debug("get measures {captor_id: "+this.captor_id+", seance_id : "+seance.seanceId+"}");
        MongoCursor<Measure> cursor = new Measure(this.captor_id,seance.seanceId).find("{captor_id: "+this.captor_id+", seance_id : "+seance.seanceId+"}");
        for (Measure uneMeasure : cursor){
            MeasureMongo uneMM = new MeasureMongo(uneMeasure.measureId, this.captor_id,seance.seanceNumber, seance.numeroUniqueDeSeance, uneMeasure.dataReceived, uneMeasure.measureTimestamp);
            this.measures.add(uneMM);
        }
    }

    public void findMeasuresBetweenTimestamps(SeanceMongo seanceMongo, long start, long stop){
        // Tri des données lors du requêtage
        //MongoCursor<MeasureMongo> cursor = new MeasureMongo().db().find("{ captorData_id : '"+_id+"', measureTimestamp : {$gte : "+start+", $lte : "+stop+"}}").sort("{measureTimestamp: 1}").as(MeasureMongo.class);
        MongoCursor<MeasureMongo> cursor = new MeasureMongo().find("{captor_id : "+this.captor_id+", numeroUniqueDeSeance : '"+seanceMongo.numeroUniqueDeSeance+"', measureTimestamp : {$gte : "+start+", $lte : "+stop+"}}");
        for (MeasureMongo uneMeasure : cursor){
            this.measures.add(uneMeasure);
        }
        // Les measures sont dorénavant triées au requêtage dans MongoDB
        //Collections.sort(this.measures);
}


    public void filter(int nb_signal){
        int numberOfPoints= 500;
        if(nb_signal==1)numberOfPoints= 5120;
        int count = this.measures.size();
        Logger.info("count "+count+"| point "+numberOfPoints);

        ArrayList<MeasureMongo> les300 = new ArrayList<MeasureMongo>();
        if (count > numberOfPoints){
            float offset = count / ((float) numberOfPoints);

            for (float cpt = 0 ; cpt < count ; cpt += offset){
                les300.add(this.measures.get((int) cpt));
            }

            this.measures.clear();
            this.measures = les300;
            Collections.sort(this.measures);
        }
    }



    public void setCaptor(){
        this.captor = (Captor) new Captor().findOne("{captor_id: "+captor_id+"}");
    }

    @Override
    public String toString() {
        return "CaptorData{" +
                "captor_id=" + captor_id +
                ", captor=" + this.captor +
                ", measures=" + this.measures.toString() +
                '}';
    }
}
