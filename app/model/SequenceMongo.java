package model;

import org.jongo.MongoCursor;
import model.new_database.Stimulus;

import java.util.ArrayList;

/**
 * Created by rbougrin on 18/11/2015.
 */
public class SequenceMongo extends ModeleMongo {
    public int sequenceId;
    public String description;
    public int duration;
    public String stimuliType;
    public Stimulus stimuli;
    public int stimuliId;
    public String stimuli_id;
    public int orderIndex;

    public SequenceMongo() {
        super("sequencesMongo");
    }


    public ArrayList<SequenceMongo> createAndFind(){
        ArrayList<SequenceMongo> list = new ArrayList<>();
        this.createAll();
        MongoCursor<SequenceMongo> leCursor= this.find("");
        for (SequenceMongo laSequenceMongo: leCursor){
            list.add(laSequenceMongo);
        }

        return list;

    }

    public void createAll(){
        MongoCursor<Sequence> lesSequences = new Sequence().find("");
        MongoCursor<SequenceMongo> lesSequencesMongo = this.find("");
        for (Sequence uneSequence: lesSequences) {
        boolean create = true;
            for (SequenceMongo uneSequenceMongo: lesSequencesMongo) {
                if(uneSequence.sequenceId == uneSequenceMongo.sequenceId){
                    create = false;
                }
            }

            if(create){
                SequenceMongo nouvelleSequenceMongo = new SequenceMongo();
                nouvelleSequenceMongo.description = uneSequence.description;
                nouvelleSequenceMongo.duration = uneSequence.duration;
                nouvelleSequenceMongo.stimuliType = uneSequence.stimuliType;
                nouvelleSequenceMongo.stimuliId = uneSequence.stimuliId;
                nouvelleSequenceMongo.stimuli = (Stimulus) new Stimulus().findOne("{stimuliId:"+uneSequence.stimuliId+"}");
                nouvelleSequenceMongo.stimuli_id = new Stimulus().findOne("{stimuliId:"+uneSequence.stimuliId+"}")._id;
                nouvelleSequenceMongo.orderIndex = uneSequence.orderIndex;
                nouvelleSequenceMongo.save();
            }

        }
    }
    public void setStimulus() {
        this.stimuli = (Stimulus) new Stimulus().findOne("{stimuliId:" + this.stimuliId + "}");
        this.stimuli_id = new Stimuli().findOne("{stimuliId:"+this.stimuliId+"}")._id;
    }




    @Override
    public String toString() {
        return "SequenceMongo{" +
                "sequenceId=" + sequenceId +
                ", description='" + description + '\'' +
                ", duration=" + duration +
                ", stimuliType='" + stimuliType + '\'' +
                ", stimuli=" + stimuli +
                ", stimuliId=" + stimuliId +
                ", orderIndex=" + orderIndex +
                '}';
    }
}
