package model;

import org.jongo.MongoCursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbougrin on 25/01/2016.
 */
public class SeanceExportCSV extends ModeleMongo{
    public String seanceMongoId;
    public Patient patient;
    public SeanceMongo seance;
    public ScenarioMongo scenario;
    public DataSet dataSet;
    public ArrayList<MeasureMongo> measures = new ArrayList<MeasureMongo>();

    public SeanceExportCSV() {
        super("seanceExportCSV");;

    }
    public SeanceExportCSV(String seanceId){
        super("seanceExportCSV");
//        seance = (SeanceMongo)new SeanceMongo().findOneById(seanceId);
//        seanceMongoId=seance._id;
//        patient = (Patient)new Patient().findOneById(seance.patientMongoId);
//        scenario = (ScenarioMongo)new ScenarioMongo().findorCreate(seance.scenario_id);
//        dataSet = (DataSet)new DataSet().findOne("{seanceMongoId: '"+seance._id+"'}");
//        dataSet.setCaptorData();
//        for(CaptorData leCaptorData: dataSet.captor_data){
//            MongoCursor<MeasureMongo> cursor= new MeasureMongo().find("{captorData_id: '"+leCaptorData._id+"'}");
//            for(MeasureMongo measure:cursor){
//                measures.add(measure);
//            }
//        }
//        this.db().remove();
//        this.save();
//        //traitement batch csv
//
//        //this.db().remove();
    }

    @Override
    public String toString() {
        return "SeanceExportCSV{" +
                "seanceMongoId='" + seanceMongoId + '\'' +
                ", patient=" + patient +
                ", seance=" + seance +
                ", scenario=" + scenario +
                //", dataSet=" + dataSet +
                '}';
    }
}
