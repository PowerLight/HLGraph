package model;


import com.fasterxml.jackson.databind.deser.std.StringArrayDeserializer;
import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;
import play.Logger;
import uk.co.panaxiom.playjongo.PlayJongo;



import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.apache.commons.io.IOUtils;

import play.Logger;
import play.libs.Json;


import java.util.*;

/**
 * Created by rleguill on 07/11/2016.
 */
public class MotifIndividuelLearn {
    public String nbUniqueMotif;//
    public String numeroUniqueDeMotif;//valeur stockée pour HL Graph
    public String numeroUniqueDeSeance;
    public String patientId;
    public boolean reconstituee;
    public int captorId;
    public String captorName;
    public String captorType;
    public String unite;
    public int debutMotif;//timeStamp
    public int finMotif;//timeStamp

    public ArrayList<DataLearn> lesData;
    //public String eventLearn_id;//dans le cas de la gestion d'évenement


    public MotifIndividuelLearn() {
        lesData =new ArrayList<DataLearn>();
        reconstituee = true;
    }


    public void fillMeasuresBetweenTimestamps(SeanceMongo seanceMongo, long start, long stop){
        DataLearn uneData;
        MeasuresReconstitueeMongo tmpMes;
        MongoCursor<MeasureMongo> cursor = new MeasureMongo().find("{captor_id : "+this.captorId+
                ", numeroUniqueDeSeance : '"+seanceMongo.numeroUniqueDeSeance+"', measureTimestamp : {$gte : "+start+
                ", $lte : "+stop+"}}");
        MongoCursor<MeasuresReconstitueeMongo> cursorR = new MeasuresReconstitueeMongo().find("{captor_id : "+this.captorId+
                ", numeroUniqueDeSeance : '"+seanceMongo.numeroUniqueDeSeance+"', measureTimestamp : {$gte : "+start+
                ", $lte : "+stop+"}}");
        for (MeasureMongo uneMeasure : cursor){
            uneData= new DataLearn();
            uneData.captorId= this.captorId;
            uneData.mesure=uneMeasure.dataReceived;
            uneData.timeStamp=uneMeasure.measureTimestamp;

            if(cursorR.hasNext()&&reconstituee){
                tmpMes=cursorR.next();
                if(tmpMes.measureTimestamp==uneData.timeStamp){
                    uneData.mesureReconstituee=tmpMes.dataReceived;
                }
                else{
                    Logger.debug("Données reconstituées asynchrones! "+cursorR.toString()+"\n"+uneData.toString());
                }
            }
            else {
                reconstituee = false;
            }
            this.lesData.add(uneData);
        }
        if(!reconstituee)
            Logger.debug("Pas de donnees reconstituees ! Seance : "+ seanceMongo.numeroUniqueDeSeance+", capteur :"+captorId);
        this.lesData = (ArrayList) this.createUUIDs();
    }

    public List<DataLearn> createUUIDs( ){
        for (DataLearn uneData : this.lesData){
            if (uneData.dataLearnId == null){
                uneData.dataLearnId = new ObjectIdGenerators.UUIDGenerator().generateId(uneData).toString();
            }
        }
        return this.lesData;
    }

    private String sCV(String clef,String val){//écriture plus compacte de toString
        return ", " + clef + "='" + val + '\'' ;
    }
    @Override
    public String toString() {
        return "MotifIndividuelLearn{" + "nbUniqueMotif='" + nbUniqueMotif+  '\'' +
                sCV("numeroUniqueDeMotif",numeroUniqueDeMotif) + sCV("captorId",captorId+"\0") +
                sCV("numeroUniqueDeSeance",numeroUniqueDeSeance) + sCV("captorName",captorName) +
                sCV("captorType",captorType) + sCV("unite",unite) + sCV("patientId",patientId) +
                ", debutMotif=" + debutMotif +
                ", finMotif=" + finMotif +
                ", lesData=" + lesData+ '}';
    }
}
