package model;

import org.jongo.MongoCursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbougrin on 18/11/2015.
 */
public class ScenarioMongo extends ModeleMongo {
    public int scenarioId;
    public String description;
    public String typeScenario;
    public int eeg;
    public int eyelid;
    public int headMovement;
    public int pulse;
    public int skinConductance;
    public String title;
    public List<SequenceMongo> sequences = new ArrayList<SequenceMongo>();

    public ScenarioMongo() {
        super("scenariosMongo");
    }

    public ScenarioMongo findorCreate(int scenario_id){

        MongoCursor cursor = this.find("{scenarioId : "+scenario_id+"}");
        if(cursor.count() == 0){
            return createScenarioMongo(scenario_id);
        }else {
            return (ScenarioMongo) cursor.next();
        }
    }

    public ScenarioMongo createScenarioMongo(int scenario_id) {
        ScenarioSequences s = new ScenarioSequences();
        MongoCursor<ScenarioSequences> cursor = s.find("{scenario_id : "+scenario_id+"}");
        Scenario sc = (Scenario)new Scenario().findOne("{scenarioId : "+scenario_id+"}");
        this.scenarioId = sc.scenarioId;
        this.description = sc.description;
        this.typeScenario = sc.typeScenario;
        this.eeg = sc.eeg;
        this.eyelid = sc.eyelid;
        this.headMovement = sc.headMovement;
        this.pulse = sc.pulse;
        this.skinConductance = sc.skinConductance;
        this.title = sc.title;
        play.Logger.debug("createScenarioMongo");
        for(ScenarioSequences scenarioSequences : cursor){
            SequenceMongo uneSequenceMongo = new  SequenceMongo();
            Sequence laSequence = (Sequence)new Sequence().findOne("{sequenceId : "+scenarioSequences.sequence_id+"}");
            uneSequenceMongo.sequenceId= laSequence.sequenceId;
            uneSequenceMongo.description=laSequence.description;
            uneSequenceMongo.duration=laSequence.duration;
            uneSequenceMongo.stimuliType=laSequence.stimuliType;
            uneSequenceMongo.stimuliId=laSequence.stimuliId;
            uneSequenceMongo.setStimulus();
            uneSequenceMongo.orderIndex=laSequence.orderIndex;
            this.sequences.add(uneSequenceMongo);
        }
        this._id = this.save();
        return this;
    }

    @Override
    public String toString() {
        return "ScenarioMongo{" +
                "scenarioId=" + scenarioId +
                ", description='" + description + '\'' +
                ", typeScenario='" + typeScenario + '\'' +
                ", eeg=" + eeg +
                ", eyelid=" + eyelid +
                ", headMovement=" + headMovement +
                ", pulse=" + pulse +
                ", skinConductance=" + skinConductance +
                ", title='" + title + '\'' +
                ", sequences=" + sequences +
                '}';
    }
}
