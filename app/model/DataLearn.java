package model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.apache.commons.io.IOUtils;

import play.Logger;
import play.libs.Json;


import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import java.sql.Timestamp;

import java.util.*;

/**
 * Created by rleguill on 04/11/2016.
 */
public class DataLearn {
    public String dataLearnId;
    public long timeStamp;
    public int captorId;
    //public String numeroUniqueDeSeance;
    public float mesure;
    public float mesureReconstituee;



    private String sCV(String clef,String val){//écriture plus compacte de toString
        return ", " + clef + "='" + val + '\'' ;
    }
    @Override
    public String toString() {
        return "DataLearn{" + "dataLearnId='" + dataLearnId+  '\'' +
                ", timeStamp=" + timeStamp +", captorId=" + captorId + ", mesure=" + mesure +
                ", mesureReconstituee=" + mesureReconstituee + '}';
    }

    public DataLearn(){
    }
}
