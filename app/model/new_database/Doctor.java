package model.new_database;

import model.ModeleMongo;

/**
 * Created by vpozdnia on 23/02/2017.
 */
public class Doctor extends ModeleMongo {

    private static final String COLLECTION_NAME = "doctor";

    private long rpps;
    private String siret;
    private String lastname;
    private String firstname;
    private String speciality;
    private String telNumber;
    private String email;

    public long getRpps() {
        return rpps;
    }

    public void setRpps(long rpps) {
        this.rpps = rpps;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Doctor(){
        super(COLLECTION_NAME);
    }

    public Doctor(long rpps, String siret, String lastname, String firstname, String speciality, String telNumber, String email){
        super(COLLECTION_NAME);
        this.rpps = rpps;
        this.siret = siret;
        this.lastname = lastname;
        this.firstname = firstname;
        this.speciality = speciality;
        this.telNumber = telNumber;
        this.email = email;
    }

    public String toString() {
        return "Doctor{" +
                "id='" + _id + '\'' +
                "rpps='" + rpps + '\'' +
                "siret='" + siret + '\'' +
                "lastname='" + lastname + '\'' +
                "firstname='" + firstname + '\'' +
                "speciality='" + speciality + '\'' +
                "telNumber='" + telNumber + '\'' +
                "email='" + email + '\'' +
                '}';
    }
}
