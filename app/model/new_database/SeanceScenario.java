package model.new_database;

import org.bson.types.ObjectId;

import java.util.ArrayList;

/**
 * Created by rcollas on 22/12/2016.
 */
public class SeanceScenario{

    private String title;
    private String description;
    private String type;
    private ArrayList<ScenarioSequence> sequences;
    private ArrayList<Capteur> captors;

    public SeanceScenario(){}

    public SeanceScenario(Scenario s){
        this.title = s.getTitle();
        this.description = s.getDescription();
        this.type = s.getScenarioType();
        this.sequences = s.getSequences();
        this.captors = s.getCaptors();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String typeScenario) {
        this.type = typeScenario;
    }

    public ArrayList<ScenarioSequence> getSequences() {
        return sequences;
    }

    public void setSequences(ArrayList<ScenarioSequence> sequences) {
        this.sequences = sequences;
    }

    public ArrayList<Capteur> getCaptors() {
        return captors;
    }

    public void setCaptors(ArrayList<Capteur> captors) {
        this.captors = captors;
    }

    @Override
    public String toString() {
        return "{" +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", sequences='" + sequences + '\'' +
                ", captors='" + captors + '\'' +
                '}';
    }
}
