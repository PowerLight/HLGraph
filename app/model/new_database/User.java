package model.new_database;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import model.HashUtils;
import model.ModeleMongo;
import model.SecurityRole;
import model.UserPermission;
import play.Logger;
import play.data.validation.Constraints;

import java.util.ArrayList;

/**
 * Created by vpozdnia on 27/02/2017.
 */
public class User extends ModeleMongo {

    private static final String COLLECTION_NAME = "users";

    @Constraints.Required
    private String login;
    @Constraints.Required
    private String password;

    private ArrayList<SecurityRole> roles;
    private ArrayList<UserPermission> permissions;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<SecurityRole> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<SecurityRole> roles) {
        this.roles = roles;
    }

    public ArrayList<UserPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(ArrayList<UserPermission> permissions) {
        this.permissions = permissions;
    }

    @JsonCreator
    public User(@JsonProperty("login") String login, @JsonProperty("password") String password) {

        super(COLLECTION_NAME);

        this.login = login;
        this.password = password;

    }


    public User() {
        super(COLLECTION_NAME);
    }

    public  User findByLogin(String login) {
        return db().findOne("{login: #}", login).as(User.class);

    }


    public  boolean authenticate(String login, String passwd){
        User leUser = findByLogin(login);
        if(leUser==null){
            return false;
        }
        HashUtils myHashUtils = new HashUtils();
        Logger.debug(login);
        return myHashUtils.verifyPassword(passwd, leUser.password);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles +
                ", permissions=" + permissions +
                '}';
    }
}
