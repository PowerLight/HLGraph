package model.new_database;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import model.ModeleMongo;
import model.MotifMongo;
import model.SeanceMongo;
import org.bson.types.ObjectId;
import org.jongo.MongoCursor;
import play.data.validation.Constraints;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by rcollas on 22/12/2016.
 */
public class Patient extends ModeleMongo {
    private static final String COLLECTION_NAME = "patient";

    private String birthday;
    @Constraints.Required
    private String firstname;
    @Constraints.Required
    private String lastname;
    @Constraints.Required
    private long lastUpdate;

    private ArrayList<SeanceMongo> lesSeances;

    public ArrayList<SeanceMongo> getLesSeances() {
        return lesSeances;
    }

    public void setLesSeances(ArrayList<SeanceMongo> lesSeances) {
        this.lesSeances = lesSeances;
    }

    public Patient() {
        super(COLLECTION_NAME);
        this.lesSeances = new ArrayList<SeanceMongo>();
    }

    public Patient(String birthday, String firstname, String lastname){
        this();
        this.birthday = birthday;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    /*@Override
    public String save(){
        this.lastUpdate = new Date().getTime();
        return this._id;
    }*/

    @Override
    public String save(){
        this.createUUIDs();
        this.lastUpdate = new Date().getTime();
        db().save(this);
        return this._id;

    }public String synchroSave(){
        this.createUUIDs();

        db().save(this);
        return this._id;
    }

    @Override
    public void update(String field, Object value){

        db().update("{_id : #}",new ObjectId(this._id)).with("{$set : {"+field+" : #, lastUpdate : #}}", value, new Date().getTime());

    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }



    public static List<Patient> findAll(String term) {
        Patient p = new Patient();
        final MongoCursor<Patient> patients = p.find(term);
        List<Patient> list = new ArrayList<Patient>();
        for (Patient patient : patients) {

            list.add(patient);
        }

        return list;
    }

    @Override
    public Patient findOne(String _id) {

        Patient patient = (Patient) new Patient().findOneById(_id);
        //Logger.debug("patient = " + patient.lesSeances);

        return patient;
    }

    public void renumeroterSeances(){
        int numero = 1;
        for (SeanceMongo uneSeanceMongo : this.lesSeances){
            uneSeanceMongo.seanceNumber = numero;
            numero++;
        }
    }

    public List<SeanceMongo> createUUIDs( ){
        boolean uptated = false;
        for (SeanceMongo uneSeanceMongo : this.lesSeances){
            if (uneSeanceMongo.numeroUniqueDeSeance == null){
                uneSeanceMongo.numeroUniqueDeSeance = new ObjectIdGenerators.UUIDGenerator().generateId(uneSeanceMongo).toString();
                uptated = true;
            }
        }

        if (uptated) {
            renumeroterSeances();
        }
        return this.lesSeances;
    }

    public int findSeanceIndex(String numeroUniqueDeSeance){
        int i = 0;
        int monIndex = -1;
        for(SeanceMongo uneSeance : this.lesSeances){
            if(uneSeance.numeroUniqueDeSeance.equals(numeroUniqueDeSeance)){

                monIndex = i;
            }
            i++;
        }
        return monIndex;
    }

    public SeanceMongo findSeance(String numeroUniqueDeSeance){
        for(SeanceMongo uneSeance : this.lesSeances){
            if(uneSeance.numeroUniqueDeSeance.equals(numeroUniqueDeSeance)){
                return uneSeance;
            }
        }
        return null;
    }

    public boolean testMotifNameExist(String motif){
        for(SeanceMongo uneSeance : this.lesSeances){
            for(MotifMongo unMotif : uneSeance.lesMotifs){
                if(unMotif.motifName.equals(motif)){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return COLLECTION_NAME+"{" +
                "_id=" + _id +
                ", birthday='" + birthday + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", lastUpdate='" + lastUpdate + '\'' +
                '}';
    }
}
