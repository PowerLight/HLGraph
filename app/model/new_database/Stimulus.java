package model.new_database;

import model.ModeleMongo;

/**
 * Created by rcollas on 22/12/2016.
 */
public class Stimulus extends ModeleMongo{

    private static final String COLLECTION_NAME = "stimulus";

    //private int id;

    private String color;
    private int intensity;
    private int frequency;

    public Stimulus() {
        super(COLLECTION_NAME);
    }

    public Stimulus(String color, int intensity, int frequency){
        this();
        this.color = color;
        this.frequency = frequency;
        this.intensity = intensity;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getIntensity() {
        return intensity;
    }

    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return COLLECTION_NAME+"{" +
                "_id=" + _id +
                ", color='" + color + '\'' +
                ", intensity=" + intensity +
                ", frequency=" + frequency +
                '}';
    }
}
