package model.new_database;

import model.ModeleMongo;
import org.bson.types.ObjectId;

import java.util.ArrayList;

/**
 * Created by rcollas on 22/12/2016.
 */
public class Scenario extends ModeleMongo{

    private static final String COLLECTION_NAME = "scenario";

    private String title;
    private String description;
    private String scenarioType;
    private ArrayList<ScenarioSequence> sequences;
    private ArrayList<Capteur> captors;
    private long lastUpdate;

    public Scenario() {
        super(COLLECTION_NAME);
        sequences = new ArrayList<>();
        captors = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<ScenarioSequence> getSequences() {
        return sequences;
    }

    public void setSequences(ArrayList<ScenarioSequence> sequences) {
        this.sequences = sequences;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getScenarioType() {
        return scenarioType;
    }

    public void setScenarioType(String scenarioType) {
        this.scenarioType = scenarioType;
    }

    public ArrayList<Capteur> getCaptors() {
        return captors;
    }

    public void setCaptors(ArrayList<Capteur> captors) {
        this.captors = captors;
    }

    @Override
    public String toString() {
        return COLLECTION_NAME+"{" +
                "_id=" + _id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", type='" + scenarioType + '\'' +
                ", sequences='" + sequences + '\'' +
                ", captors='" + captors + '\'' +
                ", lastUpdate='" + lastUpdate + '\'' +
                '}';
    }
}
