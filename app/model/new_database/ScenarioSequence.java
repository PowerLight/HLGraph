package model.new_database;

import model.ModeleMongo;

/**
 * Created by vpozdnia on 24/02/2017.
 */
public class ScenarioSequence extends ModeleMongo {

    private static final String COLLECTION_NAME = "sequence";

    private String description;
    private int duration;
    private Stimulus stimulus;
    private int orderIndex;

    public ScenarioSequence() {
        super(COLLECTION_NAME);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Stimulus getStimulus() {
        return stimulus;
    }

    public void setStimulus(Stimulus stimulus) {
        this.stimulus = stimulus;
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }

    @Override
    public String toString() {
        return "{" +
                ", description='" + description + '\'' +
                ", duration='" + duration + '\'' +
                ", orderIndex='" + orderIndex + '\'' +
                ", stimulus='" + stimulus + '\'' +
                '}';
    }
}
