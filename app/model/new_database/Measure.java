package model.new_database;

import model.ModeleMongo;
import org.bson.types.ObjectId;

/**
 * Created by rcollas on 22/12/2016.
 */
public class Measure extends ModeleMongo{

    private static final String COLLECTION_NAME = "measure";

    private int timestamp;
    private double dataReceived;
    private ObjectId seanceId;
    private ObjectId captorId;

    public Measure() {
        super(COLLECTION_NAME);
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public double getDataReceived() {
        return dataReceived;
    }

    public void setDataReceived(double dataReceived) {
        this.dataReceived = dataReceived;
    }

    public ObjectId getSeanceId() {
        return seanceId;
    }

    public void setSeanceId(ObjectId seanceId) {
        this.seanceId = seanceId;
    }

    public ObjectId getCaptorId() {
        return captorId;
    }

    public void setCaptorId(ObjectId captorId) {
        this.captorId = captorId;
    }

    @Override
    public String toString() {
        return COLLECTION_NAME+"{" +
                "_id=" + _id +
                ", timestamp='" + timestamp + '\'' +
                ", dataReceived='" + dataReceived + '\'' +
                ", seanceId='" + seanceId + '\'' +
                ", captorId='" + captorId + '\'' +
                '}';
    }
}
