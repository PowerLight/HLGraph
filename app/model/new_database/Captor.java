package model.new_database;

import model.ModeleMongo;

/**
 * Created by rcollas on 22/12/2016.
 */
public class Captor extends ModeleMongo {

    private static final String COLLECTION_NAME = "captor";

    private int _id;
    private String unit;
    private String description;
    private String captorType;
    private int responseByte;

    public Captor() {
        super(COLLECTION_NAME);
    }

    public Captor(String unit, String description, String captorType, int reponseBytes){
        this();
        this.unit = unit;
        this.captorType = captorType;
        this.description = description;
        this.responseByte = reponseBytes;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCaptorType() {
        return captorType;
    }

    public void setCaptorType(String captorType) {
        this.captorType = captorType;
    }

    public int getResponseByte() {
        return responseByte;
    }

    public void setResponseByte(int responseByte) {
        this.responseByte = responseByte;
    }

    @Override
    public String toString() {
        return COLLECTION_NAME+"{" +
                "_id=" + _id +
                ", unit='" + unit + '\'' +
                ", description='" + description + '\'' +
                ", captorType='" + captorType + '\'' +
                ", responseByte='" + responseByte + '\'' +
                '}';
    }
}
