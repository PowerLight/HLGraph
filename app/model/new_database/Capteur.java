package model.new_database;

import model.ModeleMongo;

/**
 * Created by rcollas on 22/12/2016.
 */
public class Capteur extends ModeleMongo {

    private static final String COLLECTION_NAME = "captor";

    private String unite;
    private String description;
    private String captorType;
    private int responseBytes;

    public Capteur() {
        super(COLLECTION_NAME);
    }

    public Capteur(String unite, String description, String captorType, int reponseBytes){
        this();
        this.unite = unite;
        this.captorType = captorType;
        this.description = description;
        this.responseBytes = reponseBytes;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCaptorType() {
        return captorType;
    }

    public void setCaptorType(String captorType) {
        this.captorType = captorType;
    }

    public int getResponseBytes() {
        return responseBytes;
    }

    public void setResponseBytes(int responseBytes) {
        this.responseBytes = responseBytes;
    }

    @Override
    public String toString() {
        return COLLECTION_NAME+"{" +
                "_id=" + _id +
                ", unite='" + unite + '\'' +
                ", description='" + description + '\'' +
                ", captorType='" + captorType + '\'' +
                ", responseBytes='" + responseBytes + '\'' +
                '}';
    }
}
