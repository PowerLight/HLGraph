package model.new_database;

import model.ModeleMongo;
import org.bson.types.ObjectId;

/**
 * Created by rcollas on 24/01/2017.
 */
public class Motif extends ModeleMongo{

    private static final String COLLECTION_NAME = "motif";

    private long startTimestamp;
    private long endTimestamp;
    private String type;
    private ObjectId seanceId;
    private ObjectId captorId;


    public Motif() {
        super(COLLECTION_NAME);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ObjectId getSeanceId() {
        return seanceId;
    }

    public void setSeanceId(ObjectId seanceId) {
        this.seanceId = seanceId;
    }

    public ObjectId getCaptorId() {
        return captorId;
    }

    public void setCaptorId(ObjectId captorId) {
        this.captorId = captorId;
    }

    @Override
    public String toString() {
        return COLLECTION_NAME+"{" +
                "_id=" + _id +
                ", unite='" + startTimestamp + '\'' +
                ", description='" + endTimestamp + '\'' +
                ", captorType='" + type + '\'' +
                ", captorType='" + seanceId + '\'' +
                ", responseBytes='" + captorId + '\'' +
                '}';
    }
}
