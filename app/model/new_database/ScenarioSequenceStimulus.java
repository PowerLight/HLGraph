package model.new_database;

/**
 * Created by rcollas on 22/12/2016.
 */
public class ScenarioSequenceStimulus{

    private String color;
    private int intensity;
    private int frequency;

    public ScenarioSequenceStimulus(){}

    public ScenarioSequenceStimulus(Stimulus s){
        this.color = s.getColor();
        this.frequency = s.getFrequency();
        this.intensity = s.getIntensity();
    }

    public String getColor() {
        return color;
    }

    public void setColor(String ledColor) {
        this.color = ledColor;
    }

    public int getIntensity() {
        return intensity;
    }

    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return "{" +
                ", color='" + color + '\'' +
                ", intensity=" + intensity +
                ", frequency=" + frequency +
                '}';
    }
}
