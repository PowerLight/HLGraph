package model.new_database;

import model.ModeleMongo;
import org.bson.types.ObjectId;

/**
 * Created by rcollas on 22/12/2016.
 */
public class Seance extends ModeleMongo{

    private static final String COLLECTION_NAME = "seance";

    private String dateStart;
    private int patientId;
    private SeanceScenario scenario;
    private long lastUpdate;
    private String status;

    public Seance() {
        super(COLLECTION_NAME);
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public SeanceScenario getScenario() {
        return scenario;
    }

    public void setScenario(SeanceScenario scenario) {
        this.scenario = scenario;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return COLLECTION_NAME+"{" +
                "_id=" + _id +
                ", dateStart='" + dateStart + '\'' +
                ", patientId='" + patientId + '\'' +
                ", scenario='" + scenario + '\'' +
                ", lastUpdate='" + lastUpdate + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
