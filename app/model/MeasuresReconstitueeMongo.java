package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;


/**
 * Created by rleguill on 08/11/2016.
 */
public class MeasuresReconstitueeMongo extends ModeleMongo implements java.lang.Comparable {
    public String _id;
    public int captor_id;
    public float dataReceived;
    public String numeroUniqueDeSeance;
    public long measureTimestamp;

    public MeasuresReconstitueeMongo() {
        super("measuresReconstituees");
    }

    public MeasuresReconstitueeMongo(int captor_id) {
        super("measuresReconstituees");
        this.captor_id = captor_id;
    }



    public MeasuresReconstitueeMongo(int measureId, int captor_id, int seanceNumber, String numUniqueSeance, float dataReceived, long measureTimestamp) {
        super("measuresReconstituees");
        this.numeroUniqueDeSeance = numUniqueSeance;
        this.captor_id = captor_id;
        this.dataReceived = dataReceived;
        this.measureTimestamp = measureTimestamp;
    }

    public HashMap toMap(){
        HashMap monMap = new HashMap();
        monMap.put("numeroUniqueDeSeance", this.numeroUniqueDeSeance);
        monMap.put("_id", this._id);
        monMap.put("captor_id", this.captor_id);
        monMap.put("dataReceived", this.dataReceived);
        monMap.put("measureTimestamp", this.measureTimestamp);

        return monMap;
    }

    @Override
    public int compareTo(Object o) {
        return (int) (this.measureTimestamp-((MeasureMongo) o).measureTimestamp);
    }

    @Override
    public String toString() {
        return "MeasuresReconstituees{" +
                "_id=" + _id +
                ", captor_id=" + captor_id +
                ", dataReceived=" + dataReceived +
                ", measureTimestamp=" + measureTimestamp +
                ", numeroUniqueDeSeance='" + numeroUniqueDeSeance +
                "'}";
    }
}
