package model;

/**
 * Created by gsauer on 10/12/2015.
 */
public class Captor extends ModeleMongo{
    public int captorId;
    public String unite;
    public String description;
    public String captorStatus;
    public String captorType;
    public String captorNumber;
    public int responseBytes;

    public Captor(){
        super("captors");
    }

    public Captor(int _captorId, String _unite, String _description, String _captorStatus, String _captorType, String _captorNumber, int _responseBytes){
      super("captors");
      this.captorId = _captorId;
      this.unite = _unite;
      this.description = _description;
      this.captorStatus = _captorStatus;
      this.captorType = _captorType;
      this.captorNumber = _captorNumber;
      this.responseBytes = _responseBytes;
    }

    @Override
    public String toString() {
        return "Captor{" +
                "unite='" + unite + '\'' +
                ", description='" + description + '\'' +
                ", captorStatus='" + captorStatus + '\'' +
                ", captorType='" + captorType + '\'' +
                ", captorNumber='" + captorNumber + '\'' +
                ", captorId=" + captorId +
                ", responseBytes=" + responseBytes +
                '}';
    }
}
