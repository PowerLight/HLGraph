package model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.apache.commons.io.IOUtils;

import play.Logger;
import play.libs.Json;


import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import java.sql.Timestamp;

import java.util.*;


/**
 * Created by rbougrin on 04/01/2016.
 */
public class SeanceMongo {
    public int seanceId;

    public int seanceNumber;
    public String date_start;
    public String patientMongoId;
    public int scenario_id;
    public String scenarioMongoId;
    public ScenarioMongo scenario;
    public String numeroUniqueDeSeance;
    public long lastUpdate;
    public String time_start;
    public String status;
    public HashMap capteurs = new HashMap();
    public MeteoData meteoData;
    public DataSet dataset;

    public ArrayList<AnalyzedData> analyses;

    public ArrayList<MotifMongo> lesMotifs;//Spindle, ComplexeK, ....


    public SeanceMongo() {
        lesMotifs =new ArrayList<MotifMongo>();
        dataset = new DataSet();
        meteoData = new MeteoData();
    }
    public MeteoData setMeteoData() {
        if (meteoData.data.size() == 0) {
            int jour = Integer.parseInt(date_start.substring(0, 2));
            int mois = Integer.parseInt(date_start.substring(3, 5));
            int annee = Integer.parseInt(date_start.substring(6));

            Timestamp timestamp = new Timestamp(new GregorianCalendar(annee, mois-1, jour).getTimeInMillis());
            Logger.debug("" + timestamp.toString() + " - " + annee + " - " + mois + " - " + jour);
            String strTimestampForUrl = "" + timestamp.getTime() / 1000;
            //GetMethod get = new GetMethod("https://api.forecast.io/forecast/1e010bd859d22d0347ba4e2ca886f629/48.8534100,2.3488000,1457860916");
            //GetMethod get = new GetMethod();

            try {
                URL url = new URL("https://api.forecast.io/forecast/1e010bd859d22d0347ba4e2ca886f629/48.8534100,2.3488000,"+strTimestampForUrl);
                URLConnection conn = url.openConnection();
                InputStream instr = conn.getInputStream();
                String theString =  IOUtils.toString(instr);
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                Json.setObjectMapper(objectMapper);
                this.meteoData = Json.fromJson(Json.parse(theString), MeteoData.class);
                DailyMeteo aDaylyMeteo = Json.fromJson(Json.parse(theString).findPath("currently"), DailyMeteo.class);
                this.meteoData.data.add(aDaylyMeteo);
                for(int i = 1; i <30; i++){
                    strTimestampForUrl = "" + (Integer.parseInt(strTimestampForUrl) - 86400);
                    URL url2 = new URL("https://api.forecast.io/forecast/1e010bd859d22d0347ba4e2ca886f629/48.8534100,2.3488000,"+strTimestampForUrl);
                    URLConnection conn2 = url2.openConnection();
                    InputStream instr2 = conn2.getInputStream();
                    String theString2 =  IOUtils.toString(instr2);
                    DailyMeteo aDaylyMeteo2 = Json.fromJson(Json.parse(theString2).findPath("currently"), DailyMeteo.class);
                    this.meteoData.data.add(aDaylyMeteo2);
                }



            } catch (java.io.IOException e) {
                e.printStackTrace();
            }


        }
        return this.meteoData;
    }
    public HashMap getCapteurs(){
        setCaptors_ids();
        return capteurs;
    }


    public int renumeroterMotif(){
        int numero = 1;
        for (MotifMongo uneMotifMongo : this.lesMotifs){
            uneMotifMongo.motifNumber = numero;
            numero++;
        }
        return numero;
    }

    public void ordonnerMotif(){
        Collections.sort(lesMotifs);
        renumeroterMotif();
    }

    public int trouverMotif(String NumUniqMotif){
        int i=0;
        for (MotifMongo unMotif : this.lesMotifs){
            if(0==unMotif.numeroUniqueMotif.compareTo(NumUniqMotif)){
                return i;
            }
            i+=1;
        }
        return -1;
    }

    public boolean supprimerMotif(String NumUniqMotif){
        int i=trouverMotif(NumUniqMotif);
        if(-1==i)return false;
        this.lesMotifs.remove(i);
        renumeroterMotif();
        return true;
    }

    public boolean recrireMotif(String NumUniqMotif,MotifPattern motif){
        int i=trouverMotif(NumUniqMotif);
        if(-1==i)return false;//fin de la fonction si le motif n'existe pas en base

        this.lesMotifs.get(i).xDebut  =motif.x_debut;
        this.lesMotifs.get(i).xFin    =motif.x_fin;
        this.lesMotifs.get(i).captorId=motif.id_choix_signaux;
        this.lesMotifs.get(i).motifName=motif.id_choix_motif;
        this.lesMotifs.get(i).lastUpdate = System.currentTimeMillis();
        renumeroterMotif();
        return true;
    }


    public List<MotifMongo> createUUIDs( ){
        boolean updated = false;
        for (MotifMongo unMotifMongo : this.lesMotifs){
            if (unMotifMongo.numeroUniqueMotif == null){
                unMotifMongo.numeroUniqueMotif = new ObjectIdGenerators.UUIDGenerator().generateId(unMotifMongo).toString();
                updated = true;
            }
        }

        if (updated) {
            renumeroterMotif();
        }

        return this.lesMotifs;
    }

    public void setCaptors_ids() {
        if (this.seanceId >= 1) {
            ScenarioMongo leScenario = (ScenarioMongo) new ScenarioMongo().findOneById(this.scenarioMongoId);
            if (leScenario.eeg == 1) {
                capteurs.put("EEG", 1);
            }
            if (leScenario.eyelid == 1) {
                capteurs.put("eye_blinking", 1);
            }
            if (leScenario.headMovement == 1) {
                capteurs.put("x_axis_gyro", 1);
            }
            if (leScenario.headMovement == 1) {
                capteurs.put("y_axis_gyro", 1);
            }
            if (leScenario.headMovement == 1) {
                capteurs.put("z_axis_gyro", 1);
            }
            if (leScenario.headMovement == 1) {
                capteurs.put("x_axis_acceleration", 1);
            }
            if (leScenario.headMovement == 1) {
                capteurs.put("y_axis_acceleration", 1);
            }
            if (leScenario.headMovement == 1) {
                capteurs.put("z_axis_acceleration", 1);
            }
            if (leScenario.pulse == 1) {
                capteurs.put("heart_rate", 1);
            }
            if (leScenario.skinConductance == 1) {
                capteurs.put("skin_conductance", 1);
            }
        }
    }

    @Override
    public String toString() {
        return "SeanceMongo{" +
                "seanceId=" + seanceId +
                ", seanceNumber=" + seanceNumber +
                ", date_start='" + date_start + '\'' +
                ", patientMongoId='" + patientMongoId + '\'' +
                ", scenario_id=" + scenario_id +
                ", scenarioMongoId='" + scenarioMongoId + '\'' +
                ", time_start='" + time_start + '\'' +
                ", status='" + status + '\'' +
                ", capteurs=" + capteurs +
                ", meteoData=" + meteoData +
                ", dataset=" + dataset +
                ", lesMotifs=" + lesMotifs +
                '}';
    }
}
