package model;

import java.util.*;

import org.jongo.MongoCursor;
import play.Logger;
import model.new_database.Patient;

/**
 * Created by bderouin on 06/11/2015.
 */
public class DataSet{

    public int patientId;
    public String patientMongoId;
    public int seanceNumber;
    public String numeroUniqueDeSeance;



    public ArrayList<CaptorData> captor_data = new ArrayList<CaptorData>();


//TODO filtrer sur le nombre de capteurs voulus
    public DataSet() {


    }


    public DataSet findOneBySeanceIdBetweenTimestampSingle(long start, long stop, Patient patient, String numeroUniqueDeSeance,int captor_id){
        try{
            DataSet leDataSet;
            SeanceMongo seanceMongo = patient.getLesSeances().get(patient.findSeanceIndex(numeroUniqueDeSeance));
            Logger.debug("before if");
            Logger.info("one signal : "+captor_id);
            MongoCursor cur = new MeasureMongo().findWithLimit("{numeroUniqueDeSeance : '"+seanceMongo.numeroUniqueDeSeance+"'}", 1);

            if (cur.hasNext()){
                patient.getLesSeances().get(patient.findSeanceIndex(seanceMongo.numeroUniqueDeSeance)).dataset = seanceMongo.dataset;
                Logger.debug("if");
            }else {
                Logger.debug("else");
                createDataset( seanceMongo);

                //Logger.debug(leDataSet.toString());
                patient.getLesSeances().get(patient.findSeanceIndex(seanceMongo.numeroUniqueDeSeance)).dataset = this;
                patient.update("lesSeances",  patient.getLesSeances());
            }
            return  patient.getLesSeances().get(patient.findSeanceIndex(seanceMongo.numeroUniqueDeSeance)).dataset.setCaptorDataSingle(seanceMongo, start, stop,captor_id);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    public DataSet findOneBySeanceIdBetweenTimestampFiltered(long start, long stop, Patient patient, String numeroUniqueDeSeance){
        try{
            DataSet leDataSet;
            SeanceMongo seanceMongo = patient.getLesSeances().get(patient.findSeanceIndex(numeroUniqueDeSeance));
            Logger.debug("get measuresMongo {numeroUniqueDeSeance : '"+seanceMongo.numeroUniqueDeSeance+"'}");
            MongoCursor cur = new MeasureMongo().findWithLimit("{numeroUniqueDeSeance : '"+seanceMongo.numeroUniqueDeSeance+"'}", 1);

            if (cur.hasNext()){
                patient.getLesSeances().get(patient.findSeanceIndex(seanceMongo.numeroUniqueDeSeance)).dataset = seanceMongo.dataset;
                Logger.debug("if");
            }else {
                Logger.debug("else");
                createDataset( seanceMongo);

                //Logger.debug(leDataSet.toString());
                patient.getLesSeances().get(patient.findSeanceIndex(seanceMongo.numeroUniqueDeSeance)).dataset = this;
                patient.update("lesSeances",  patient.getLesSeances());

            }

            return  patient.getLesSeances().get(patient.findSeanceIndex(seanceMongo.numeroUniqueDeSeance)).dataset.setCaptorData(seanceMongo, start, stop);

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;


    }

    public DataSet setCompleteCaptorData(SeanceMongo laSeance){

        for(CaptorData unCapData : this.captor_data){
            MongoCursor<MeasureMongo> cursor = new MeasureMongo().find("{captor_id : "+unCapData.captor_id+", numeroUniqueDeSeance : '"+laSeance.numeroUniqueDeSeance+"'}");
            Logger.debug(unCapData.captor_id+"");
            if(cursor.count() == 0){
                unCapData.generateMeasuresMongo(laSeance);
            }
            for(MeasureMongo uneMM : cursor){
                unCapData.measures.add(uneMM);
            }

        }
        return this;
    }

    public DataSet setCaptorData(SeanceMongo laSeanceMongo, long start, long stop){
        ArrayList<CaptorData> lesCapData = new ArrayList<CaptorData>();
        int nb_signal=8;//this.captor_data.size();

        Logger.info("set all signal : "+nb_signal);
        for (CaptorData unCapData : this.captor_data){
            MongoCursor count = new MeasureMongo().findWithLimit("{captor_id : "+unCapData.captor_id+", numeroUniqueDeSeance : '"+laSeanceMongo.numeroUniqueDeSeance+"'}", 1);
            if(!count.hasNext()){
                unCapData.generateMeasuresMongo(laSeanceMongo);
            }
            unCapData.findMeasuresBetweenTimestamps(laSeanceMongo, start, stop);
            unCapData.filter(nb_signal);

            lesCapData.add(unCapData);

        }
        this.captor_data = lesCapData;
        Collections.sort(this.captor_data, new Comparator<CaptorData>() {
            @Override
            public int compare(CaptorData o1, CaptorData o2) {
                return o1.captor_id - o2.captor_id;
            }
        });
        return this;
    }


    public DataSet setCaptorDataSingle(SeanceMongo laSeanceMongo, long start, long stop, int captor_id){
        ArrayList<CaptorData> lesCapData = new ArrayList<CaptorData>();
        //CaptorData unCapData=this.captor_data.get(captor_id);
        /*for (CaptorData unCapData : this.captor_data)*/{
            MongoCursor count = new MeasureMongo().findWithLimit("{captor_id : "+ this.captor_data.get(captor_id).captor_id+", numeroUniqueDeSeance : '"+laSeanceMongo.numeroUniqueDeSeance+"'}", 1);
            if(!count.hasNext()){
                this.captor_data.get(captor_id).generateMeasuresMongo(laSeanceMongo);
            }
            //Logger.debug("Start "+start+"| stop "+stop);
            this.captor_data.get(captor_id).findMeasuresBetweenTimestamps(laSeanceMongo, start, stop);
            this.captor_data.get(captor_id).filter(1);
            lesCapData.add(this.captor_data.get(captor_id));
        }
        this.captor_data = lesCapData;
        Collections.sort(this.captor_data, new Comparator<CaptorData>() {
            @Override
            public int compare(CaptorData o1, CaptorData o2) {
                return o1.captor_id - o2.captor_id;
            }
        });
        return this;
    }


    public DataSet createDataset( SeanceMongo seance){

            this.seanceNumber = seance.seanceNumber;
            this.patientMongoId = seance.patientMongoId;
            this.numeroUniqueDeSeance = seance.numeroUniqueDeSeance;

            this.captor_data.clear();

            Iterator it = seance.getCapteurs().entrySet().iterator();
            while(it.hasNext()){
                //Logger.debug("while");
                Map.Entry pair = (Map.Entry) it.next();

                if((int) pair.getValue() == 1){
                    Captor leCaptor = (Captor) new Captor().findOne("{captorType : '"+pair.getKey()+"'}");
                    CaptorData unCaptorData = new CaptorData(leCaptor.captorId);
                    unCaptorData.captor = leCaptor;
                    this.captor_data.add(unCaptorData);

                }



            }

            return this;

    }
    public Captor findCaptor(int captor_id){
        Iterator<CaptorData> ite=this.captor_data.iterator();
        CaptorData tmp= new CaptorData();
        while (ite.hasNext()){
            tmp=ite.next();
            if(tmp.captor_id==captor_id){
                return tmp.captor;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "DataSet{" +
                "patientId=" + patientId +
                ", patientMongoId='" + patientMongoId + '\'' +
                ", seanceNumber=" + seanceNumber +
                ", captor_data=" + captor_data +
                '}';
    }
}
