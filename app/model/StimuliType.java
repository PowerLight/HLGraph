package model;

/**
 * Created by dmatjaou on 25/04/2016.
 */
public class StimuliType extends ModeleMongo{
    public String name;
    public StimuliType() {
        super("stimuliType");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
