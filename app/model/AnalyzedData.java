package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jblairon on 14/06/2016.
 */
public class AnalyzedData extends ModeleMongo {
    public String numeroUniqueDeSeance;
    public String patient_id;
    public ArrayList<SequencesFft> lesSequences;

    public AnalyzedData(String analyseId) {
        super("analyzedData");
        this.lesSequences = new ArrayList<SequencesFft>();
    }

    public AnalyzedData() {
        super("analyzedData");
        this.lesSequences = new ArrayList<SequencesFft>();
    }

    @Override
    public String toString() {
        return "AnalyzedData{" +
                "numeroUniqueDeSeance='" + numeroUniqueDeSeance + '\'' +
                ", patient_id='" + patient_id + '\'' +
                ", lesSequences=" + lesSequences +
                '}';
    }
}
