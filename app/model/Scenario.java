package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

/**
 * Created by rbougrin on 18/11/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Scenario extends ModeleMongo {
    public int scenarioId;
    public String description;
    public String typeScenario;
    public int eeg;
    public int eyelid;
    public int headMovement;
    public int pulse;
    public int skinConductance;
    public String title;
    public String lastUpdate;

    //Les attributs (capteurs) : eeg, pulse etc sont de type boolean sur sqlite et int sur mongo
    public Scenario() {
        super("scenari");
    }
    public  Scenario(int scnId, String desc, String typScn, String title, String upDate,
                     boolean eeg, boolean eyelid,
                     boolean head, boolean pulse,
                     boolean skin){
        super("scenari");
        this.scenarioId = scnId;
        this.description = desc;
        this.typeScenario = typScn;
        this.title = title;
        this.lastUpdate = upDate;
        this.eeg = (eeg) ? 1 : 0;
        this.eyelid = (eyelid) ? 1 : 0;
        this.headMovement = (head) ? 1 : 0;
        this.pulse = (pulse) ? 1 : 0;
        this.skinConductance = (skin ) ? 1 : 0;
    }

    @Override
    public String save(){
        if(this.lastUpdate == null) {
            this.lastUpdate = String.valueOf(new Date().getTime());
        }
         db().save(this);
        return this._id;
//        return null;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    //Convertir un boolean à 1 si true et à 0 si false

}
