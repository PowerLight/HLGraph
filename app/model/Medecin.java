package model;

import java.math.BigInteger;

/**
 * Created by gsauer on 21/01/2016.
 */
public class Medecin extends ModeleMongo{
    public long rpps;
    public String siret;
    public String nom;
    public String prenom;
    public String discipline;
    public String telephone;
    public String courriel;

    public void setRpps(long rpps) {
        this.rpps = rpps;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setCourriel(String courriel) {
        this.courriel = courriel;
    }

    public Medecin(){
        super("medecin");
    }

    public Medecin(long rpps, String siret, String nom, String prenom, String discipline, String telephone, String courriel){
        super("medecin");
        this.rpps = rpps;
        this.siret = siret;
        this.nom = nom;
        this.prenom = prenom;
        this.discipline = discipline;
        this.telephone = telephone;
        this.courriel = courriel;
    }

    public String toString() {
        return "Medecin{" +
                "id='" + _id + '\'' +
                "rpps='" + rpps + '\'' +
                "siret='" + siret + '\'' +
                "nom='" + nom + '\'' +
                "prenom='" + prenom + '\'' +
                "discipline='" + discipline + '\'' +
                "telephone='" + telephone + '\'' +
                "courriel='" + courriel + '\'' +
                '}';
    }

}
