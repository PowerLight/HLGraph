package model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by dmatjaou on 20/02/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SqltSenario{
    public int scenarioId;
    public String description;
    public String title;
    public String typeScenario;
    public boolean eeg;
    public boolean pulse;
    public boolean eyelid;
    public boolean headMovement;
    public boolean skinConductance;
    public String lastUpdate;

    public SqltSenario() {
    }

    //Convertir un boolean à 1 si true et à 0 si false
    public void boolToInt(boolean eeg, boolean eyelid,
                          boolean head, boolean pulse,
                          boolean skin, Scenario scenario){

        scenario.eeg = (eeg) ? 1 : 0;
        scenario.eyelid = (eyelid) ? 1 : 0;
        scenario.headMovement = (head) ? 1 : 0;
        scenario.pulse = (pulse) ? 1 : 0;
        scenario.skinConductance = (skin ) ? 1 : 0;
    }

}
