package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.bson.types.ObjectId;
import org.jongo.MongoCursor;
import play.data.validation.Constraints;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by rbougrin on 16/11/2015.
 */
public class Patient extends ModeleMongo {
    @Constraints.Required
    public int patientId;
    public String birthday;
    @Constraints.Required
    public String firstname;
    @Constraints.Required
    public String lastname;
    public long lastUpdate;
    public int permanentId;

    public ArrayList<SeanceMongo> lesSeances;
    @JsonIgnore
    public ArrayList sessionsAttended;

    public Patient() {
        super("patients");
        this.lesSeances = new ArrayList<SeanceMongo>();
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    @Override
    public String save(){
        this.createUUIDs();
        this.lastUpdate = new Date().getTime();
        db().save(this);
        return this._id;

    }
    public String synchroSave(){
        this.createUUIDs();

        db().save(this);
        return this._id;
    }

    @Override
    public void update(String field, Object value){

        db().update("{_id : #}",new ObjectId(this._id)).with("{$set : {"+field+" : #, lastUpdate : #}}", value, new Date().getTime());

    }

    public static List<Patient> findAll(String term) {
        Patient p = new Patient();
        final MongoCursor<Patient> patients = p.find(term);
        List<Patient> list = new ArrayList<Patient>();
        for (Patient patient : patients) {

            list.add(patient);
        }

        return list;
    }
    @Override
    public Patient findOne(String _id) {

        Patient patient = (Patient) new Patient().findOneById(_id);
        //Logger.debug("patient = " + patient.lesSeances);

        return patient;
    }

   public void renumeroterSeances(){
       int numero = 1;
       for (SeanceMongo uneSeanceMongo : this.lesSeances){
           uneSeanceMongo.seanceNumber = numero;
           numero++;
       }
   }

    public List<SeanceMongo> createUUIDs( ){
        boolean uptated = false;
        for (SeanceMongo uneSeanceMongo : this.lesSeances){
            if (uneSeanceMongo.numeroUniqueDeSeance == null){
                uneSeanceMongo.numeroUniqueDeSeance = new ObjectIdGenerators.UUIDGenerator().generateId(uneSeanceMongo).toString();
                uptated = true;
            }
        }

       if (uptated) {
           renumeroterSeances();
       }
        return this.lesSeances;
    }

    public int findSeanceIndex(String numeroUniqueDeSeance){
        int i = 0;
        int monIndex = -1;
        for(SeanceMongo uneSeance : this.lesSeances){
            if(uneSeance.numeroUniqueDeSeance.equals(numeroUniqueDeSeance)){

                monIndex = i;
            }
            i++;
        }
        return monIndex;
    }

    public SeanceMongo findSeance(String numeroUniqueDeSeance){
        for(SeanceMongo uneSeance : this.lesSeances){
            if(uneSeance.numeroUniqueDeSeance.equals(numeroUniqueDeSeance)){
                return uneSeance;
            }
        }
        return null;
    }

    public boolean testMotifNameExist(String motif){
        for(SeanceMongo uneSeance : this.lesSeances){
            for(MotifMongo unMotif : uneSeance.lesMotifs){
                if(unMotif.motifName.equals(motif)){
                    return true;
                }
            }
        }
        return false;
    }


//    //@Override
//    public List<? extends Permission> getPermissions() {
//        return permissions;
//    }
//
//
//    //@Override
//    public String getIdentifier() {
//        return lastname;
//    }
//
//
//    @JsonCreator
//    public Patient(@JsonProperty("lastname") String lastName, @JsonProperty("frstname") String firstName) {
//        super("patients");
////        this.login = login;
//        this.lastname = lastName;
//        this.firstname = firstName;
//        //this.userPassword = password;
//    }


    @Override
    public String toString() {
        return "Patient{" +
                "patientId=" + patientId +
                ", birthday='" + birthday + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", permanentId=" + permanentId +
                ", lesSeances=" + lesSeances +
                '}';
    }
}