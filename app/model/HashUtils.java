package model;



import com.typesafe.config.ConfigFactory;
import play.api.libs.Codecs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbougrin on 25/11/2015.
 */

public class HashUtils {

    int SALT_LENGTH = 10;
    String SECRET = ConfigFactory.load().getString("play.crypto.secret");

    private String createRandomString(int length){
        List<Character> chars = new ArrayList<Character>();
        for (int i = 48; i < 58; i++)//0-9
        {
            chars.add((char)i);
        }
        for (int i = 65; i < 91; i++)//A-Z
        {
            chars.add((char)i);
        }
        for (int i = 97; i < 123; i++)//a-z
        {
            chars.add((char)i);
        }
        chars.add('&');chars.add('!');//special characters
        String sb="";
        for (int i=1; i <= length;i++) {
            int randomNum = (int)(Math.random()*chars.size());
            sb+=chars.get(randomNum);
        }
        return sb.toString();
    }

    public String createPassword(String plainPassword) {
        String salt = this.createRandomString(SALT_LENGTH);
        String password = salt+Codecs.sha1(SECRET+plainPassword +salt);
        return password;
    }

    public boolean verifyPassword(String plainPassword, String encryptedPassword){
        String salt = encryptedPassword.substring(0, SALT_LENGTH);
        String validHash = salt+Codecs.sha1(SECRET+plainPassword+salt);
        return encryptedPassword.equals(validHash);
    }
}
