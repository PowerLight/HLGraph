/**
 * Created by rbougrin on 05/11/2015.
 */

import model.HashUtils;
import model.SecurityRole;
import model.User;
import model.UserPermission;
import play.Application;
import play.GlobalSettings;

import play.api.mvc.EssentialFilter;
import play.filters.gzip.GzipFilter;


import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Steve Chaloner (steve@objectify.be)
 */
public class Global extends GlobalSettings {
    /*
    public <T extends EssentialFilter> Class<T>[] filters() {
        return new Class[]{GzipFilter.class};
    }/**/
    @Override
    public void onStart(Application application) {
        /**GzipFilter gzipFilter = new GzipFilter(
                new GzipFilterConfig().withShouldGzip((req, res) ->
                        res.body().contentType().orElse("").startsWith("text/html")
                ), materializer
        );/**/

        SecurityRole unSR = new SecurityRole();
        if (unSR.db().count() == 0)
            for (String name : Arrays.asList("foo", "bar", "hurdy", "gurdy", "admin", "user")) {
                SecurityRole role = new SecurityRole(name);
                role.save();
            }

        UserPermission uneUP = new UserPermission();
        if (uneUP.db().count() == 0) {
            UserPermission permission = new UserPermission(null, "printers.edit");
            permission.save();
        }

        User unUser = new User();
        HashUtils myHash = new HashUtils();
        if (unUser.db().count() == 0) {
            User user = new User("admini", "un nom", "un prénom",myHash.createPassword("admin") );
            user.roles.add(unSR.findByName("admin"));
            user.roles.add(unSR.findByName("user"));
            user.permissions.add(uneUP.findByPermissionValue("printers.edit"));
            user.save();
        }
    }
}
