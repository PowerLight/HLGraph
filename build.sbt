name := """play-java"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)
//val testOptions = "-Dconfig.file=conf/" + Option(System.getProperty("application.test.conf")).getOrElse("application") + ".conf"
//javaOptions in Test += testOptions

scalaVersion := "2.11.7"




resolvers += "MVNRepo" at "http://mvnrepository.com/artifact"

libraryDependencies ++= Seq(
  //javaJdbc,
  cache,
  javaWs,


  "uk.co.panaxiom" %% "play-jongo" % "0.9.0-jongo1.2",
  "com.google.code.gson" % "gson" % "2.4",
  "net.sourceforge.javacsv" % "javacsv" % "2.0",
  "commons-io" % "commons-io" % "2.4",
  "be.objectify" %% "deadbolt-java" % "2.4.3",
  "com.fasterxml.jackson.dataformat" % "jackson-dataformat-csv" % "2.7.3"


)



//dependencyOverrides += "com.google.guava" % "guava" % "16.0"
dependencyOverrides += "org.scala-lang" % "scala-library" % "2.11.7"
dependencyOverrides += "org.scala-lang" % "scala-reflect" % "2.11.7"
dependencyOverrides +=  "org.apache.httpcomponents" % "httpclient" % "4.3.4"
dependencyOverrides += "org.apache.httpcomponents" % "httpcore" % "4.3.2"
dependencyOverrides += "junit" % "junit" % "4.12"
dependencyOverrides += "commons-logging" % "commons-logging" % "1.1.3"
dependencyOverrides += "org.scala-lang.modules" % "scala-parser-combinators_2.11" % "1.0.4"
dependencyOverrides += "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.4"
dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-annotations" % "2.7.0"
dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-core" % "2.7.3"
dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-databind" % "2.7.3"

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

libraryDependencies += filters

sources in (Compile, doc) := Seq.empty

publishArtifact in (Compile, packageDoc) := false

fork in run := false
