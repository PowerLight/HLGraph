import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import model.DataSet;
import org.junit.Before;
import org.junit.Test;
import play.Configuration;
import play.Logger;
import play.libs.F;
import play.test.Helpers;
import play.test.TestBrowser;

import java.io.File;

import static org.junit.Assert.assertTrue;
import static play.test.Helpers.HTMLUNIT;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.testServer;

/**
 * Created by bderouin on 13/11/2015.
 */
public class DataSetTest {
    private Configuration additionalConfigurations;

    @Before
    public void initialize(){
        Config additionalConfig = ConfigFactory.parseFile(new File("conf/application.conf"));
        additionalConfigurations = new Configuration(additionalConfig);
    }



    @Test
    public void createDataset() throws Exception {
        Helpers.running(testServer(3333, fakeApplication(additionalConfigurations.asMap())), HTMLUNIT, new F.Callback<TestBrowser>() {
            public void invoke(TestBrowser browser) {
                DataSet unDataset = new DataSet().createDataset(12);
                Logger.debug(unDataset.toString());
                assertTrue(unDataset.captor_data_ids.size() >= 1);
            }
        });
    }
}
