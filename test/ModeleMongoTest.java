import com.sun.media.sound.ModelTransform;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;


import model.ModeleTest;
import org.bson.types.ObjectId;
import org.jongo.MongoCursor;
import org.jongo.Oid;
import org.junit.After;

import org.junit.Before;
import org.junit.Test;
import play.Configuration;
import play.Logger;

import play.libs.F;
import play.libs.Json;
import play.test.Helpers;
import play.test.TestBrowser;

import java.io.File;
import java.util.Date;
import java.util.Objects;

import static org.junit.Assert.*;
import static play.test.Helpers.HTMLUNIT;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.testServer;

/**
 * Created by gsauer on 12/11/2015.
 */
public class ModeleMongoTest {
    private Configuration additionalConfigurations;
    private ModeleTest maClasseDeTest;
    private ModeleTest verifMaClasseDeTest;

    @Before
    public void initialize(){
        Config additionalConfig = ConfigFactory.parseFile(new File("conf/application.conf"));
        additionalConfigurations = new Configuration(additionalConfig);
        maClasseDeTest = new ModeleTest();
    }

    @Test
    public void remove(){
        Helpers.running(testServer(3333, fakeApplication(additionalConfigurations.asMap())), HTMLUNIT, new F.Callback<TestBrowser>() {
            public void invoke(TestBrowser browser) {
                try{

                    maClasseDeTest.save();
                    int idSize = maClasseDeTest._id.length();
                    assertEquals(24, idSize);
                    maClasseDeTest.remove();
                    assertNull(maClasseDeTest.findOne(maClasseDeTest._id));
                }
                catch (IllegalArgumentException  e){
                    Logger.debug(e.toString());
                }
            }
        });
    }

    @Test
    public void save() throws Exception{
        Helpers.running(testServer(3333, fakeApplication(additionalConfigurations.asMap())), HTMLUNIT, new F.Callback<TestBrowser>() {
            public void invoke(TestBrowser browser) {
                try{
                    maClasseDeTest.attributDeTest = "toto";
                    maClasseDeTest.save();
                    int idSize = maClasseDeTest._id.length();
                    assertEquals(24, idSize);
                }
                catch (IllegalArgumentException  e){
                    Logger.debug(e.toString());
                }
            }
        });
    }

    @Test
    public void findOne() throws Exception{
        Helpers.running(testServer(3333, fakeApplication(additionalConfigurations.asMap())), HTMLUNIT, new F.Callback<TestBrowser>() {
            public void invoke(TestBrowser browser) {
                try{
                    maClasseDeTest.save();
                    maClasseDeTest.attributDeTest = "jojo";
                    assertEquals(24, maClasseDeTest._id.length());

                    //Logger.debug(maClasseDeTest.toString());
                    //String verif = maClasseDeTest.attributDeTest;
                    //maClasseDeTest = (ModeleTest)maClasseDeTest.findOne(verif);
                    ModeleTest findOne = (ModeleTest)new ModeleTest().findOne("{attributDeTest : 'toto'}");
                    //maClasseDeTest = (ModeleTest)maClasseDeTest.findOne(Oid.withOid("564d84e121efc51c9cc2e66f"));
                    Logger.debug(findOne.toString());
                    assertTrue(findOne.count() >= 1);
                }
                catch (IllegalArgumentException  e){
                    Logger.debug(e.toString());
                }
            }
        });
    }

    @Test
    public void find() throws Exception{
        Helpers.running(testServer(3333, fakeApplication(additionalConfigurations.asMap())), HTMLUNIT, new F.Callback<TestBrowser>() {
            public void invoke(TestBrowser browser) {
                try{
//                    for (int i = 0; i < 2; i++){
//                        ModeleTest maClasse = new ModeleTest();
//                        maClasse.attributDeTest = "find() fonctionnel";
//                        maClasse.save();
//                    }
                    MongoCursor<ModeleTest> findAll = maClasseDeTest.find("{attributDeTest : 'test'}");
                    for (ModeleTest item : findAll){
                        Logger.debug(item.toString());
                    }
                    assertTrue(findAll.count() >= 1);
                }
                catch (IllegalArgumentException  e){
                    Logger.debug(e.toString());
                }
            }
        });
    }
}
